#! /usr/bin/env python

import argparse
import importlib
i = importlib.import_module('plextools.ptl')


class MyAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        # Set optional arguments to True or False
        if option_string:
            attr = True if values else False
            setattr(namespace, self.dest, attr)

        # Modify value of "input" in the namespace
        if hasattr(namespace, 'input'):
            current_values = getattr(namespace, 'input')
            try:
                current_values.extend(values)
            except AttributeError:
                current_values = values
            finally:
                setattr(namespace, 'input', current_values)
        else:
            setattr(namespace, 'input', values)

parser = argparse.ArgumentParser()
parser.add_argument('-a', nargs=1, default=None)
# parser.add_argument('-b', nargs='+', action=MyAction)
parser.add_argument('-b', nargs=1, default=False)
parser.add_argument('link', nargs=1, metavar='LINK', choices=['ln', 'link'], action=MyAction)
parser.add_argument('cpy', nargs=1, metavar='COPY', choices=['cp', 'copy'], action=MyAction)
parser.add_argument('-cp', '--copy', default=None)
parser.add_argument('target', nargs='+')

args = parser.parse_args()

print args

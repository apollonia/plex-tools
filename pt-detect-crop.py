#! /usr/bin/env python
"""
Script to enable batch operation of Don Melton's 'detect-crop.sh.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re
import curses

# package specific imports
from plextools import colors
from plextools import ptl
from plextools.ptl import pt_print, pt_print_err
from plextools.util import switch, ensure_dir, exec_cli, get_script_name, \
                           is_root
from plextools import ptcp, ptmv, ptln, ptsavmd, ptrstmd, ptrmvmd, ptinfo


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()

def my_parse_args(description):
    # parse options and args
    parser = ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter, 
        description=description)
    
    parser.add_argument('-v', '--version', action='version', 
                        version="%(prog)s ("+__version__+")")
    
    parser.add_argument(
        "targets", metavar='FILE|DIR', type=str, nargs='+',
        help="One or more video files (or directories containing video "\
             "files) for detect-crop.sh to scan")
    
    args = parser.parse_args()
    
    return args

def process_dir(dir_path, args):
    """Walk directory tree calling (indirectly) process_file() on each file"""
    full_dir_path = os.path.abspath(dir_path)
    (par_dir_path, dir_name) = os.path.split(full_dir_path)
    os.path.walk(dir_path, process_dir_files, args)

def process_dir_files(in_args, in_dir_path, in_files):
    for video_file in files:
        file_path = os.path.join(in_dir_path, in_files)
        process_file(file_path, in_args)

def process_file(in_file_path, in_args):
    """Calls detect-crop.sh with the passed in file path"""
    pt_print("".join(["Analysing '", colors.BOLD, os.path.basename(in_file_path), 
             colors.END, "'"]), inset=1)
    try:
        command_list = ["detect-crop.sh", in_file_path]
        result = exec_cli(command_list)
        if result[2] != 0:
            pt_print_err(result[2])
            pt_print(result[1])
        else:
            progress_lines_list = result[1].split("\n")
            for cmd in progress_lines_list:
                if cmd != '':
                    pt_print(cmd, inset=2)
            crop_cmd_list = result[0].split("\n")
            for cmd in crop_cmd_list:
                if cmd != '':
                    pt_print(cmd, inset=2)
    except ptl.PTError, e:
        pt_print_err(e.value)

def main():
    """Script to enable batch operation of Don Melton's 'detect-crop.sh'.
    
    Don Melton includes a tool in his video transcoding script suite that is 
    used to check whether you need to set a special crop when Handbrake is
    transcoding. Unfortunately the tool does not accept multiple video files
    on the command line, this tool calls detect-video.sh with each of the 
    passed in files in turn.
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Batch script for detect-crop.sh."
    
    # parse options and args
    args = my_parse_args(description)
     
    if is_root():
        print >> sys.stderr, \
            get_script_name() + ": Error: This script must not run as root. "\
            "Exiting..."
        sys.exit(1)
    
    ptl.print_tool_desc('Batch operation for detect-crop.sh')
    
    for file_path in args.targets:
        if os.path.exists(file_path):
            if os.path.isfile(file_path):
                process_file(file_path, args)
            elif os.path.isdir(file_path):
                process_dir(file_path)
        else:
            pt_print_err("The file '" + file_path + "' "
                         "does not exist. Perhaps it is a broken symbolic "
                         "link or alias")
    
    # exit cleanly
    sys.exit(0)


### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

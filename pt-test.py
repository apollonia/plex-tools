#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
pt-mkv2mp4
    Script to store files which will be overwritten by a homebrew install.

Usage:
    pt-mkv2mp4 [-v]
    pt-mkv2mp4 [-h]
    pt-mkv2mp4 [-t -n NAME -s SEASON -e FIRST_EPISODE FILE [FILE ...]]
    pt-mkv2mp4 [-m FILE NAME YEAR [FILE NAME YEAR ...]]
"""

# standard library imports
import subprocess
import os
import sys
import optparse
from optparse import OptionParser
import re
import xml.etree.ElementTree as ET
from xml.dom import minidom
import plistlib


# package specific imports
from plextools import ptl
#from plextools import prefs
from plextools import ptprefs
from plextools.util import DataHolder, exec_cli


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()

MOUNT_REGEX = r"(.*@.*)( on )(.*)( \(.*\))"
raw_mount = """/dev/disk1s2 on / (hfs, local, journaled)
devfs on /dev (devfs, local, nobrowse)
/dev/disk0s2 on /Volumes/Old Bones (hfs, local, journaled)
/dev/disk3s2 on /Volumes/Internal Mirror (hfs, local, journaled)
/dev/disk2s2 on /Volumes/Time Machine Internal (hfs, local, journaled)
map -hosts on /net (autofs, nosuid, automounted, nobrowse)
map auto_home on /home (autofs, automounted, nobrowse)
//bofh@roadhouse.local/Shared%20Media on /Volumes/Shared Media (afpfs, nodev, nosuid, mounted by alan)
//bofh@roadhouse.local/bofh on /Volumes/bofh (afpfs, nodev, nosuid, mounted by alan)
"""

lines = raw_mount.split('\n')
line_count = 0
lines_len = len(lines)
while line_count < lines_len:
    list_line = lines[line_count]
    parsed_mount = re.search(MOUNT_REGEX, list_line)
    if parsed_mount is not None:
        print parsed_mount.groups()[0]
        print parsed_mount.groups()[2]
    line_count += 1

test_path = '/Users/alan/Temporary/mkvtest/Breaking_Bad_Season_4_Disc_1_t01.mp4'
print ptl.is_path_on_network_mount(test_path)
test_path = '/Users/alan/Temporary/mkvtest/output.mkv'
print ptl.is_path_on_network_mount(test_path)
test_path = 'The Pity of War -  p01nl00x default.mp4'
print ptl.is_path_on_network_mount(test_path)
test_path = '/Volumes/bofh/Downloads/iPlayer/BBC Proms - Chamber Music: 1. Vilde Frang and Michail Lifits b03p7p6n default.mp4'
print ptl.is_path_on_network_mount(test_path)
test_path = '/Volumes/bofh/Downloads/iPlayer/The Lives of Others - The Lives of Others b00r0sf7 default.mp4'
print ptl.is_path_on_network_mount(test_path)
test_path = '/Volumes/Shared Media/Plex Media Libraries/TV Shows/Amber/Season 1/Amber - s01e01 - Sarah.mp4'
print ptl.is_path_on_network_mount(test_path)
"""
Test Output
//bofh@roadhouse.local/Shared%20Media
/Volumes/Shared Media
//bofh@roadhouse.local/bofh
/Volumes/bofh
(False, None, None)
(False, None, None)
(False, None, None)
The path '/Volumes/bofh/Downloads/iPlayer/BBC Proms - Chamber Music: 1. Vilde Frang and Michail Lifits b03p7p6n default.mp4' is a network mount.
(True, '//bofh@roadhouse.local/bofh', '/Volumes/bofh')
The path '/Volumes/bofh/Downloads/iPlayer/The Lives of Others - The Lives of Others b00r0sf7 default.mp4' is a network mount.
(True, '//bofh@roadhouse.local/bofh', '/Volumes/bofh')
"""
sys.exit(0)

output = exec_cli(['ls', '-l'])[0]
text_file = open("local.txt", "w")
text_file.write(output)
text_file.close()

output = exec_cli(['ls', '-l'], host='localhost', user='alan')[0]
text_file = open("remote.txt", "w")
text_file.write(output)
text_file.close()

sys.exit(0)

def restore_mp4_metadata(in_file_path):
    """Restores extracted metadata to an MPEG-4 container file."""
    restore_cl = ['AtomicParsley', in_file_path]
    saved_md = ptl.get_saved_metadata(in_file_path)
    if saved_md is not None:
        for atom in ptl.NUISANCE_METADATA_ATOMS:
            if atom in saved_md:
                if (atom == ptl.EYETV_XML_LABEL) or \
                   (atom == ptl.EYETV_RATING_LABEL):
                    restore_cl.append('--rDNSatom')
                    restore_cl.append(saved_md[atom])
                    if (atom == ptl.EYETV_XML_LABEL):
                        restore_cl.append('name=iTunMOVI')
                    else:
                        restore_cl.append('name=iTunEXTC')
                    restore_cl.append('domain=com.apple.iTunes')
                else:
                    # Select the option flag to add
                    if atom == ptl.NAME_ID['atom']:
                        restore_cl.append('--title')
                    elif atom == ptl.SERIES_NAME_ID['atom']:
                        restore_cl.append('--TVShowName')
                    elif atom == ptl.YEAR_ID['atom']:
                        restore_cl.append('--year')
                    elif atom == ptl.DESC_ID['atom']:
                        restore_cl.append('--description')
                    elif atom == ptl.LONG_DESC_ID['atom']:
                        restore_cl.append('--longdesc')
                    elif atom == ptl.ARTIST_ID['atom']:
                        restore_cl.append('--artist')
                    elif atom == ptl.GENRE_ID['atom']:
                        restore_cl.append('--genre')
                    elif atom == ptl.ALBUM_ARTIST_ID['atom']:
                        restore_cl.append('--albumArtist')
                    # That's the option flag added now append the data
                    if (atom != 'type'):
                        restore_cl.append(saved_md[atom])
    covers = ptl.extracted_cover_art_exists(in_file_path)
    if covers is not None:
        dir_path = os.path.dirname(in_file_path)
        for cover_name in covers:
            restore_cl.append('--artwork')
            restore_cl.append(os.path.join(dir_path, cover_name))
    if len(restore_cl) > 2:
        ptl.exec_cli(restore_cl, ptl.fake_file_ops)

# try:x
#     my_proc = subprocess.Popen(["ls", "-l"], stdout=subprocess.PIPE)
#     ret_code = my_proc.returncode
#     (raw_output, raw_err) = my_proc.communicate()
#     ret_code = my_proc.returncode
# except:
#     pass
#     #return atom_dict
# output = unicode(raw_output, 'utf-8')
# lines = output.split('\n')
# 
# exit(0)

# in_file_path = '/Users/alan/Temporary/mkvtest/Breaking_Bad_Season_4_Disc_1_t01.mp4'
in_file_path = '/Users/alan/Temporary/mkvtest/output.mkv'
# in_file_path = 'The Pity of War -  p01nl00x default.mp4'
ptl.preserve_nuisance_metadata_from_file(in_file_path)
ptl.remove_nuisance_metadata_from_file(in_file_path)
# full_file_path = os.path.abspath(in_file_path)
# print full_file_path
# restore_mp4_metadata(full_file_path)
sys.exit(0)
# file_path_array = ["/Volumes/Shared Media/Temporary/The Good Wife/Season 5/The Good Wife - s05e01 - Everything Is Ending.m4v", 
#                    "/Volumes/Shared Media/Temporary/The Good Wife/Season 5/The Good Wife - s05e02 - The Bit Bucket.m4v", 
#                    "/Volumes/Shared Media/Temporary/The Good Wife/Season 5/The Good Wife - s05e03 - A Precious Commodity.m4v", 
#                    "/Volumes/Shared Media/Temporary/The Good Wife/Season 5/The Good Wife - s05e04 - Outside the Bubble.m4v"]
# for fix_file in file_path_array:
#     ptl.remove_nuisance_metadata_from_file(fix_file)
# 
# exit(0)
in_file_path = '/Users/alan/Temporary/mkvtest/output.mkv'
try:
    my_proc = subprocess.Popen(["mkvmerge", "--identify", file_path], 
                               stdout=subprocess.PIPE)
    (raw_output, raw_err) = my_proc.communicate()
    ret_code = my_proc.returncode
except:
    pass
    #return atom_dict
output = unicode(raw_output, 'utf-8')
lines = output.split('\n')
image_count = 0
extract_cl = ['mkvextract', 'attachments', in_file_path]
# _artwork_1.jpg
for list_line in lines:
    attach_search = re.search(r"^Attachment\sID\s([0-9])+:\stype\s"
                              r"\'image/(.+)\',\ssize\s[0-9]+\s"
                              r".*bytes, file name \'(.+)\'", 
                              list_line)
    if attach_search is not None:
        image_count += 1
        ext_str = str(image_count) + ':' + \
                  os.path.splitext(in_file_path)[0] + '_artwork_' + \
                  str(image_count), + '.' + attach_search.group(3)
        extract_cl.append(ext_str)
if image_count != 0:
    exec_cli(extract_cl, fake_file_ops)

exit(0)

# file_path = '/Users/alan/Do Not Backup/to watch/iplayer-downloads/iPlayer/For Your Consideration - 2006, Movie.m4v'
# file_path = 'How I Met Your Mother - 24-24, Something New.m4v'
# file_path = '/Users/alan/Do Not Backup/to watch/iplayer-downloads/iPlayer/How I Met Your Mother - 24-24, Something New.m4v'
file_path = '/Users/alan/Movies/BBC iPlayer/The Killing/Season 3/01 Episode 1.mp4'
# file_path = '/Users/alan/Music/iTunes/iTunes Music/TV Shows/The Killing/Season 3/01 Episode 1.mp4'
file_path_array = ['/Users/alan/Movies/BBC iPlayer/The Killing/Season 3/01 Episode 1.mp4',
                   'ToDo.txt',
                   "/Users/alan/Temporary/iplayer/Evita\'s\ Odyssey - Evita's Odyssey b01l05dq default.flv"]

md_dict = ptl.get_show_mkv_metadata(file_path)

print md_dict

exit(0)

#xml_output = subprocess.Popen(["mediainfo", file_path, "--output=XML"], 
#                                  stdout=subprocess.PIPE).communicate()[0]
# raw_output = subprocess.Popen(["AtomicParsley", file_path, "-t"], 
#                                   stdout=subprocess.PIPE).communicate()[0]
xml_output = md_dict[ptl.EYETV_XML_LABEL]
xmd_dict = plistlib.readPlistFromString(xml_output)
new_director = {'adamId': 0, 'name': 'Some Body'}
new_directors_array = [new_director]
xmd_dict['directors'] = new_directors_array
# xml_output = xml_output.replace('\n', '')
# root = ET.fromstring(xml_output)
# main_dict = root.find('dict')
# main_dict_iter = main_dict.iter()
# while True:
#     elem = next(main_dict_iter, None)
#     if elem != None:
#         if elem.text == 'directors':
#             # get the arrag of director dicts
#             directors_array = next(main_dict_iter, None)
#             if len(directors_array) == 0:
#                 director_dict = make_itunes_person_dict('Some One')
#                 directors_array.append(director_dict)
#     else:
#         break
# new_xml = minidom.parseString(ET.tostring(main_dict)).toprettyxml()
new_xml = plistlib.writePlistToString(xmd_dict)
# new_xml = ET.tostring(main_dict, encoding='UTF-8', method="xml")
# new_xml = xml_output.replace('Guest', 'Geust')

# Example of how to replace the XML (and other data)
raw_output = subprocess.Popen(["AtomicParsley", file_path, "--rDNSatom", 
                               new_xml, "name=iTunMOVI", 
                               "domain=com.apple.iTunes", "--manualAtomRemove",
                               "moov.udta.meta.ilst.©gen", "--title", "", 
                               "--artist", "", "--year", "", "--longdesc", "", 
                               "--description", "", "--contentRating", "",
                               "--TVShowName", "An Other Show", 
                               "--TVSeasonNum", "3", "--TVEpisodeNum", "3"], 
                               stdout=subprocess.PIPE).communicate()[0]
# How to clean out all nuisance metadata:
# raw_output = subprocess.Popen(["AtomicParsley", file_path, "--rDNSatom", 
#                                "", "name=iTunMOVI", "domain=com.apple.iTunes", 
#                                "--manualAtomRemove","moov.udta.meta.ilst.©gen", 
#                                "--title", "", "--artist", "", "--year", "", 
#                                "--longdesc", "", "--description", "", 
#                                "--contentRating", "", "--TVShowName", "", 
#                                "--artwork REMOVE_ALL", "--overWrite"],
#                                stdout=subprocess.PIPE).communicate()[0]


exit(0)
root = ET.fromstring(xml_output)
file_element = root.find('File')
track_iter = file_element.iter('track')
while True:
    track = next(track_iter, None)
    if track != None:
        if track.attrib['type'] == 'General':
            format_elem = track.find('Format')
            if format_elem != None:
                file_format = format_elem.text
                break
    else:
        file_format = 'No Format'
        break

print file_format
exit(0)
# d = c.find('track')
for d in c:
    print d.tag, d.attrib, d.get('type')
e = d.find('Complete_name')
print e.tag, e.attrib, e.text

exit(0)

for child in root:
    print child.tag, child.attrib
    print child.findtext('File')
    for grandchild in child:
        print grandchild.tag, grandchild.attrib
        print grandchild.findtext('track')
        for ggc in grandchild:
            print ggc.tag, ggc.attrib, ggc.text, ggc.tail
            print ggc.findtext('Complete_name')

exit(0)

my_list = ptl.MOVIE_TYPES

test_ustr = u'©lyr'
slen = len(test_ustr)
sc1 = test_ustr[0]
sslice = test_ustr[0:4]


#file_path = '/Users/alan/Movies/BBC iPlayer/Being Human/Season 4/"\
#            '01 1. Eve of the War.mp4'
file_path = '/Users/alan/Movies/BBC iPlayer/Movies/Junebug/01 Junebug.mp4'
#file_path = '/Users/alan/Music/iTunes/iTunes Music/Tv Shows/Being Human/'\
#            'Season 4/01 1. Eve of the War.mp4'

ptl.save_nuisance_mp4_metadata(file_path)

test = ptl.get_show_metadata(file_path)

exit(0)


def build_clean_command(file_path):
    cmd_list = ["AtomicParsley", file_path, '--overWrite', \
                '--artwork REMOVE_ALL']
    output = subprocess.Popen(["AtomicParsley", file_path, "-t"], 
                              stdout=subprocess.PIPE).communicate()[0]
    lines = output.split('\n')
    for list_line in lines:
        atom_test = re.search('^Atom\s\"([.]{4})\"\s', list_line)
        if atom_check != None:
            if not (atom_test.group(1) in [u'©too']):
                new_strip_atom_cmd = '--manualAtomRemove ' \
                                     '"moov.udta.meta.ilst.' + \
                                     atom_test.group(1) + '"'
            out_list.append(new_strip_atom_cmd)
    return out_list


exit(0)

test_file_name = 'Death in Paradise/ Series 3 - Episode 2 b03sh1fb default.mp4'
new_file_name = ptl._plex_tv_series_file_name(
            test_file_name, int(3), "Death in Paradise")


source_file_path = '/Users/alan/Documents/Development/MyShellScripts/iTunes/Plex Tools/pt-test.py'
make_alias_str = 'make new alias to file (posix file "' + \
                 source_file_path + \
                 '") at desktop',

try:
    subprocess.check_call(['osascript', '-e', 'tell application "Finder"',
                           '-e', 'make new alias to file (posix file "/Users/alan/Documents/Development/MyShellScripts/iTunes/Plex Tools/pt-test.py") at desktop',
                           '-e', 'end tell'])
except:
    print "Ooops!"
finally:
    print "Done!"

#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys

args = sys.argv[1:]

# read lines into array
with open(args[0], 'r') as srt_file: srt_lines = srt_file.readlines()
srt_file.close()
 
line_count = 0
sub_index = 0
# Strips the newline character
for line in srt_lines:
    line_count += 1
    if line_count == 1:
        sub_index += 1
        print(sub_index)
    else:
        print("{}".format(line.strip()))

    if line.strip() == '':
        line_count = 0

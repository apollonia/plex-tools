#! /usr/bin/env python
"""
Script to symlink files in the iTunes Music library on 'Media Store' into 
the Plex TV Shows library with appropriate renaming.

Assumes the library is on the on Roadhouse server. Intended to be used 
from a client with the 'Media Store' volume mounted.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
import colors
import ptl
import ptprefs
from ptprefs import PtPrefs
from ptl import pt_print, pt_print_err, ensure_nomedia
from util import switch, ensure_dir, exec_cli, get_script_name, \
                 is_root, same_partition


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.getversionstr(ptl.VERSION_INFO)

def process_series_dir(dir_path):
    """Iterates through dir_path calling process_season_dir() on season dirs"""
    pt_print("".join(["Scanning possible series directory '", colors.BOLD, 
             os.path.basename(dir_path), colors.END, "'..."]))
    dir_list = os.listdir(dir_path)
    season_dir_list = []
    for dir_obj in dir_list:
        if dir_obj[0] != '.':
            sub_dir_path = os.path.join(dir_path, dir_obj)
            if os.path.isdir(sub_dir_path):
                season_test_grep = re.search(
                    "Season[\W]+([0-9]+)", sub_dir_path)
                if ('NoneType' != season_test_grep.__class__.__name__):
                    # Regex has worked so name is correct for a season 
                    # directory. add it to season_dir_list
                    pt_print("".join(["Found probable series directory '", \
                             colors.BOLD, sub_dir_path, colors.END, "'..."]))
                    season_dir_list.append(sub_dir_path)
            else:
                # Print an error so user knows may need to check for video
                # file in the wrong place.
                pt_print_err("'" + colors.BOLD + file_path, + colors.END + 
                             "' is not a dir.")
            
            # Should now have a list of season directories in season_dir_list
            if len(season_dir_list) != 0:
                for season_dir in season_dir_list:
                    os.path.walk(season_dir, process_season_dir, None)
            else:
                pt_print_err("No season directories found in '" + colors.BOLD + 
                    dir_path + colors.END + "'.")

def process_season_dir(args, dir_path, dir_list):
    """Iterates through dir_path passing each file to process_file()"""
    pt_print("".join(["Processing season directory '", colors.BOLD, \
                   os.path.basename(dir_path), colors.END, "' of series '", \
                   colors.BOLD, os.path.basename(os.path.dirname(dir_path)), \
                   colors.END, "'..."]))
    for dir_obj in dir_list:
        if dir_obj[0] != '.':
            file_path = os.path.join(dir_path, dir_obj)
            if os.path.isfile(file_path):
                process_file(file_path, args)
            else:
                pt_print_err("'" + colors.BOLD + file_path + colors.END +
                             "' is not a file.")

def process_dir(dir_path, args):
    """Tests if dir_path is a series or season dir and calls correct handler"""
    full_dir_path = os.path.abspath(dir_path)
    (par_dir_path, dir_name) = os.path.split(full_dir_path)
    season_test_grep = re.search("Season[\W]+([0-9]+)", dir_name)
    if ('NoneType' != season_test_grep.__class__.__name__):
        # Regex has worked so name is correct for a season directory, pass
        # it on to season directory handler
        os.path.walk(dir_path, process_season_dir, args)
    else:
        # Not a season dir so pass to series dir handler
        process_series_dir(dir_path)

def process_file(file_path, args):
    """Loads the FileInfo structure and passes it to the relevant function"""
    pt_print("".join(["Analysing '", colors.BOLD, os.path.basename(file_path), 
             colors.END, "'"]), inset=1)
    try:
        file_info = ptl.FileInfo(file_path, args)
        #ptl.fill_file_info(file_info)
        if file_info.validate():
            if file_info.name_type in ptl.TV_TYPES:
                link_tv_file_to_plex(file_info, args)
            elif file_info.name_type in ptl.MOVIE_TYPES:
                link_movie_file_to_plex(file_info, args)
    except ptl.PTError, e:
        pt_print_err(e.value)


def link_movie_file_to_plex(file_info, args):
    """Links the given TV Series iTunes Library file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    movie_dir_name = file_info.movie_dir_name()
    movie_svr = PtPrefs().get_movie_server(args.alt_server)
    local_dest_full_path = os.path.join(movie_svr.local_path(), 
                                        movie_dir_name, new_file_name)
    link_file_to_plex(file_info, local_dest_full_path, args)

def link_tv_file_to_plex(file_info, args):
    """Links the given TV Series iTunes Library file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    tv_svr = PtPrefs().get_tv_server(args.alt_server)
    # ensure the series and season folders exist in the Plex 
    # library
    local_dest_full_path = os.path.join(tv_svr.local_path(), 
                                        file_info.show_name, 
                                        "Season " + file_info.season_num, 
                                        new_file_name)
    link_file_to_plex(file_info, local_dest_full_path, args)

def link_file_to_plex(file_info, local_dest_full_path, args):
    """Links the iTunes Library file into the Plex library at the given path"""
    pt_print("Linking file '" + colors.BOLD + file_info.file_name + 
             colors.END + "' to Plex library", inset=1)
    if os.path.exists(local_dest_full_path) and (not args.force):
        pt_print(colors.FG_RED +  "Error:" + colors.END + " the file '" + 
                 colors.BOLD + local_dest_full_path + colors.END + 
                 "' already exists. Use -f/--force to overwrite.", inset=2)
    else:
        try:
            if ensure_dir(local_dest_full_path, ptl.fake_file_ops):
                pt_print("Creating missing '" + 
                         os.path.dirname(local_dest_full_path) + 
                         "' directory", inset=1)
            if (file_info.name_type == ptl.TYPE_PLEX_EXTRA['id']):
                # an extra, ensure '.nomedia file is present
                ensure_nomedia(local_dest_full_path)
            # Create the link if required
            svr_src_dir_path = file_info.dir_path
            svr_src_file_path = os.path.join(svr_src_dir_path, 
                                             file_info.file_name)
            # Does file already exist?
            if os.path.lexists(local_dest_full_path):
                # iI a real file don't want to overwrite with a link.
                if not os.path.islink(local_dest_full_path):
                    raise ptl.PTError( 
                        "File '" + local_dest_full_path + "' already exists in "\
                        "Plex library and is a real file not a link.")
                # If already links the source file no need to re-create.
                if os.path.islink(local_dest_full_path):
                    existing_link_path = os.readlink(local_dest_full_path)
                    if (svr_src_file_path == existing_link_path):
                        raise ptl.PTError( 
                            "File '" + local_dest_full_path + "' already exists "\
                            "in Plex library and already links '" + 
                            svr_src_file_path + "' into the library.")
            # create link
            new_file_name = os.path.basename(local_dest_full_path)
            # create hard links be default
            link_args = '-f'
            if not same_partition(svr_src_file_path, 
                                  os.path.dirname(local_dest_full_path)):
                # not on same volume, create symbolic link
                link_args = '-sf'
            pt_print("Linking as '" + colors.BOLD + new_file_name + colors.END + 
                     "'...", inset=1)
            ptl.exec_cli(["ln", link_args, svr_src_file_path, 
                          local_dest_full_path], ptl.fake_file_ops)
            ptl.add_matching_local_media(svr_src_file_path, local_dest_full_path,
                                  'ln', [link_args])
            if args.clean_md:
                if ptl.check_for_nuisance_metadata(file_info.full_file_path):
                    # Remove but preserve nuisance metadata from the linked file.
                    ptl.clean_nuisance_metadata_safely(file_info.full_file_path)
        except ptl.PTError, e:
            # Catch exception thrown by my code
            pt_print_err(e.value, inset=1)
        except:
            # Catch unexpected exceptions. Exit as no idea of state.
            pt_print_err("Unexpected error while processing '" + colors.BOLD + 
                         file_info.full_file_path + colors.END + "'. Exiting...",
                         inset=1)
            import traceback
            traceback.print_exc()
            sys.exit(1)

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptln() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools link module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptln(args)
    
    # exit cleanly
    sys.exit(0)


def ptln(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    
    ptl.print_tool_desc('Link tool')
    
    ptl.tool_main('ptln', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

"""
__init__.py
    package __init__ for the plextools package.
"""

# standard library imports

# package specific imports
import ptl
import util


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2010 Alan Staniforth"
__license__ = "New-style BSD"
__version__ = ptl.get_plextools_version_string()


#### Interface
__all__ = [ # Modules
            'colors', 'prefs', 'ptprefs', 'ptl', 'util'
]

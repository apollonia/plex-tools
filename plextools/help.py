# -*- coding: utf-8 -*-
"""Simple help system for Plex Tools"""

class Help(object):
    """
    Exception class for use by Plex Tools routines
    
    Instantiate with an error string
    
    """
    def __init__(self, title, help_text=None, root=False):
        if (title is None) or (title == ''):
            raise Exception('A help object *must* have a title!')
        self.title = title
        self.help_text = help_text
        self.root = root
    
    def __str__(self):
        if self.root:
            out_text = ['Root help text objject, title: "', self.title, '"']
        else:
            out_text = ['Help text objject, title: "', self.title, '"']
        return ''.join(out_text)
    
    

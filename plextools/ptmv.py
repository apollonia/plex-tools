#! /usr/bin/env python
"""
Script to move video files into the Plex TV Shows library.

Assumes the library is on the on Roadhouse server. Intended to be used 
from a client with the 'Media Store' volume mounted.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
import colors
import ptl
import ptprefs
from ptprefs import PtPrefs
from ptl import pt_print, pt_print_err, ensure_nomedia
from util import switch, ensure_dir, exec_cli, get_script_name, \
                 is_root


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()


def process_file(file_path, args):
    """Loads the FileInfo structure and passes it to the relevant function"""
    try:
        pt_print("".join(["Analysing '", colors.BOLD, 
                 os.path.basename(file_path), colors.END, "'"]), inset=1)
        file_info = ptl.FileInfo(file_path, args)
        if file_info.validate():
            if file_info.name_type in ptl.TV_TYPES:
                move_tv_file_to_plex(file_info, args)
            elif file_info.name_type in ptl.MOVIE_TYPES:
                move_movie_file_to_plex(file_info, args)
        else:
            pt_print_err("Insufficient information to create a Plex name for "
                         "file '" + colors.BOLD + os.path.basename(file_path) +
                         colors.END + "'.")
    except ptl.PTError, e:
        pt_print_err(e.value)

def move_tv_file_to_plex(file_info, args):
    """Moves the given TV series file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    tv_svr = PtPrefs().get_tv_server(args.alt_server)
    local_dest_full_path = os.path.join(tv_svr.local_path(), 
                                        file_info.show_name, 
                                        "Season " + file_info.season_num, 
                                        new_file_name)
    move_file_to_plex(file_info, local_dest_full_path, args)

def move_movie_file_to_plex(file_info, args):
    """Moves the given movie file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    movie_dir_name = file_info.movie_dir_name()
    movie_svr = PtPrefs().get_movie_server(args.alt_server)
    local_dest_full_path = os.path.join(movie_svr.local_path(), 
                                        movie_dir_name, new_file_name)
    move_file_to_plex(file_info,local_dest_full_path, args)

def move_file_to_plex(file_info, local_dest_full_path, args):
    """
    Moves the file into the Plex Library at the given path
    
    If it is a file type that contains mpeg-4 metadata then the function 
    also deletes those atoms from the file that may confuse the Plex 
    server by over-riding the video data implicit in the file name. This is 
    necessary because sometimes that atomic data is wrong, or truncated from 
    what it should be.
    """
    pt_print("Moving file '" + colors.BOLD + file_info.file_name + 
             colors.END + "' to Plex library", inset=1)
    if not ptl.are_paths_on_same_local_vol(file_info.full_file_path, 
                                       local_dest_full_path):
        pt_print(colors.BOLD +  "Error:" + colors.END + " '" + colors.BOLD + 
                 file_info.full_file_path + colors.END + "' and '" + 
                 colors.BOLD + local_dest_full_path + colors.END + 
                 "' are not on the same local volume - use 'cp' "
                 "instead of 'mv'.", 
                 inset=2)
    elif os.path.exists(local_dest_full_path) and not args.force:
        pt_print(colors.BOLD +  "Error:" + colors.END + " '" + colors.BOLD + 
                 local_dest_full_path + colors.END + "' already exists in "
                 "the Plex library.", 
                 inset=2)
    else:
        if ensure_dir(local_dest_full_path, ptl.fake_file_ops):
            pt_print("Creating missing '" + 
                     os.path.dirname(local_dest_full_path) + 
                     "' directory", inset=1)
        if (file_info.name_type == ptl.TYPE_PLEX_EXTRA['id']):
            # an extra, ensure '.nomedia file is present
            ensure_nomedia(local_dest_full_path)
        new_file_name = os.path.basename(local_dest_full_path)
        cli_list = ["mv", file_info.full_file_path, local_dest_full_path]
        pt_print("Moving to '" + colors.BOLD + new_file_name + colors.END + 
                 "'...", inset=1)
        exec_cli(cli_list, ptl.fake_file_ops)
        pt_print("Move complete.", inset=1)
        ptl.add_matching_local_media(file_info.full_file_path, local_dest_full_path,
                              'mv')
        if args.clean_md:
            if ptl.check_for_nuisance_metadata(local_dest_full_path):
                ptl.remove_nuisance_metadata_from_file(local_dest_full_path)

def is_mv_efficient(in_path1, in_path2):
    """
    Tests if 'mv' is less efficient than 'cp'
    
    If the source and destination files are on the same server but on
    different volumes mounted from that server then a simple 'mv' is 
    very inefficent because the file has to be copied off the source 
    mount and written to the source volume so traversing the network
    twice.
    
    in that case it is much more efficient to use a remote command on 
    the server. Unfortunately we can't use a 'mv' on the server - it works 
    but for some reason - possibly 
    
    """
    ret_val = True
    if ptl.are_paths_on_same_server(in_path1, in_path2):
        if not are_paths_on_the_same_volume(in_path1, in_path2):
            ret_val = False
    return ret_val

def process_dir(dir_path, args):
    print >> sys.stderr, \
    get_script_name() + ": Error: The path '" + dir_path + "' is a directory "\
    "not a file. I do not (yet) handle directories"

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptcp() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools copy module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptmv(args)
    
    # exit cleanly
    sys.exit(0)

def ptmv(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    
    ptl.print_tool_desc('Move tool')
    
    ptl.tool_main('ptmv', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

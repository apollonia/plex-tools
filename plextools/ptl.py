# -*- coding: utf-8 -*-
"""Utility routines and constants for the plex tools suite."""

# standard library imports
import os
import sys
import re
import subprocess
import cPickle
import xml.etree.ElementTree as ET
import argparse
from argparse import ArgumentParser
import importlib
import uuid
import tempfile
import copy
import fnmatch
import string

# Third party imports

# package specific imports
from util import switch, getversionstr, exec_cli, DataHolder, is_root, \
                 get_script_name, ensure_dir, split_path, \
                 remove_leading_zeroes, CommandAction, ISO_639_1_CODES, \
                 ISO_639_2B_CODES
import colors
from ptprefs import PtPrefs


# Atributes -------------------------------------------------------------------
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2010 Alan Staniforth"
__license__ = "New-style BSD"
# same format as sys.version_info: "A tuple containing the five components of
# the version number: major, minor, micro, releaselevel, and serial. All
# values except release level are integers; the release level is 'alpha',
# 'beta', 'candidate', or 'final'. The version_info value corresponding to the
# Python version 2.0 is (2, 0, 0, 'final', 0)."  Additionally we use a
# releaselevel of 'dev' for unreleased under-development code.
VERSION_INFO = (1, 1, 0, 'dev', 1)
__version__ = getversionstr(VERSION_INFO)


# Interface -------------------------------------------------------------------
__all__ = [  # Classes ###
            'PTError', 'FileInfo',

            # Functions ###
            'detect_file_name_type', 'get_show_metadata',
            'get_plextools_version_string', 'get_plex_ident',
            'add_matching_local_media', 'mediainfo_xml_option',

            # Constants ###
            # Regex's
            'FILE_EXTN_REGEX', 'IPLAYER_BASE_REGEX', 'IPLAYER_1_REGEX',
            'IPLAYER_2_REGEX', 'IPLAYER_3_REGEX', 'IPLAYER_4_REGEX',
            'MKMKV_TV_REGEX', 'EYETV_TV_REGEX', 'EYETV_MOVIE_REGEX',
            'ITUNES_MOVIE_REGEX', 'ITUNES_TV_REGEX', 'PLEX_TV_REGEX',
            'PLEX_MOVIE_REGEX',
            # Type constants
            'IPLAYER_TYPE_1', 'IPLAYER_TYPE_2', 'IPLAYER_TYPE_3',
            'IPLAYER_TYPE_4', 'IPLAYER_TYPES', 'TYPE_MAKEMKV_TV_RIP',
            'TYPE_EYETV_TV', 'TYPE_EYETV_MOV_1', 'TYPE_ITUNES_MOVIE',
            'TYPE_ITUNES_TV', 'MOVIE_TYPES', 'TV_TYPES',
            # Metadata identifier constant dictionaries
            'SERIES_NAME_ID', 'SEASON_ID', 'EP_NUM_ID', 'NAME_ID',
            'COMMENT_ID', 'DESC_ID', 'LYRICS_ID', 'RDNS_ID',
            'EYETV_XML_ID', 'EYETV_XML_LABEL', 'EYETV_RATING_ID',
            'EYETV_RATING_LABEL',
            # Misc constants
            'VERSION_INFO',

            # Flags ###
            'fake_file_ops'
            ]


# Constants -------------------------------------------------------------------
FILE_EXTN_REGEX = r'\.([0-9a-zA-Z]+)$'
"""Reasonably reliable regex to pull out a file extension"""

EYETV_MOVIE_STEM = r'^(.*) - ([0-9]{4}), '
EYETV_MOVIE_REGEX = EYETV_MOVIE_STEM + r'Movie' + FILE_EXTN_REGEX + r'|' + \
                    EYETV_MOVIE_STEM + r'Other' + FILE_EXTN_REGEX
"""EyeTV movie export
= '<Movie Title> - <movie year>, Movie.<ext>' or
= '<Movie Title> - <movie year>, Other.<ext>'"""

EYETV_TV_REGEX = '(.+)( - ([0-9]{1,4})[-:]([0-9]{1,4}), )(.*)' + \
                 FILE_EXTN_REGEX
"""EyeTV tv series export
= '<Series name> - <episode num>-<total episodes>, <Episode Title>.<ext>'"""

IPLAYER_BASE_REGEX = r'(\s[0-9a-z]{8}\sdefault)' + FILE_EXTN_REGEX
"""All get_iplayer downloads end in ' <a1b2c3d4> default.<ext>'"""

IPLAYER_1_REGEX = r'^(.*[\w]): Series ([0-9]+) - (.*)' + \
                  IPLAYER_BASE_REGEX
"""get_iplayer type 1 format
= '<Series Name>: Series <num> - <Episode info> <a1b2c3d4> default.<ext>'"""

IPLAYER_2_REGEX = r'^(.*[\w]) - ([0-9a-zA-Z\s\-]+: )(.*)' + IPLAYER_BASE_REGEX
"""get_iplayer type 2 format
= '<Series Name> - <Season info>: <Episode info> <a1b2c3d4> default.<ext>'"""

IPLAYER_3_REGEX = r'^(.*[\w]) - (.*)' + IPLAYER_BASE_REGEX
"""get_iplayer type 3 format
= '<Series name> - <Episode Title> <a1b2c3d4> default.<ext>'"""

IPLAYER_4_REGEX = r'^(.*[\w]) - \1' + IPLAYER_BASE_REGEX
"""get_iplayer type 4 format - movie
= '<Movie name> - <Movie name> <a1b2c3d4> default.<ext>'"""

ITUNES_MOVIE_REGEX = r'^([0-9]{2,}?\s)?(.*)' + FILE_EXTN_REGEX
"""iTunes Movie = '[<nn> ]<Movie Title>.<ext>'
Obviously this pattern will match any filename with an extension so it is of
very limited use for screening purposes. However the second match group is the
movie title and all you need to do is check if the enclosing folder name
matches the title."""

ITUNES_TV_REGEX = r'^([0-9]+) (.*)' + FILE_EXTN_REGEX
"""iTunes TV Series episode
= '<nn> <Episode name>.<ext>'"""

MKMKV_TV_REGEX = r'^([a-zA-Z0-9_]+?)[-_]{0,2}_Season_'\
                 r'([0-9]+)[-_]+Disc_([0-9]+)_t[0-9]{2}' + FILE_EXTN_REGEX
"""MakeMKV DVD/Blu-ray TV Series Rip
= '<Series_name>_[-_]Season_<num>_[-_]Disc_<num>_t<nn>.<ext>'"""

PLEX_TV_REGEX = r'(.*)( - )(s([0-9]{2,})e([0-9]{2,})(-e([0-9]{2,}))*?)(( - )'\
                + r'(.*))*' + FILE_EXTN_REGEX
"""Plex TV Library
= '<Series Title> - sXXeYY[-eZZ][ - Episode Name].<ext>'"""

PLEX_MOVIE_REGEX = r'(.*) \(([0-9]{4})\)([ ]*[-.][ ]*(.*))*' + FILE_EXTN_REGEX
"""Plex Movie Library
= '<Movie Title> (YYYY)[[ ]-[ ]<part or version signifier].<ext>'"""

PLEX_EXTRA_REGEX = r'(.*)-(behindthescenes|deleted|featurette|interview|'\
                   r'scene|short|trailer|other)' + FILE_EXTN_REGEX
"""Plex Movie Library Extra
= '<filename>-<extra type signifier>.<ext>'"""

TYPE_GENERIC_TV = {'id': 'tv', 'desc': 'generic TV show'}
TYPE_GENERIC_MOVIE = {'id': 'movie', 'desc': 'generic movie'}
TYPE_EYETV_TV = {'id': 'eyetvtv', 'desc': 'EyeTV exported TV show'}
TYPE_EYETV_MOV_1 = {'id': 'eyetvmovie1', 'desc': 'EyeTV exported movie'}
TYPE_EYETV_MOV_2 = {'id': 'eyetvmovie2', 'desc': 'EyeTV exported unrecognised '
                    'movie'}
IPLAYER_TYPE_1 = {'id': 'iplayer1', 'desc': 'get_iplayer type 1 - TV show'}
IPLAYER_TYPE_2 = {'id': 'iplayer2', 'desc': 'get_iplayer type 2 - TV show'}
IPLAYER_TYPE_3 = {'id': 'iplayer3', 'desc': 'get_iplayer type 3 - TV show'}
IPLAYER_TYPE_4 = {'id': 'iplayer4', 'desc': 'get_iplayer type 4 = movie'}
IPLAYER_TYPES = [IPLAYER_TYPE_1['id'], IPLAYER_TYPE_2['id'],
                 IPLAYER_TYPE_3['id'], IPLAYER_TYPE_4['id']]
TYPE_ITUNES_TV = {'id': 'ittv', 'desc': 'iTunes library TV show'}
TYPE_ITUNES_MOVIE = {'id': 'itmovie', 'desc': 'iTunes library movie'}
TYPE_MAKEMKV_TV_RIP = {'id': 'mkvtv', 'desc': 'MakeMKV DVD or Blu-ray rip'}
TYPE_PLEX_TV = {'id': 'plextv', 'desc': 'Plex TV library show'}
TYPE_PLEX_MOVIE = {'id': 'plexmovie', 'desc': 'Plex library movie'}
TYPE_PLEX_EXTRA = {'id': 'extra', 'desc': 'Plex movie library extra'}
TV_TYPES = [TYPE_EYETV_TV['id'], IPLAYER_TYPE_1['id'], IPLAYER_TYPE_2['id'],
            IPLAYER_TYPE_3['id'], TYPE_ITUNES_TV['id'],
            TYPE_MAKEMKV_TV_RIP['id'], TYPE_PLEX_TV['id'],
            TYPE_GENERIC_TV['id']]
MOVIE_TYPES = [TYPE_EYETV_MOV_1['id'], TYPE_EYETV_MOV_2['id'],
               IPLAYER_TYPE_4['id'], TYPE_ITUNES_MOVIE['id'],
               TYPE_ITUNES_MOVIE['id'], TYPE_GENERIC_MOVIE['id'],
               TYPE_PLEX_MOVIE['id'], TYPE_PLEX_EXTRA['id']]
MOVIE_EXTRA_TYPE = [TYPE_PLEX_EXTRA['id']]
METADATA_TV_TYPES = [TYPE_EYETV_TV['id'], TYPE_ITUNES_TV['id'],
                     TYPE_MAKEMKV_TV_RIP['id'], TYPE_PLEX_TV['id']]
METADATA_MOVIE_TYPES = [TYPE_EYETV_MOV_1['id'], TYPE_ITUNES_MOVIE['id'],
                        TYPE_PLEX_MOVIE['id']]
METADATA_FILE_TYPES = METADATA_TV_TYPES + METADATA_MOVIE_TYPES

NAME_TYPE_ARRAY = [TYPE_GENERIC_TV, TYPE_GENERIC_MOVIE, TYPE_EYETV_TV,
                   TYPE_EYETV_MOV_1, TYPE_EYETV_MOV_2, IPLAYER_TYPE_1,
                   IPLAYER_TYPE_2, IPLAYER_TYPE_3, IPLAYER_TYPE_4,
                   TYPE_ITUNES_TV, TYPE_ITUNES_MOVIE, TYPE_MAKEMKV_TV_RIP,
                   TYPE_PLEX_TV, TYPE_PLEX_MOVIE, TYPE_PLEX_EXTRA]

# AtomicParsley ('atom') and mediainfo ('tag') identifiers
# mediainfo identifiers are sub-tags of the:
#   <Mediainfo version="0.7.65">
#       <File>
#           <track type="General"> tag.
# and are produced by invoking mediainfo with the --Output=XML option
EYETV_XML_LABEL = u'iTunMOVI'
EYETV_RATING_LABEL = u'iTunEXTC'
SERIES_NAME_ID = {'atom': u'tvsh', 'tag': None, 'desc': 'Show name'}
SEASON_ID = {'atom': u'tvsn', 'tag': 'Season', 'desc': 'Season number'}
EP_NUM_ID = {'atom': u'tves', 'tag': 'Part', 'desc': 'Episode number'}
ARTIST_ID = {'atom': u'©ART', 'tag': 'Performer', 'desc': 'Artist'}
NAME_ID = {'atom': u'©nam', 'tag': 'Movie_name', 'desc': 'Episode name'}
YEAR_ID = {'atom': u'©day', 'tag': 'Recorded_date', 'desc': 'Release year'}
LYRICS_ID = {'atom': u'©lyr', 'tag': 'Lyrics', 'desc': 'iTunes lyrics'}
GENRE_ID = {'atom': u'©gen', 'tag': 'Genre', 'desc': 'Genre'}
COMMENT_ID = {'atom': u'©cmt', 'tag': 'Comment', 'desc': 'Comment'}
DESC_ID = {'atom': u'desc', 'tag': 'Description', 'desc': 'Description'}
LONG_DESC_ID = {'atom': u'ldes', 'tag': 'LongDescription',
                'desc': 'Long description'}
ALBUM_ARTIST_ID = {'atom': u'aART', 'tag': 'Album_Performer',
                   'desc': 'Album performer'}
ALBUM_ID = {'atom': u'©alb', 'tag': 'Album', 'desc': 'Album'}
COVER_ID = {'atom': u'covr', 'tag': 'Cover', 'desc': 'Cover art'}
MEDIAKIND_ID = {'atom': u'stik', 'tag': 'ContentType', 'desc': 'Media kind'}
EP_ID_ID = {'atom': u'tven', 'tag': 'Part_ID', 'desc': 'Episode ID'}
COPYRIGHT_ID = {'atom': u'cprt', 'tag': 'Copyright', 'desc': 'Copyright'}
RDNS_ID = {'atom': u'----', 'tag': None, 'desc': None}
EYETV_XML_ID = {'atom': u'com.apple.iTunes;' + EYETV_XML_LABEL,
                'tag': str(EYETV_XML_LABEL), 'desc': 'iTunes extended info'}
EYETV_RATING_ID = {'atom': u'com.apple.iTunes;' + EYETV_RATING_LABEL,
                   'tag': 'ContentRating', 'desc': 'EyeTV rating'}

TYPE_ATOM = 'type'
ORDER_ATOM = 'typeorder'

META_IDS = [
    SERIES_NAME_ID,
    SEASON_ID,
    EP_NUM_ID,
    ARTIST_ID,
    NAME_ID,
    YEAR_ID,
    LYRICS_ID,
    GENRE_ID,
    COMMENT_ID,
    DESC_ID,
    LONG_DESC_ID,
    ALBUM_ARTIST_ID,
    ALBUM_ID,
    COVER_ID,
    MEDIAKIND_ID,
    EP_ID_ID,
    COPYRIGHT_ID,
    RDNS_ID,
    EYETV_XML_ID,
    EYETV_RATING_ID
]

NUISANCE_METADATA_ATOMS = [NAME_ID['atom'], SERIES_NAME_ID['atom'],
                           YEAR_ID['atom'], DESC_ID['atom'],
                           LONG_DESC_ID['atom'], EYETV_XML_LABEL,
                           EYETV_RATING_LABEL, ARTIST_ID['atom'],
                           GENRE_ID['atom'], ALBUM_ARTIST_ID['atom'],
                           COPYRIGHT_ID['atom']]

EXTRA_FLAGS = ['behindthescenes', 'deleted', 'featurette', 'interview',
               'scene', 'short', 'trailer', 'other']

PLEX_MOVIE_EXTRA_FOLDERS = {EXTRA_FLAGS[0]: 'Behind The Scenes',
                            EXTRA_FLAGS[1]: 'Deleted Scenes',
                            EXTRA_FLAGS[2]: 'Featurettes',
                            EXTRA_FLAGS[3]: 'Interviews',
                            EXTRA_FLAGS[4]: 'Scenes',
                            EXTRA_FLAGS[5]: 'Shorts',
                            EXTRA_FLAGS[6]: 'Trailers',
                            EXTRA_FLAGS[7]: 'Other'}

USEFUL_METADATA_ATOMS = [NAME_ID['atom'], SERIES_NAME_ID['atom'],
                         YEAR_ID['atom'], EP_NUM_ID['atom'], SEASON_ID['atom']]

SUBTITLE_EXTENSIONS = ['srt', 'smi', 'ssa', 'ass']

ARTWORK_EXTENSIONS = ['jpg', 'jpeg', 'png', 'tbn']

MUSIC_EXTENSIONS = ['mp3']

POSTER_SFX = 'poster'
FANART_SFX = 'fanart'
BANNER_SFX = 'banner'
SBANNER_SFX = 'sbanner'
SEASON_SFX = 'season'
THEME_SFX = 'theme'
LOCAL_MEDIA_SUFFIXES = [POSTER_SFX, FANART_SFX, BANNER_SFX, SBANNER_SFX,
                        SEASON_SFX, THEME_SFX]

# mediainfo 'Format' tag text values (needs --Output=XML) Another
#   <Mediainfo version="0.7.65">
#       <File>
#           <track type="General">
# subtag
FLV_CONTAINER = 'Flash Video'
MKV_CONTAINER = 'Matroska'
MP4_CONTAINER = 'MPEG-4'
WMV_CONTAINER = 'Windows Media'
UKNOWN_CONTAINER = 'Unknown'


# Flags -----------------------------------------------------------------------
fake_file_ops = False


# Classes ---------------------------------------------------------------------

class PTError(Exception):
    """
    Exception class for use by Plex Tools routines.

    Instantiate with an error string
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class MetadataStore():
    """
    Class to store video metadata in pickled form.

    Version 1.0.0 only stores mpeg-4 metadata
    """
    def __init__(self, in_data={}):
        self.__version__ = (1, 0, 1)
        self.md = in_data


class NameInfo():
    """
    Class to hold the information which can be extracted from a filename.

    Instantiate with a file name

    Information instance variables, all initialised to None:

    name        - the TV Show/Movie name.
    season_num  - the season number for a TV show
    episode_num - the episode number for a TV show
    label       - the episode name for a TV show
    movie_year  - the release year for a movie

    """
    def __init__(self, in_file_name):
        # Prime the instance variables with None
        self.file_name = os.path.basename(in_file_name)
        self.dir_path = os.path.dirname(os.path.abspath(in_file_name))
        self.name = None
        self.season_num = None
        self.episode_num = None
        self.episode_range = None
        self.label = None
        self.movie_year = None
        self.extra_type = None
        # Initialise
        self._extract_name_info(in_file_name)

    def __str__(self):
        return '\n'.join([
            'Name: ' + str(self.name),
            'Season number: ' + str(self.season_num),
            'Episode number: ' + str(self.episode_num),
            'Label: ' + str(self.label),
            'Movie year: ' + str(self.movie_year)
        ])

    def _extract_name_info(self, in_file_name):
        """
        Identify file name type and call correct method to load ivars.

        Return None if cannot extract the requested data.
        """
        name_type = detect_file_name_type(in_file_name, False)
        if name_type is not None:
            for case in switch(name_type):
                if case(TYPE_EYETV_MOV_1['id']):
                    pass
                if case(TYPE_EYETV_MOV_2['id']):
                    self._info_from_eyetv_movie_filename()
                    break
                if case(TYPE_EYETV_TV['id']):
                    self._info_from_eyetv_tv_filename()
                    break
                if case(IPLAYER_TYPE_1['id']):
                    self._info_from_iplayer_type_1_filename()
                    break
                if case(IPLAYER_TYPE_2['id']):
                    self._info_from_iplayer_type_2_filename()
                    break
                if case(IPLAYER_TYPE_3['id']):
                    self._info_from_iplayer_type_3_filename()
                    break
                if case(IPLAYER_TYPE_4['id']):
                    self._info_from_iplayer_type_4_filename()
                    break
                if case(TYPE_ITUNES_MOVIE['id']):
                    self._info_from_itunes_movie_filename()
                    break
                if case(TYPE_ITUNES_TV['id']):
                    self._info_from_itunes_tv_filename()
                    break
                if case(TYPE_MAKEMKV_TV_RIP['id']):
                    self._info_from_makemkv_filename()
                    break
                if case(TYPE_PLEX_MOVIE['id']):
                    self._info_from_plex_movie_filename()
                    break
                if case(TYPE_PLEX_TV['id']):
                    self._info_from_plex_tv_filename()
                    break
                if case(TYPE_PLEX_EXTRA['id']):
                    self._info_from_plex_extra_filename()
                    break

    def _info_from_eyetv_movie_filename(self):
        """
        Extract info about a show from an EyeTV movie format file name.

        EyeTV movie format:
            '<Movie Title> - <movie year>, Movie.<ext>' or
            '<Movie Title> - <movie year>, Other.<ext>'

        This file name form only has information to fill 'name' and 'year.
        """
        parsed_name = re.search(EYETV_MOVIE_REGEX, self.file_name)
        if parsed_name is not None:
            if parsed_name.group(1) is not None:
                self.name = parsed_name.group(1)
                self.movie_year = parsed_name.group(2)
            else:
                self.name = parsed_name.group(4)
                self.movie_year = parsed_name.group(5)

    def _info_from_eyetv_tv_filename(self):
        """
        Extract info about a show from an EyeTV TV show format file name.

        EyeTV TV Show format:
        '<Series name> - <episode num>-<total episodes>, <Episode Title>.<ext>'

        This file name form only has information to fill 'name', 'episode'
        and 'label'.
        """
        parsed_name = re.search(EYETV_TV_REGEX, self.file_name)
        if parsed_name is not None:
            self.name = parsed_name.group(1)
            self.episode_num = parsed_name.group(3)
            self.label = parsed_name.group(5)

    def _info_from_iplayer_type_1_filename(self):
        """
        Extract info about a show from a get_iplayer type 1 file name.

        get_iplayer type 1 format:
        '<Series Name>/ Series <num> - <Episode info> <a1b2c3d4> default.<ext>'
        the <Episode info> may be 'Episode <N>', '<N>. An Episode Title' or
        'An Episode Title'

        This file name form potentially has all the information except 'year'
        """
        parsed_name = re.search(IPLAYER_1_REGEX, self.file_name)
        if parsed_name is not None:
            self.name = parsed_name.group(1)
            self.season_num = parsed_name.group(2)
            episode_str = parsed_name.group(3)
            ep_test1 = re.search('Episode\s([0-9]+)', episode_str)
            ep_test2 = re.search('([0-9]+)\.\s(.*)', episode_str)
            if ep_test1 is not None:
                self.episode_num = ep_test1.group(1)
            elif ep_test2 is not None:
                self.episode_num = ep_test2.group(1)
                self.label = ep_test2.group(2)
            else:
                self.label = episode_str

    def _info_from_iplayer_type_2_filename(self):
        """
        Extract info about a show from a get_iplayer type 2 file name.

        get_iplayer type 2 format:
        '<Series Name> - <Season info>/ <[<Episode Num>. ]Episode name>
          <a1b2c3d4> default.<ext>'

        This file name form potentially has information to fill 'name',
        'episode' and 'label'.
        """
        parsed_name = re.search(IPLAYER_2_REGEX, self.file_name)
        if parsed_name is not None:
            self.name = parsed_name.group(1)
            label_text = parsed_name.group(3)
            parsed_label = re.search('^([0-9]+)\.\s+(.*)$', label_text)
            if parsed_label is None:
                # Test for embedded episode number
                parsed_label = re.search('^.+\s([0-9]{1,3})\.\s+(.*)$',
                                         label_text)
                if parsed_label is not None:
                    # Found embedded episode number, strip it from label
                    label_text = re.sub('\s([0-9]{1,3})\.\s', " ",
                                        label_text, 1)
            else:
                label_text = re.sub('^([0-9]{1,3})\.\s+', "", label_text, 1)
            if parsed_label is not None:
                self.episode_num = parsed_label.group(1)
            self.label = label_text

    def _info_from_iplayer_type_3_filename(self):
        """
        Extract info about a show from a get_iplayer type 3 file name.

        get_iplayer type 3 format:
        '<Series name> - <Episode Title> <a1b2c3d4> default.<ext>'

        This file name form potentially has information to fill 'name',
        'episode' and 'label'.
        """
        parsed_name = re.search(IPLAYER_3_REGEX, self.file_name)
        if parsed_name is not None:
            parsed_label = re.search('^([0-9]+)\.\s+(.*)$',
                                     parsed_name.group(2))
            parsed_label2 = re.search('Episode[\s]+([0-9]+)',
                                      parsed_name.group(2))
            self.name = parsed_name.group(1)
            if parsed_label is not None:
                self.episode_num = parsed_label.group(1)
            elif parsed_label2 is not None:
                self.episode_num = parsed_label2.group(1)
            if parsed_label is not None:
                self.label = parsed_label.group(2)
            else:
                self.label = parsed_name.group(2)

    def _info_from_iplayer_type_4_filename(self):
        """
        Extract info about a show from a get_iplayer type 4 file name.

        get_iplayer type 4 format:
        '<Movie name> - <Movie name> <a1b2c3d4> default.<ext>'

        This file name form only has information to fill 'name'.
        """
        parsed_name = re.search(IPLAYER_4_REGEX, self.file_name)
        if parsed_name is not None:
            # Use show_name field for movie title.
            self.name = parsed_name.group(1)

    def _info_from_itunes_movie_filename(self):
        """
        Extract info about a show from an iTunes movie file name.

        iTunes Movie
        = '<Movie Title>.<ext>'

        This file name form only has information to fill 'name''.
        """
        parsed_name = re.search(ITUNES_MOVIE_REGEX, self.file_name)
        if parsed_name is not None:
            self.name = parsed_name.group(2)

    def _info_from_itunes_tv_filename(self):
        """
        Extract info about a show from an iTunes TV show file name.

        iTunes TV Series episode
        = '<nn> <Episode name>.<ext>'

        This file name form only has information to fill 'episode_num' and
        'label' by itself, but if it is in an iTunes library we can use the
        path to get 'name' and 'series_num'.
        """
        parsed_name = re.search(ITUNES_TV_REGEX, self.file_name)
        if parsed_name is not None:
            # Get episode number
            self.episode_num = parsed_name.group(1)
            # Get label
            parsed_label = re.search('^Episode\s[0-9]+$',
                                     parsed_name.group(2))
            if parsed_label is None:
                # Not a simple 'Episode <n>' label, so use the text,
                # stripping off any leading episode number
                self.label = re.sub('^[0-9]+\.[\s]+', "",
                                    parsed_name.group(2))
            else:
                self.label = parsed_name.group(2)
            # work out season number and series name from iTunes file path
            season_dir = os.path.basename(self.dir_path)
            season_grep = re.search("Season[\W]+([0-9]+)", season_dir)
            if season_grep is not None:
                self.season_num = season_grep.group(1)
                # force no leading zero
                self.season_num = str(int(self.season_num))
                self.name = os.path.basename(
                    os.path.dirname(self.dir_path))

    def _info_from_makemkv_filename(self):
        """
        Extract info about a show from a MakeMKV rip file name.

        MakeMKV DVD/Blu-ray TV show rip format:
        '<Series_name>_[-_]Season_<num>_[-_]Disc_<num>_t<nn>.<ext>'

        This file name form may have information to fill 'name' and
        'season_num'.
        """
        parsed_name = re.search(MKMKV_TV_REGEX, self.file_name)
        if parsed_name is not None:
            self.name = parsed_name.group(1)
            self.season_num = parsed_name.group(2)

    def _info_from_plex_movie_filename(self):
        """
        Extract info about a show from an Plex library movie file name.

        Plex Movie
        = '<Movie Title> (<YYYY>) - <label>.<ext>'

        This file name form has information to fill 'name', 'year' and
        may have 'label'.
        """
        parsed_name = re.search(PLEX_MOVIE_REGEX, self.file_name)
        if parsed_name is not None:
            if parsed_name.group(1) is not None:
                self.name = parsed_name.group(1)
                self.movie_year = parsed_name.group(2)
            if parsed_name.group(4) is not None:
                self.label = parsed_name.group(4)

    def _info_from_plex_extra_filename(self):
        """
        Extract info about the filename about a Plex library movie extra.

        Plex Movie Library Extra
        = '<filename>-<extra type signifier>.<ext>'

        where <extra type signifier> must be one of:

        'behindthescenes', 'deleted', 'featurette', 'interview', 'scene',
        'short' or 'trailer'.

        The rest of the filename will be used for the  'label' mvar.
        """
        parsed_name = re.search(PLEX_EXTRA_REGEX, self.file_name)
        if parsed_name is not None:
            if parsed_name.group(1) is not None:
                self.extra_type = parsed_name.group(2)
                if self.label is None:
                    self.label = parsed_name.group(1)

    def _info_from_plex_tv_filename(self):
        """
        Extract info about a show from an Plex library TV show file name.

        Plex Library TV Series episode
        = '<Series name> - s<nn>e<nn>[-e<nn>] - <Episode name>.<ext>'

        This file name form has information to fill 'name', 'season_num',
        'episode_num' and possibly episode_range' and 'label'.
        """
        parsed_name = re.search(PLEX_TV_REGEX, self.file_name)
        if parsed_name is not None:
            if parsed_name.group(1) is not None:
                self.name = parsed_name.group(1)
                self.season_num = parsed_name.group(4)
                self.season_num = remove_leading_zeroes(self.season_num)
                self.episode_num = parsed_name.group(5)
                self.episode_num = remove_leading_zeroes(self.episode_num)
            if parsed_name.group(6) is not None:
                self.episode_range = parsed_name.group(6)[2:]
                self.episode_range = remove_leading_zeroes(self.episode_range)
                self.episode_range = int(self.episode_range) -\
                    (int(self.episode_num) - 1)
            if parsed_name.group(10) is not None:
                self.label = parsed_name.group(10)


class FileInfo():
    """
    File info class for use by Plex Tools routines

    Instantiate with a file path name and the CLI args
    """
    def __init__(self, file_path, args, ask=True):
        self.full_file_path = os.path.abspath(file_path)
        (self.dir_path, self.file_name) = os.path.split(self.full_file_path)
        self.cont_type = None
        self.name_type = args.name_type
        self.show_name = args.show_name
        self.season_num = args.season_num
        self.episode_num = args.episode_num
        self.episode_range = args.episode_range
        self.extra_type = args.extra_type
        self.label = args.label
        self.movie_year = args.movie_year
        self.ask_user = ask
        self._fill_file_info()

    def __str__(self):
        return '\n'.join([
            'Full file path: ' + self.full_file_path,
            'File name: ' + self.file_name,
            'Directory path: ' + self.dir_path,
            'Container type: ' + self.cont_type,
            'Name type: ' + str(self.name_type),
            'Series name: ' + str(self.show_name),
            'Season number: ' + str(self.season_num),
            'Episode number: ' + str(self.episode_num),
            'Episode range: ' + str(self.episode_range),
            'Extra type: ' + str(self.extra_type),
            'Label: ' + str(self.label),
            'Movie year: ' + str(self.movie_year)
        ])

    def validate(self):
        """Checks object can be used to generate a Plex library name."""
        validity = False
        if self.name_type in TV_TYPES:
            if (self.show_name is not None) and (self.season_num is not None) \
                    and (self.episode_num is not None):
                validity = True
        elif self.name_type in MOVIE_TYPES:
            if (self.name_type == TYPE_PLEX_EXTRA['id']):
                if (self.show_name is not None) and \
                   (self.movie_year is not None) and \
                   (self.extra_type is not None):
                    validity = True
            else:
                if (self.show_name is not None) and \
                   (self.movie_year is not None):
                    validity = True
        return validity

    def plex_file_name(self):
        """Returns the Plex library name for this file."""
        plex_name = None
        if self.name_type in TV_TYPES:
            plex_name = self._plex_tv_series_file_name()
        elif self.name_type in MOVIE_TYPES:
            plex_name = self._plex_movie_file_name()
        return plex_name

    def movie_dir_name(self):
        """Returns the proper Plex library for the enclosing folder."""
        out_plex_name = None
        if self.name_type in MOVIE_TYPES:
            out_plex_name = self._movie_file_name_stem()
        return out_plex_name

    def _movie_file_name_stem(self):
        """
        Get the minimal form of a movie file name, without the extension

        'minimal' form means the movie name plus bracketed release year
        only. This method will not return the name with a stacked or
        multiresolution label appended.
        """
        out_name_stem = "".join([self.show_name, " (", self.movie_year, ")"])
        return out_name_stem

    def _plex_movie_file_name(self):
        """
        Generate the Plex library file name for a movie.

        Returns:
            The new file name
        """
        out_name = None
        name_list = []
        # Extract and append the file name extension.
        extn_str = os.path.splitext(self.file_name)[1]
        if (self.name_type != TYPE_PLEX_EXTRA['id']):
            name_list.append(self._movie_file_name_stem())
            if self.label is not None:
                name_list.append(" - ")
                name_list.append(self.label)
        else:
            # an extra, interpose the sub-directory name
            name_list.append(PLEX_MOVIE_EXTRA_FOLDERS[self.extra_type])
            name_list.append("/")
            # use label or the existing filename for the new file name.
            if self.label is not None:
                name_list.append(self.label)
            else:
                name_list.append(os.path.splitext(self.file_name)[0])
        name_list.append(extn_str)
        out_name = ''.join(name_list)
        return out_name

    def _plex_tv_series_file_name(self):
        """
        Generate the Plex library file name for a TV show.

        Returns:
            The new file name
        """
        # Add the series name to the list used to construct the new name.
        name_list = [self.show_name]

        # Work out the episode ident string.
        ident_str = get_plex_ident(self.season_num, self.episode_num,
                                   self.episode_range)
        name_list.append(ident_str)

        # Is there a label
        if self.label is not None:
            name_list.append(self.label)

        # Extract and append the file name extension.
        list_len = len(name_list) - 1
        extn_str = os.path.splitext(self.file_name)[1]
        name_list[list_len] = name_list[list_len] + extn_str
        out_file_name = ' - '.join(name_list)

        return out_file_name

    def _sanitise_os_sep(self):
        """
        Remove directory seperators from the show/move name and label
        
        If there is a '/' in the path/filename on POSIX systems then when
        plextool needs to create a directory or copy a file spurious subdirs
        will be created. This function subsitutes os seperators found in
        the 'show_name' and 'label' mvars replacing them with ':'.

        ':' is chosen as the substitute because
        
        (a) in Mac OS in the Finder files with a ':' in their POSIX path show \
            as having a '/'

        (b) Plex's library scanning tools treat ':' as '/' when searching for
            the show in thetvdb.com and other matching services
        """
        if self.show_name is not None:
            if os.sep in self.show_name:
                self.show_name = self.show_name.replace(os.sep, ':')
        if self.label is not None:
            if os.sep in self.label:
                self.label = self.label.replace(os.sep, ':')

    def _fill_file_info(self):
        """Loads FileInfo object with data extracted from the name and path."""
        # Identify the container format
        self.cont_type = get_video_container_format(self.full_file_path)
        # Identify the file name type.
        if self.name_type is None:
            self.name_type = detect_file_name_type(
                self.full_file_path, self.ask_user)
            if self.name_type is None:
                raise PTError("Unable to identify name type for file '" +
                              colors.BOLD +
                              os.path.basename(self.full_file_path) +
                              colors.ALL_OFF + "', in directory '" +
                              os.path.dirname(self.full_file_path) + "'.'")
        # Pass to name type specific routines to get the rest of the data
        if self.name_type in TV_TYPES:
            self._fill_finfo_for_tv_show()
        elif self.name_type in MOVIE_TYPES:
            self._fill_finfo_for_movie()
        # Quick sanity check as date values may be weird
        if self.movie_year is not None:
            year_match = re.search('(20\d{2})|(19\d{2})', self.movie_year)
            if year_match is not None:
                self.movie_year = year_match.group(1) \
                    if (year_match.group(1) is not None) \
                    else year_match.group(2)
        self._sanitise_os_sep()

    def _fill_finfo_from_metadata(self):
        """Abstraction to select correct metadata extraction tool."""
        for case in switch(self.cont_type):
            if case(MP4_CONTAINER):
                self._fill_finfo_from_mp4_metadata()
                break
            if case(MKV_CONTAINER):
                pass
                break

    def _fill_finfo_from_mp4_metadata(self):
        """
        Get the FileInfo data from iTunes MP4 style metadata.

        Uses the contents of the 'tvsh', 'tvsn', 'tves' and '©nam' atoms.
        """
        pt_print("Examining '" + colors.BOLD + self.file_name +
                 colors.END + "' for MPEG-4 type metadata...", inset=1)
        metadata = get_show_metadata(self.full_file_path)
        if self.show_name is None:
            if metadata[SERIES_NAME_ID['atom']] is not None:
                self.show_name = metadata[SERIES_NAME_ID['atom']]
            elif (self.name_type in MOVIE_TYPES) and \
                 (metadata[NAME_ID['atom']] is not None):
                self.show_name = metadata[NAME_ID['atom']]
        if self.name_type in TV_TYPES:
            if self.season_num is None:
                if metadata[SEASON_ID['atom']] is not None:
                    # get season number ensuring no leading zero
                    self.season_num = str(int(metadata[SEASON_ID['atom']]))
            if self.episode_num is None:
                if metadata[EP_NUM_ID['atom']] is not None:
                    # get episode number ensuring no leading zero
                    self.episode_num = str(int(metadata[EP_NUM_ID['atom']]))
            if self.label is None:
                if metadata[NAME_ID['atom']] is not None:
                    if self.name_type == TYPE_EYETV_TV['id']:
                        label_search = re.search(
                            r'[0-9]{1,4}/[0-9]{1,4}, (.*)',
                            metadata[NAME_ID['atom']])
                        if label_search is not None:
                            self.label = label_search.group(1)
                    if self.label is None:
                        if self.name_type == TYPE_ITUNES_TV['id']:
                            ep_num_search = re.search(
                                                '(^[0-9]+\.[\s]+)(.*)',
                                                metadata[NAME_ID['atom']])
                            if ep_num_search is not None:
                                self.label = ep_num_search.group(2)
                        if self.label is None:
                            ep_search = re.search(
                                            '.*[\s]+(Episode[\s]+[0-9]+)$',
                                            metadata[NAME_ID['atom']])
                            if ep_search is not None:
                                self.label = ep_search.group(1)
                    if self.label is None:
                        self.label = metadata[NAME_ID['atom']]
        if self.movie_year is None:
            if self.name_type == TYPE_EYETV_MOV_1['id'] or \
                        self.name_type == TYPE_EYETV_MOV_2['id']:
                    year_search = re.search(r'^([0-9]{4})',
                                            metadata[NAME_ID['atom']])
                    if year_search is not None:
                        self.movie_year = year_search.group(1)
            else:
                if metadata[YEAR_ID['atom']] is not None:
                    self.movie_year = metadata[YEAR_ID['atom']]

    def _fi_get_num(self, in_prompt):
        out_num = None
        if self.ask_user:
            out_num = get_num_from_user(in_prompt)
        return out_num

    def _fi_get_text(self, in_prompt):
        out_str = None
        if self.ask_user:
            out_str = get_text_from_user(in_prompt)
        return out_str

    def _fill_finfo_for_movie(self):
        """Extracts FileInfo data for a generic movie."""
        pt_print("Getting information for generic movie.",
                 inset=1)
        self._fill_finfo_from_metadata()
        if not self.validate():
            name_info = NameInfo(self.full_file_path)
            if self.show_name is None:
                if name_info.name is not None:
                    self.show_name = name_info.name
                else:
                    self.show_name = self._fi_get_text(
                        'Please enter the movie name: ')
            if self.movie_year is None:
                if name_info.movie_year is not None:
                    self.movie_year = name_info.movie_year
                else:
                    self.movie_year = self._fi_get_num(
                        'Please enter year movie released: ')
            if self.label is None:
                if name_info.label is not None:
                    self.label = name_info.label
            if self.name_type == TYPE_PLEX_EXTRA['id']:
                if self.extra_type is None:
                    if name_info.extra_type is not None:
                        self.extra_type = name_info.extra_type
                    else:
                        user_input = self._fi_get_text(
                            'Please enter the type of extra: ')
                        if user_input in EXTRA_FLAGS:
                            self.extra_type = user_input
        else:
            # Occasionally a file with a plex movie type filename will have
            # a movie date in the MPEG 4 metadata that differs from the date
            # in the file name. The MPEG 4 date will now be in the 
            # movie_year ivar of this object and as this year is likely
            # wrong we need to correct the ivar to the date from the
            # filename.
            if (self.name_type == TYPE_PLEX_MOVIE['id']):
                name_info = NameInfo(self.full_file_path)
                self.movie_year = name_info.movie_year

    def _fill_finfo_for_tv_show(self):
        """
        Extracts FileInfo data for a generic TV show.
        """
        pt_print("Getting information for TV show.",
                 inset=1)
        self._fill_finfo_from_metadata()
        if not self.validate():
            name_info = NameInfo(self.full_file_path)
            if self.show_name is None:
                if name_info.name is not None:
                    self.show_name = name_info.name
                else:
                    self.show_name = self._fi_get_text(
                        'Please enter show name: ')
            if self.season_num is None:
                if name_info.season_num is not None:
                    self.season_num = name_info.season_num
                else:
                    self.season_num = self._fi_get_num(
                        'Please enter season number: ')
            if self.episode_num is None:
                if name_info.episode_num is not None:
                    self.episode_num = name_info.episode_num
                else:
                    self.episode_num = self._fi_get_num(
                        'Please enter episode number: ')
            if self.episode_range is None:
                if name_info.episode_range is not None:
                    self.episode_range = name_info.episode_range
            if self.label is None:
                self.label = name_info.label


class IsMountPathReply():
    """
    Encapsulates the data returned by the is_path_on_network_mount() method

    Instantiate with the data to be returned
    """
    def __init__(self, in_is_mnt=False, in_user=None, in_svr=None,
                 in_mnt_name=None, in_mnt_path=None):
        self.is_mount = in_is_mnt
        self.user = in_user
        self.server = in_svr
        self.mount_name = in_mnt_name
        self.mount_path = in_mnt_path

    def __str__(self):
        return ', '.join([
            'is_mount:' + str(self.is_mount),
            'user:' + str(self.user),
            'server:' + str(self.server),
            'mount_name:' + str(self.mount_name),
            'mount_path:' + str(self.mount_path)
        ])


# Functions -------------------------------------------------------------------


def _name_type_description(id):
    """Get the description of a filename type from it's id."""
    ret_desc = None
    for ntype in NAME_TYPE_ARRAY:
        if ntype['id'] == id:
            ret_desc = ntype['desc']
            break
    return ret_desc


def get_show_metadata(in_file_path):
    """
    Identify the file's video type, extract and return the metadata.

    Returns:
        a dictionary with the following information:

        SERIES_NAME_ID['atom']: the series name
        SEASON_ID['atom']: the season number (as a string)
        EP_NUM_ID['atom']: the episode number (as a string)
        NAME_ID['atom']: the name of the episode
        YEAR_ID['atom']: the year of release (as a string)
        GENRE_ID['atom']: the iTunes genre, if present
        COMMENT_ID['atom']: the iTunes comment, if present
        DESC_ID['atom']: the iTunes description, if present
        LONG_DESC_ID['atom']: A longer description, iTunes doesn't create this
        ARTIST_ID['atom']: The director or producing company
        ALBUM_ARTIST_ID['atom']: the album artist - eg performer of a classic
        ALBUM_ID['atom']: the album name, eg 'LOST, Season 6'
        COVER_ID['atom']: present if there is cover art, not the actual data
        MEDIAKIND_ID['atom']: broad category, eg 'Movie', TV Show'
        COPYRIGHT_ID['atom']: the copyright owner
        EP_ID_ID['atom']: a unique id for episode, useful to spot EyeTV
                          N/R movies where it is filled with 'YYYY, Other'
        EYETV_XML_LABEL: the show info that EyeTV includes in exported files
        EYETV_RATING_LABEL: Rating (as in PG13) for show

        If the atom was not found the value will be None
    """
    out_md = _prime_metadata_dict()
    md_format = get_video_container_format(in_file_path)
    for case in switch(md_format):
        if case(MP4_CONTAINER):
            out_md = get_show_mpeg4_metadata(in_file_path)
            break
        if case(MKV_CONTAINER):
            out_md = get_show_mkv_metadata(in_file_path)
            break
    return out_md


def get_video_container_format(in_file_path):
    """
    Test the file to find its video container format.

    Examines the file via the mediainfo tool, requesting the information
    in XML format. Parses the XML to find out if the file has a video
    container format.

    Raises a PTError exception if the file doesn't exist or if does not appear
    to be a media file.

    Params:
        in_file_path:   The file to examine.

    Returns:
        the detected format, or 'No Format' if none specified.
    """
    out_format = 'No Format'
    xml_output = safe_mediainfo_call(in_file_path, mediainfo_xml_option())[0]
    root = ET.fromstring(xml_output)
    file_element = root.find('File')
    if file_element is None:
        raise PTError("No file '" + colors.BOLD + in_file_path +
                      colors.ALL_OFF + "' found by mediainfo.")
    track_iter = file_element.iter('track')
    if track_iter is None:
        raise PTError("No track info found by mediainfo in '" +
                      colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
    while True:
        track = next(track_iter, None)
        if track is not None:
            if track.attrib[TYPE_ATOM] == 'General':
                format_elem = track.find('Format')
                if format_elem is not None:
                    out_format = format_elem.text
                    break
        else:
            # safety bail out
            break
    return out_format


def get_show_mpeg4_metadata(file_path):
    """
    Extracts identifying and other data from iTunes MP4 style metadata.

    Looks for the 'tvsh', 'tvsn', 'tves', '©nam', '©lyr', '©cmt', 'desc' and
    '----' [com.apple.iTunes;iTunMOVI] atoms

    Requires that AtomicParsley is installed.

    Params:
        file_path:  The file to examine.

    Returns:
        A dictionary containing the located metadata with the atoms as
        key: data pairs
    """
    # Prime the dictionary.
    atom_dict = _prime_metadata_dict()
    atom_dict[TYPE_ATOM] = MP4_CONTAINER
    # Get any saved metadata.
    saved_md = get_saved_metadata(file_path)
    if saved_md is not None:
        atom_dict = dict(atom_dict.items() + saved_md.items())
    # Read the MPEG-4 metadata.
    #
    # Long comment here because I have to do some odd stuff for non-abvious
    # reasons. I admit most of the problem was due to my extremely limited/
    # virtually non-existent knowledge of Unicode and how Python handles it.
    # This long note is written in the hope that setting the problem out
    # clearly will help consolidate what I have learnt about those two
    # domains and act as an aide-momoire if I should have to come back to
    # the code later.
    #
    # First some premises:
    #
    # *   AtomicParsley's output is Unicode, encoded as UTF-8. That is
    #     any character with an Unicode code point <= 0x7f is represented as a
    #     single byte. Any character with a code point above that is
    #     represented by a sequence of 2, 3 or four bytes, each >= 0x80.
    #
    # *   In Python Unicode strings (eg u'a string') are strings of 16-bit or
    #     32-bit chars (depending on the compile time settings when the
    #     Python interpreter was built). In these each character is
    #     represented by its Unicode code point.
    #
    # *   Standard python strings (eg 'a string') are technically ASCII only
    #     (all chars <= 0x7f) but can in fact hold chars in the 0x80-0xff
    #     range BUT THESE ARE NOT UNICODE CODE POINTS as they are 8-bit only
    #     not 16/32-bit. You might acquire such a string via user input
    #     or by construction in code thus:
    #         a_str = '\xa9nam'
    #     The above line creates the string '©nam' and is technically in
    #     one of the Western Latin encodings. Similarly you could construct
    #     a UTF-8 encoded string like this:
    #         utf8_str = '\xc2\xa9name'
    #     This line creates a string which prints out as '©nam'. In addition,
    #     if your source file is UTF-8 (set by your editor) and is declared
    #     as UTF-8 with the appropriate first line declaration then
    #         utf8_str = '©nam'
    #     also produces a UTF-8 encoded Python string.
    #
    # Now the problem. I split the output from AtomicParsley into an array of
    # lines and use a regex on each line to (a) test if it begins with 'Atom'
    # and so contains the data from an MPEG-4 atom and (b), to pull out the
    # four character token following that word which identifies which atom
    # the data belongs to.
    #
    # I then compare the extracted token against a table of tokens I am
    # interested in to decide whther the current atom is one whose data I
    # want to extract. The tokens in my table are Unicode strings
    # eg 'u'©nam'. If you compare a unicode string against the UTF-8
    # encoded extracted token the comparison will always fail if the
    # extracted token includes a character with a code point >=0x80.
    #
    # To make the test work I was converting the AtomicParsley output from
    # UTF-8 to Unicode before the comparison. This worked on all my original
    # test and real world files but then i met a problem: one file contained
    # a character in an atom's *data* that had a code point >= 0x80 *but*
    # because it was file data had not been converted to a UTF-8 character.
    # Essentially I had a chunk of Latin-1 encoded text in the middle of a
    # block of UTF-8 encoded text. When the Python UTF-8 to Unicode codec was
    # called on this it errored when it hit this single byte >= 0x80 as there
    # was no way this could be valid UTF-8.
    #
    # After a lot of experimentation - mostly required because at this stage
    # I didn't understand the problem - the penny dropped and I realised
    # what the problem was, why I was seeing it and it had a simple fix.
    #
    # First I needed to modify my search regex. To extract the token it was
    # looking for four characters prefixed and suffixed by a double-quote
    # character in the first 'word' position after the 'Atom'. As I was now
    # searching UTF-8 encoded text there would possibly be more than four
    # characters between the quotes (in theory as many as 16 bytes in an
    # extreme case. That done all I needed to do was convert the extracted
    # atom to a unicode string, which I do with:
    #     unicode(extracted_token, 'utf-8')
    # and I can compare that string successfully against my token table
    # entries.
    try:
        raw_output = subprocess.Popen(["AtomicParsley", file_path, "-t"],
                                      stdout=subprocess.PIPE).communicate()[0]
    except:
        return atom_dict
    lines = raw_output.split('\n')
    line_count = 0
    lines_len = len(lines)
    while line_count < lines_len:
        list_line = lines[line_count]
        if line_count == 0:
            list_line = list_line[3:]   # Discard unicode marker.
        atom_test = re.search(u'^Atom\s\"(.+?)\"\s', list_line)
        if atom_test is not None:
            cur_atom = unicode(atom_test.group(1), 'utf-8')
            if cur_atom in [
                SERIES_NAME_ID['atom'], SEASON_ID['atom'],
                    EP_NUM_ID['atom'], NAME_ID['atom'], YEAR_ID['atom'],
                    GENRE_ID['atom'], COMMENT_ID['atom'], DESC_ID['atom'],
                    LONG_DESC_ID['atom'], ARTIST_ID['atom'],
                    ALBUM_ARTIST_ID['atom'], ALBUM_ID['atom'],
                    COVER_ID['atom'], MEDIAKIND_ID['atom'], EP_ID_ID['atom'],
                    COPYRIGHT_ID['atom'], RDNS_ID['atom']]:
                if cur_atom == RDNS_ID['atom']:
                    save_match = DataHolder(attr_name='match')
                    if save_match(re.search(u'^Atom\s\"(.{4})\"\s\[' +
                                            EYETV_XML_ID['atom'] +
                                            u'\]\scontains:\s(.*)$',
                                            list_line)):
                        # Get EyeTV prog info XML
                        xml_list = []
                        xml_list.append(save_match.match.group(2))
                        for xml_line in lines[line_count + 1:]:
                            xml_atom_test = re.search(u'^Atom\s\"(.{4})\"\s',
                                                      xml_line)
                            if (xml_atom_test is not None):
                                break
                            xml_list.append(unicode(xml_line, "utf-8"))
                            line_count += 1
                        atom_dict[EYETV_XML_LABEL] = u'\n'.join(xml_list)
                    elif save_match(re.search(u'^Atom\s\"(.{4})\"\s\[' +
                                              EYETV_RATING_ID['atom'] +
                                              u'\]\scontains:\s(.*)$',
                                              list_line)):
                        # Get EyeTV prog rating
                        atom_dict[EYETV_RATING_LABEL] = \
                            save_match.match.group(2)
                else:
                    atom_data = re.search(u'^Atom\s\"(.+?)\"\scontains:'
                                          u'\s(.*)$', list_line)
                    if atom_data is not None:
                        atom_dict[cur_atom] = atom_data.group(2)
        line_count += 1
    return atom_dict


def get_show_mkv_metadata(in_file_path):
    """
    Extracts identifying and other data from Matroska style metadata.

    Requires that mediainfo is installed and uses its --Output=XML option
    to get easily parse-able metadata

    Params:
        file_path:  The file to examine.

    Returns:
        A dictionary with the information found. To simplify code using this
        function and get_show_mpeg4_metadata() this returns its data with
        key: data pairs using the atom keys that get_show_mpeg4_metadata()
        uses.
    """
    # Prime the dictionary.
    atom_dict = _prime_metadata_dict()
    atom_dict[TYPE_ATOM] = MKV_CONTAINER
    try:
        xml_output = safe_mediainfo_call(in_file_path,
                                         mediainfo_xml_option())[0]
    except:
        return None
    root = ET.fromstring(xml_output)
    file_element = root.find('File')
    if file_element is None:
        raise PTError("No file '" + colors.BOLD + in_file_path +
                      colors.ALL_OFF + "' found by mediainfo.")
    track_iter = file_element.iter('track')
    if track_iter is None:
        raise PTError("No track info found by mediainfo in '" +
                      colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
    while True:
        track = next(track_iter, None)
        if track is not None:
            if track.attrib[TYPE_ATOM] == 'General':
                for cur_id in META_IDS:
                    if cur_id['tag'] is not None:
                        found_elem = track.find(cur_id['tag'])
                        if found_elem is not None:
                            if cur_id['tag'] == EYETV_RATING_ID['tag']:
                                atom_dict[EYETV_RATING_LABEL] = found_elem.text
                            elif cur_id['tag'] == EYETV_XML_ID['tag']:
                                atom_dict[EYETV_XML_LABEL] = found_elem.text
                            else:
                                atom_dict[cur_id['atom']] = found_elem.text
                break
    return atom_dict


def _prime_metadata_dict():
    """
    Get an empty dictionary for the get_show_metdata functions.

    See get-show_metadata() docstring for detailed info on what each key:value
    pair is.

    Returns:
        A dictionary, initialised with the mpeg4 atom names as the keys and
        the data for each pair set to None.
    """
    # Prime the dictionary.
    out_dict = {SERIES_NAME_ID['atom']: None, SEASON_ID['atom']: None,
                EP_NUM_ID['atom']: None, NAME_ID['atom']: None,
                YEAR_ID['atom']: None, GENRE_ID['atom']: None,
                COMMENT_ID['atom']: None, DESC_ID['atom']: None,
                LONG_DESC_ID['atom']: None, ARTIST_ID['atom']: None,
                ALBUM_ARTIST_ID['atom']: None, ALBUM_ID['atom']: None,
                COVER_ID['atom']: None, MEDIAKIND_ID['atom']: None,
                COPYRIGHT_ID['atom']: None, EP_ID_ID['atom']: None,
                EYETV_XML_LABEL: None, EYETV_RATING_LABEL: None}
    return(out_dict)


def get_saved_metadata(in_file_path):
    """
    If a saved metadata exists for the file return the saved data dict.

    Returns:
        The saved metadata, 'None' if no saved metadata found.
    """
    out_saved_md = None
    if saved_metadata_exists(in_file_path):
        save_file_name = get_saved_metadata_filename(in_file_path)
        with open(save_file_name, 'rb') as save_file:
            try:
                save_data = cPickle.load(save_file)
                out_saved_md = save_data.md
            except cPickle.PickleError, e:
                pt_print_err(e.value, inset=1)
    return out_saved_md


def get_saved_metadata_filename(in_file_path):
    """Generate a filename in which to save metadata from this file."""
    out_fname = in_file_path + '.ptsmd'
    return out_fname


def saved_metadata_exists(in_file_path):
    """Tests if saved metadata exists for the file, returning True or False"""
    save_file_name = get_saved_metadata_filename(in_file_path)
    return(os.path.exists(save_file_name))


def extracted_cover_art_exists(in_file_path):
    """
    Test if at least one extracted cover image exists for file.

    Returns:
        A list of the files in the same directory as the file that (as
        determined by their name) contain artwork extracted from the file.
        Otherwise returns None.
    """
    out_list = None
    if os.path.isfile(in_file_path):
        full_path = os.path.abspath(in_file_path)
        (dir_name, file_name) = os.path.split(full_path)
        (f_name_stem, f_name_ext) = os.path.splitext(file_name)
        artwork_f_name_stem = f_name_stem + '_artwork_'
        dir_list = os.listdir(dir_name)
        for cur_name in dir_list:
            match_stem = os.path.commonprefix([artwork_f_name_stem, cur_name])
            if match_stem == artwork_f_name_stem:
                if out_list is None:
                    out_list = []
                out_list.append(cur_name)
    return out_list


def safe_mediainfo_call(in_file_path, in_cmd):
    """
    Call mediainfo with a safe file path and sanitizes the returned XML.

    If the '?' character is in the path mediainfo uses to open a file it fails.
    This function tests the path for the presence of '?' and if found creates
    a symlink to in_file_path in a temp directory and uses the path to that
    link with the mediainfo call.

    Raises an exception if the link creation is required but fails.

    Versions of mediainfo subsequent to 0.7.67 produce malformed XML if the
    text of any field it exracts contains a high ascii (> 0x7f) character it
    includes it 'as is' rather than converting it to a UTF-8 character as is
    required. If this isn't done, subsequent attempts to parse the XML fail.

    Params:
        in_file_path:   The file you are asking mediainfo to act on.

        in_cmd:         The arguments to be passed to mediainfo, except the
                        file

    Returns:
        a tuple of (
            std_out of the mediainfo command,
            std_err of the mediainfo command,
            return code of the mediainfo command
        )
    """
    raw_output = raw_err = return_code = None
    safe_path = in_file_path
    if has_mediainfo_bad_char(in_file_path):
        temp_dir = tempfile.gettempdir()
        temp_file_name = str(uuid.uuid4()) + '.mp4'
        safe_path = os.path.join(temp_dir, temp_file_name)
        try:
            os.symlink(in_file_path, safe_path)
        except:
            safe_path = None
            raise PTError("Unable to create temporary link at '" +
                          safe_path + "' to '" + in_file_path + "'")
    cmd_list = ['mediainfo', safe_path, in_cmd]
    my_proc = subprocess.Popen(cmd_list, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    (raw_output, raw_err) = my_proc.communicate()
    return_code = my_proc.returncode
    if in_file_path != safe_path:
        # if temp link used remove it
        os.remove(safe_path)
    # sanitize the XML
    try:
        # with well formed XML the next command returns unchanged text, with
        # malformed XML it raises an exception and the bad XML is fixed in the
        # except block.
        xml_out = raw_output.decode('utf-8')
        # except sometimes it doesn't raise an exception so still need to do
        # the same fix <sigh>
        mac_roman_text = raw_output.decode('mac_roman')
        xml_out = mac_roman_text.encode('utf-8')
    except UnicodeDecodeError:
        mac_roman_text = raw_output.decode('mac_roman')
        xml_out = mac_roman_text.encode('utf-8')
    return xml_out, raw_err, return_code


def has_mediainfo_bad_char(in_file_path):
    """Test file path for the presence chars known to upset mediainfo."""
    BAD_CHARS = ['?', '*']

    out_truth = False
    for bad_char in BAD_CHARS:
        if bad_char in in_file_path:
            out_truth = True
            break
    return out_truth


def mediainfo_xml_option():
    """
    Get  the correct mediainfo '--Output=' option.

    When its developers changed mediainfo's version numbering system they also
    changed its (default) XML output format to one that breaks Plex Tools
    routines trying to extract data from the Element Tree parsed XML. There
    is an option that lets the user select the original format XML. This
    routine tests the installed mediainfo version and returns the correct
    option to ensure we always get the original XML

    Params:
        None

    Returns:
        the fully formed option to pass to mediainfo as a atring

    """
    out_option = "--Output=XML"
    cmd_list = ['mediainfo', "--Version"]
    my_proc = subprocess.Popen(cmd_list, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    (raw_output, raw_err) = my_proc.communicate()
    vers_lines = raw_output.split('\n')
    vers_search = re.search(r'(MediaInfoLib - v)([0-9\.]+)', vers_lines[1])
    vers_string = vers_search.group(2)
    if vers_string[:2] != "0.":
        # mediainfo generates new XML by default
        out_option = "--Output=OLDXML"
    return out_option


def get_plex_ident(in_season_num, in_episode_num, in_episode_range=None):
    """
    Generate the season/episode ident for plex files.

    Params:
        in_season_num: the season number as a string.
        in_episode_num: the episode number as a string
        in_episode_range: count of episode in file if greater than 1, as text

    Returns:
        the ident as a string

    """
    season_num_text = in_season_num
    if len(in_season_num) == 1:
        season_num_text = '0' + in_season_num
    episode_num_text = in_episode_num
    if len(in_episode_num) == 1:
        episode_num_text = '0' + in_episode_num
    ident_str = "".join(['s', season_num_text, 'e', episode_num_text])
    # Is there an episode range?
    if in_episode_range is not None:
        num_eps = int(in_episode_range)
        ep_range_num = (int(in_episode_num) - 1) + num_eps
        range_num_text = str(ep_range_num)
        if len(range_num_text) == 1:
            range_num_text = '0' + range_num_text
        ident_str = "".join([ident_str, '-e', range_num_text])
    return ident_str


PT_PRINT_INSET_WIDTH = 4
PT_PRINT_MARKER_ARROW_WIDTH = 4


def pt_print(msg, inset=0, no_nl=False):
    """
    Pretty printing function for Plex Tools.

    Prints out the passed string with a marker prefix.

    Params:
        msg: the text to print, can use colors.py flags for bold, colour &c.
        inset: inset the message by inset * 4 spaces (default 0)
        no_nl: don't move cursor back to col 1 and down a line
                 (default False)

    Returns:
        Nothing
    """
    inset_str = ''
    while inset != 0:
        inset_str = inset_str + '    '
        inset -= 1
    with colors.pretty_output() as out:
        out.write(colors.BOLD + '==> ' + colors.END + inset_str + msg,
                  no_crlf=no_nl)


def pt_print_err(in_msg, inset=0, no_nl=False):
    """
    Pretty printing function for errors for Plex Tools.

    Simply calls through to pt_print with red 'Error' prefixed to the msg
    param string and the err flag set.

    Params:
        msg: the text to print, can use colors.py flags for bold, colour &c.
        inset: inset the message by inset * 4 spaces (default 0)
        no_nl: don't move cursor back to col 1 and down a line
                 (default False)

    Returns:
        Nothing
    """

    out_msg = colors.FG_RED + "Error" + colors.END + ": " + in_msg
    pt_print(out_msg, inset, no_nl)


def get_plextools_version_string(strict=True):
    """
    Returns the Plex Tools version number as a string.

    By default the function returns a distutils strict version string.

    Params:
        strict: If False then a string that can be compared with
                distutils.version.LooseVersion() will be returned.
    """
    return getversionstr(VERSION_INFO, strict)


def detect_file_name_type(file_path, progress_info=True):
    """
    Try to identify the transcoding, storing or downloading app.

    Various downloading tools have predictable or semi-predictable patterns
    to their names. This function tests the file name in various ways to see
    if it can be identified. If it can then information like series name
    may be able to be extracted.

    This is not super-reliable but occasionally can help.

    Params:
        file_path:      The file to examine.

        progress_info:  Print informatio about what was found to stdout.

    Returns:
        If the file name format is identified a code is returned specific to
        that format. The code is the data corresponding to the 'id' key in the
        dictionary for that file name type. A list of dictionaries can be
        found in the "NAME_TYPE_ARRAY" array. Otherwise returns ""
"""
    ret_type = ""
    full_file_path = os.path.abspath(file_path)
    (dir_path, file_name) = os.path.split(full_file_path)

    # Is it a get_iplayer generated file?
    save_match = DataHolder(attr_name='match')
    if re.search(IPLAYER_BASE_REGEX, file_name) is not None:
        if save_match(re.search(IPLAYER_1_REGEX, file_name)):
            # Type 1 format
            ret_type = IPLAYER_TYPE_1['id']
        elif save_match(re.search(IPLAYER_2_REGEX, file_name)):
            # Type 2 format
            ret_type = IPLAYER_TYPE_2['id']
        elif save_match(re.search(IPLAYER_3_REGEX, file_name)):
            # Type 3 or 4
            if save_match.match.group(1) == save_match.match.group(2):
                ret_type = IPLAYER_TYPE_4['id']
            else:
                ret_type = IPLAYER_TYPE_3['id']
    else:
        if save_match(re.search(MKMKV_TV_REGEX, file_name)):
            # A MakeMKV generated file.
            ret_type = TYPE_MAKEMKV_TV_RIP['id']
        elif save_match(re.search(EYETV_MOVIE_REGEX, file_name)):
            # An EyeTV generated movie file.
            if save_match.match.group(1) is not None:
                ret_type = TYPE_EYETV_MOV_1['id']
            else:
                ret_type = TYPE_EYETV_MOV_2['id']
        elif save_match(re.search(EYETV_TV_REGEX, file_name)):
            # An EyeTV generated TV show file.
            ret_type = TYPE_EYETV_TV['id']
        elif save_match(re.search(PLEX_TV_REGEX, file_name)):
            # Is a Plex TV Library file
            ret_type = TYPE_PLEX_TV['id']
        elif save_match(re.search(PLEX_EXTRA_REGEX, file_name)):
            # Is a Plex Movie Library Extra file
            # Must precede the TYPE_PLEX_MOVIE test because that will also
            # match this
            ret_type = TYPE_PLEX_EXTRA['id']
        elif save_match(re.search(PLEX_MOVIE_REGEX, file_name)):
            # Is a Plex Movie Library file
            ret_type = TYPE_PLEX_MOVIE['id']
        elif save_match(re.search(ITUNES_TV_REGEX, file_name)):
            # Is it an iTunes library TV series file?
            # This is a pretty weak test because ITUNES_TV_REGEX will match
            # almost any filename with an extension that begins with some
            # digits and a space. So need to check a little more.
            # Note that this is dependant on the file being in a normal
            # iTunes library structure.
            dir_name = os.path.basename(dir_path)
            if ('/iTunes Music/TV Shows/' in dir_path) or \
                    ('/iTunes Media/TV Shows/' in dir_path):
                ret_type = TYPE_ITUNES_TV['id']
            elif ('/iTunes Music/Movies/' in dir_path) or \
                    ('/iTunes Media/Movies/' in dir_path) and \
                    (dir_name == save_match.match.group(2)):
                # Some iTunes movies begin with a number so may be
                # caught by this regex
                ret_type = TYPE_ITUNES_MOVIE['id']
        elif save_match(re.search(ITUNES_MOVIE_REGEX, file_name)):
            # Is it an iTunes library movie file?
            # This is a very weak test because ITUNES_MOVIE_REGEX will match
            # almost any filename with an extension. Need to check a little
            # more.
            # Note that this is dependant on the file being in a normal
            # iTunes library structure.
            # Note 2 We will not end up here with a file with a digit group
            # at the beginning as that is picked up by
            dir_name = os.path.basename(dir_path)
            if ('/iTunes Music/Movies/' in dir_path) or \
                    ('/iTunes Media/Movies/' in dir_path) and \
                    (dir_name == save_match.match.group(2)):
                ret_type = TYPE_ITUNES_MOVIE['id']
    # Failure case
    if ret_type == "":
        ret_type = None
    if (ret_type is not None) and progress_info:
        pt_print("'" + colors.BOLD + file_name + colors.END + "' has a " +
                 _name_type_description(ret_type) + " filename.", inset=1)
    return ret_type


def get_num_from_user(prompt='Please enter number: ',
                      err_msg='Sorry, that isn\'t a valid number. '
                      'Try again....'):
    """
    Get a number from the user via raw_input().

    Params:
        prompt: message to request the number.
        err_msg: message to display if the user doesn't enter a valid number

    Returns:
        An integer as a string, None if user aborts.
    """
    got_num = False
    try:
        while not got_num:
            pt_print(prompt, inset=1, no_nl=True)
            in_res = raw_input('')
            in_res = re.sub('[\s,]*', '', in_res)
            if in_res.isdigit():
                got_num = True
            else:
                print err_msg
    except:
        # user aborted input, return None
        in_res = None
    return in_res


def get_text_from_user(prompt='Please enter your text: '):
    """
    Get text from the user via raw_input().

    Params:
        prompt: message to request the number.

    Returns:
        A string, None if user aborts.
    """
    try:
        pt_print(prompt, inset=1, no_nl=True)
        in_res = raw_input('')
    except:
        # user aborted input, return None
        in_res = None
    return in_res


def check_for_nuisance_metadata(in_file_path):
    """
    Checks if a file contains nuisance metadata.

    Tests for the presence of MPEG-4 type cover art or any atom specified in
    the NUISANCE_METADATA_ATOMS constant array.

    Returns:
        True if any nuisance metadata or MPEG-4 cover art found, otherwise
        False.
    """
    pt_print('Checking for nuisance metadata...', inset=1)
    out_bool = False
    if (os.path.exists(in_file_path)):
        cur_metadata = get_show_metadata(in_file_path)
        found = []
        for atom in NUISANCE_METADATA_ATOMS:
            if cur_metadata[atom] is not None:
                out_bool = True
                found.append(atom)
        # Test for cover art - only need to do for MPEG-4, MKV art is ignored.
        if cur_metadata[TYPE_ATOM] == MP4_CONTAINER:
            xml_output = safe_mediainfo_call(in_file_path,
                                             mediainfo_xml_option())[0]
            root = ET.fromstring(xml_output)
            file_element = root.find('File')
            if file_element is None:
                raise PTError("No file '" + colors.BOLD + in_file_path +
                              colors.ALL_OFF + "' found by mediainfo.")
            track_iter = file_element.iter('track')
            if track_iter is None:
                raise PTError("No track info found by mediainfo in '" +
                              colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
            while True:
                track = next(track_iter, None)
                if track is not None:
                    if track.attrib[TYPE_ATOM] == 'General':
                        cover_elem = track.find('Cover')
                        if cover_elem is not None:
                            if 'Yes' == cover_elem.text:
                                out_bool = True
                                found.append('Cover art')
                            break
                else:
                    # safety bail out
                    break
        if not out_bool:
            pt_print("None found.", inset=2)
        else:
            found_str = u"Found: '" + u"', '".join(found) + u"'"
            pt_print(found_str.encode('utf-8'), inset=2)
    else:
        pt_print('No file found...', inset=2)
    return out_bool


def remove_nuisance_metadata_from_file(in_file_path):
    """
    Identifies video type, calls function to remove nuisance metadata.

    Params:
        in_file_path: the file to act on.

    Returns:
        Nothing
    """
    pt_print("Removing nuisance metadata.", inset=1)
    md_format = get_video_container_format(in_file_path)
    for case in switch(md_format):
        if case(MP4_CONTAINER):
            remove_nuisance_mp4_metadata_from_file(in_file_path)
            break
        if case(MKV_CONTAINER):
            remove_nuisance_mkv_metadata_from_file(in_file_path)
            break


def remove_nuisance_mkv_metadata_from_file(in_file_path):
    """
    Deletes the metadata from a Matroska file that potentially confuse Plex.

    Currently the only known problem is the 'title' field of the 'info'
    segment so I don't bother getting metadata and scanning it, just delete
    that field.

    Attached cover art is not deleted as testing has shown no sign that Plex
    pays any attention to it.

    Params:
        in_file_path: the file to act on.

    Returns:
        Nothing
    """
    cmd_host = None
    cmd_user = None
    cmd_cmd = 'mkvpropedit'
    cmd_file_path = in_file_path
    svr_mount_info = is_path_on_network_mount(os.path.dirname(in_file_path))
    if svr_mount_info.is_mount:
        svr = PtPrefs().get_server_by_local_filepath(in_file_path)
        if svr is not None:
            if svr.remote_path() is not None:
                cmd_file_path = in_file_path.replace(
                    svr.local_path(), svr.remote_path())
                cmd_host = svr.host()
                cmd_user = svr.user()
            cmd_cmd = svr.mkvpropedit_path()
    remove_tags_cl = [cmd_cmd, cmd_file_path, '--edit', 'info']
    atom_delete_count = 0
    for atom in NUISANCE_METADATA_ATOMS:
        for case in switch(atom):
            if case(NAME_ID['atom']):
                remove_tags_cl.append('--delete')
                remove_tags_cl.append('title')
                atom_delete_count += 1
                break
            if case():
                break
    if atom_delete_count != 0:
        exec_cli(remove_tags_cl, fake_file_ops, cmd_host, cmd_user)


def remove_nuisance_mp4_metadata_from_file(in_file_path):
    """
    Deletes the metadata atoms from an mp4 file that potentially confuse Plex.

    The atoms specified in the NUISANCE_METADATA_ATOMS constant array are
    deleted from the file 'in_file_path' if present. Currently these are:

        SERIES_NAME_ID['atom']: the series name
        NAME_ID['atom']: the name of the episode
        YEAR_ID['atom']: the year of release
        DESC_ID['atom']: the iTunes description
        LONG_DESC_ID['atom']: a longer description,
        EYETV_XML_LABEL: the show info that EyeTV includes in exported files
        EYETV_RATING_LABEL: Rating (as in PG13) for show
        ARTIST_ID['atom']: the director or producing company
        GENRE_ID['atom']: the iTunes genre, if present
        ALBUM_ARTIST_ID['atom']: the album artist - eg performer of a classic
        COPYRIGHT_ID['atom']: the copyright owner

    In addition any cover art is also deleted from the file. NB The copyright
    atom is removed solely because the BBC iPlayer puts a BBC copyright
    declaration in it and, as Plex uses this filed to fill in the Studio info
    for a movie library files you get weird things like the BBC showing as the
    studio that produced Casablanca...

    Params:
        in_file_path: the file to act on.

    Returns:
        Nothing
    """
    cmd_host = None
    cmd_user = None
    cmd_cmd = 'AtomicParsley'
    cmd_file_path = in_file_path
    svr_mount_info = is_path_on_network_mount(os.path.dirname(in_file_path))
    if svr_mount_info.is_mount:
        svr = PtPrefs().get_server_by_local_filepath(in_file_path)
        if svr is not None:
            if svr.remote_path() is not None:
                cmd_file_path = in_file_path.replace(
                    svr.local_path(), svr.remote_path())
                cmd_host = svr.host()
                cmd_user = svr.user()
            cmd_cmd = svr.atomicparsley_path()
    cmd_list = [cmd_cmd, cmd_file_path, '--overWrite', '--artwork',
                'REMOVE_ALL']
    cur_metadata = get_show_metadata(in_file_path)
    for atom in NUISANCE_METADATA_ATOMS:
        if cur_metadata[atom] is not None:
            if (atom == EYETV_XML_LABEL) or (atom == EYETV_RATING_LABEL):
                cmd_list.append('--rDNSatom')
                cmd_list.append('')
                if (atom == EYETV_XML_LABEL):
                    cmd_list.append('name=iTunMOVI')
                else:
                    cmd_list.append('name=iTunEXTC')
                cmd_list.append('domain=com.apple.iTunes')
            else:
                cmd_list.append('--manualAtomRemove')
                cmd_list.append(u'moov.udta.meta.ilst.' + atom)
    if cur_metadata[COVER_ID['atom']] is not None:
        cmd_list.append('--artwork')
        cmd_list.append('REMOVE_ALL')
    exec_cli(cmd_list, fake_file_ops, host=cmd_host, user=cmd_user)


def clean_nuisance_metadata_safely(in_file_path):
    """
    Saves nuisance metadata before deleting it from a video file.

    Convenience function, saves you having to remember to call two functions.
    The called functions themselves decide what metadata type to extract, save
    and delete.

    Params:
        in_file_path: the file to act on.

    Returns:
        Nothing
    """
    preserve_nuisance_metadata_from_file(in_file_path)
    remove_nuisance_metadata_from_file(in_file_path)


def preserve_nuisance_metadata_from_file(in_file_path):
    """
    Saves NUISANCE_METADATA_ATOMS atoms from a video to an external file.

    Any atom specified in the NUISANCE_METADATA_ATOMS constant array that is
    found in the 'in_metadata' dictionary is saved to a file  with the same
    name stem as 'file_path' but  with the extension 'ptsmd'. Currently these
    are:

        SERIES_NAME_ID['atom']: the series name
        NAME_ID['atom']: the name of the episode
        YEAR_ID['atom']: the year of release
        DESC_ID['atom']: the iTunes description
        LONG_DESC_ID['atom']: A longer description, not sure if iTunes creates
                              this
        EYETV_XML_LABEL: the show info that EyeTV includes in exported files
        EYETV_RATING_LABEL: Rating (as in PG13) for show
        ARTIST_ID['atom']: The director or producing company
        GENRE_ID['atom']: the iTunes genre, if present
        ALBUM_ARTIST_ID['atom']: the album artist - eg performer of a classic
                                 work

    For MPEG-4 container files cover art is extracted as files named:

        <video file name>_artwork_<n>.<ext>

    For Matroska containers we don't need to extract cover art as Plex seems
    to ignore cover art embedded in Matroska files, even when correctly named
    according to Matroska specification.

    Params:
        in_file_path: the file to act on.

    Returns:
        Nothing
    """
    cur_metadata = get_show_metadata(in_file_path)
    save_data = MetadataStore()
    pt_print('Saving metadata atoms:', inset=1)
    save_data.md[TYPE_ATOM] = cur_metadata[TYPE_ATOM]
    found = []
    for atom in NUISANCE_METADATA_ATOMS:
        if cur_metadata[atom] is not None:
            found.append(atom)
            save_data.md[atom] = cur_metadata[atom]
    if len(found) > 0:
        found_str = u"'" + u"', '".join(found) + u"'"
        pt_print(found_str.encode('utf-8'), inset=2)
    save_file_name = os.path.basename(in_file_path + '.ptsmd')
    pt_print("to '" + save_file_name + "'", inset=2)
    if not fake_file_ops:
        with open(save_file_name, 'w+b') as save_file:
            cPickle.dump(save_data, save_file)
    # Save the cover art
    if cur_metadata[TYPE_ATOM] == MP4_CONTAINER:
        cmd_list = ['AtomicParsley', in_file_path, '--extractPix']
        exec_cli(cmd_list, fake_file_ops)
        extracted_files = extracted_cover_art_exists(in_file_path)
        if extracted_files is not None:
            pt_print('Extracted cover art:', inset=1)
            found = []
            for art in extracted_files:
                found.append(art)
            found_str = u"as '" + u"', '".join(found) + u"'"
            pt_print(found_str.encode('utf-8'), inset=2)


COMMAND_HELP_TEXT = \
    """
    Command you want plextool to execute. Accepted commands are:

    'cp' or 'copy' - copy the TARGET(s) into the Plex library;
    'ln' or 'link' - symlink the TARGET(s) into the Plex library;
    'mv' or 'move' - move the TARGET(s) into the Plex library;
    'info' - scan TARGET(s) and report on type, metadata etc;
    'savmd' or 'save-metadata'
        - save nuisance metadata from TARGET(s) to '<file_name>.ptsmd' file;
    'rstmd' or 'restore-metadata'
        - restore nuisance metadata to TARGET(s);
    'rmvmd' or 'remove-metadata'
         - safely remove nuisance metadata from TARGET(s);
    """
# Removed from COMMAND_HELP_TEXT until detailed help is implemented,
# 'help' - provide detailed help on TARGET command;

TYPE_HELP_TEXT = \
    """The type of file we are adding. This is not required if plextool can
    identify the file type from the name form.

    Values may be: 'tv' 'movie' or 'extra'. If you use 'extra' (indicating
    you are adding a Plex library movie extra) then, unless the filename has a
    structure that plextool recognises an an extra, you need to use the -et
    flag to indicate the type of extra being added and the -l flag to add a
    filename.

    The flag will also accept 'iplayer1', 'iplayer2', 'iplayer3', 'iplayer4',
    'eyetvtv', 'eyetvmovie1', eyetvmovie2', 'ittv', 'itmovie', or 'mkvrip'
    which act as hints to the file name type but have no real use - this is
    legacy code I have not removed.
    """

FORCE_HELP_TEXT = \
    """
    Operations will not overwrite existing files by default
    (except for obvious cases such as restoring preserved
    metadata). This flag allows the overwrite to happen. It does
    not over-ride the -d/--dry-run flag. It will also allow
    removal of metadata without saving metadata.
    """

CLEAN_HELP_TEXT = \
    """
    Operations will not, by default, remove metadata embedded in video files.
    By default Plex uses embedded metadata over TheMovieDB/TheTVDB metadata
    which is usually more complete. This flag allows you to force the deletion
    of that embedded metadata. Note, it is probably better to use the 'Agents'
    section of the Plex Server settings to tell Plex to prefer the other
    external metadata, that way you don't loose data which may be useful in the
    future or or to some other aplication.
    """

EPNUM_HELP_TEXT = \
    """
    Episode number (or list of episode numbers) for the episode(s) we are
    adding. If this is the last option before the file(s) to be processed then
    it must be followed with '--' (without the quotes) or else you will get the
    error 'too few arguments'.

    If a single number is passed with multiple files then the number is
    incremented linearly for each additional episode

    If a list of numbers is passed, each number is allocated to the file in the
    same position in the file list. If there are fewer numbers in the list than
    than the number of files passed then the surplus episodes will be numbered
    by incrementing the final number in the episode number list.
    """

EPRNG_HELP_TEXT = \
    """
    Some TV show files contain more than one episode. This option lets you
    indicate how many episodes that is. The episodes must be in sequence
    with no gaps. This option allows you to indicate how many episodes an
    episode file holds.

    If fewer numbers are passed than files then the numbers are used in order
    until exhausted at which point if a number an be extracted from metadata or
    the file name that will be used, otherwise the last number will be used
    repeatedly.
    """

EXTRA_TYPE_HELP_TEXT = \
    """
    The type of the movie extra being added. Valid types are:
    'behindthescenes', 'deleted', 'featurette', 'interview', 'scene', 'short'
    and 'trailer'. The name for the file must be given as a label.  If this is
    the last option before the file(s) to be processed then it must be followed
    with '--' (without the quotes) or else you will get the error 'too few
    arguments'.

    If fewer type flags are passed than files then the flags are used in order
    until exhausted at which point the last flag will be used for all the
    remaining files..
    """

LABEL_HELP_TEXT = \
    """
    'Label' IE episode name (or list of episode name) for the episode(s) we are
    adding. If this is the last option before the file(s) to be processed then
    it must be followed with '--' (without the quotes) or else you will get the
    error 'too few arguments'.

    If fewer labels are passed than files then the labels are used in order
    until exhausted at which point if a label an be extracted from metadata or
    the file name that will be used, otherwise no label will be used.
    """

TARGET_HELP_TEXT = \
    """
    One or more source files (or directories containing source files) to add to
    the Plex library.

    If there are local media files (artwork, subtitles etc) that you would like
    to add to the Plex library it will be done automatically for appropriately
    named files int he same directory as the target video file. The naming
    convention is as follows:

        Subtitle files must end in one of the extensions 'srt', 'smi', 'ass' or
        'ssa' and may have that extension preceded by a language indicator,
        either the 2-letter ISO-639-1 code or the 3 letter ISO-639-2/B code.
        For example the file "Great Movie.mp4" could have the following
        associated sub-title files:

            "Great Movie.srt"
            "Great Movie.en.ssa"
            "Great Movie.eng.smi"

        The file will be added with the same name stem as the final video file
        but with one of the two part extensions shown above. Where an incoming
        file has a language indicator it is preserved. An incoming subtitle
        file with no language indicator will have one added for the default
        language because Plex treats subtitle files with no language indicator
        as "unknown" language. This will overwrite any subtitle file with the
        same extension and which is in the default language.

        Artwork files must have one of the extensions 'jpg', 'jpeg', 'png' or
        'tbn' which may be preceded by a suffix indicating the artwork type,
        further optionally followed by a number if there is more than one of
        that type of artwork.

            Type                                Ends in
            ====                                =======
            Movie poster or TV episode artwork  .ext
            Poster (for TV Series or Movie)     -poster[-N].ext
            Background                          -fanart[-N].ext
            Banner                              -banner[-N].ext
            Season banner                       -sbanner[-N].ext
            Season poster                       -season[-N].ext
            Theme Music                         -theme.ext

        An artwork file with the same name as the target file will be used as
        the poster for a movie or the episode art for a TV show. This choice
        was made because those two possibilities are by far the most common
        artwork types to be added so this saves effort in preparing your media.

        You may not have more than one piece of episode artwork per episode, or
        posters and banners for "Specials" seasons.
    """

PT_FILE_COMMANDS = ['cp', 'copy', 'ln', 'link', 'mv', 'move', 'savmd',
                    'save-metadata', 'rmvmd', 'remove-metadata', 'rstmd',
                    'restore-metadata', 'info']
PT_COMMANDS = copy.copy(PT_FILE_COMMANDS)
# Removed until detailed help is implemented,
# PT_COMMANDS.append('help')


def common_parse_args(description):
    """
    Called by all the plextool tools to parse the command line.

    The copy, cp, move, mv, link, ln,  info, savmd, save-metadata,
    rstmd, restore-metadata and rmvmd all call this shortly after entry to get
    their arguments and set up help.

    Params:
        description: SHort description of the calling tool's function'.

    Returns:
        The parsed argument object.
    """
    # parse options and args
    parser = ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=description)

    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s ("+__version__+")")

    parser.add_argument(
        "command", metavar='COMMAND', type=str, nargs=1,
        choices=PT_COMMANDS, action=CommandAction, help=COMMAND_HELP_TEXT)

    parser.add_argument(
        "-n", "--name", dest="show_name", metavar='SHOW_NAME',
        type=str, help="The name of the TV series or movie.", default=None)

    parser.add_argument(
        "-s", "--season-num", dest="season_num", metavar='SEASON_NUM',
        type=str, help="The season of the TV series.", default=None)

    parser.add_argument(
        '-e', '--episode-num-list', dest="episode_num_list",
        metavar='EPISODE_NUM', type=str, nargs='*',
        help=EPNUM_HELP_TEXT, default=None)

    parser.add_argument(
        '-er', '--episode-range-list', dest="episode_range_list",
        metavar='EPISODE_RANGE', type=str, nargs='*',
        help=EPRNG_HELP_TEXT, default=None)

    parser.add_argument(
        '-et', '--extra-type-list', dest="extra_type_list",
        metavar='EXTRA_TYPE', type=str, nargs='*', choices=EXTRA_FLAGS,
        help=EXTRA_TYPE_HELP_TEXT, default=None)

    parser.add_argument(
        '-l', '--label-list', dest="label_list", metavar='LABEL_LIST',
        nargs='*', type=str, help=LABEL_HELP_TEXT, default=None)

    choice_list = []
    for name_type in NAME_TYPE_ARRAY:
        choice_list.append(name_type['id'])
    parser.add_argument(
        '-t', '--type', dest="name_type", metavar='NAME_TYPE', type=str,
        choices=choice_list, help=TYPE_HELP_TEXT, default=None)

    parser.add_argument(
        '-y', '--year', dest="movie_year", metavar='MOVIE_YEAR', type=str,
        help="The year this movie was made.", default=None)

    parser.add_argument(
        "-d", "--dry-run", dest="dry_run",  action="store_true", default=False,
        help="Operations that change files or directory structure will only "
             "be printed out, not actually executed.")

    parser.add_argument(
        "-f", "--force", dest="force",  action="store_true", default=False,
        help=FORCE_HELP_TEXT)

    parser.add_argument(
        "-c", "--clean", dest="clean_md",  action="store_true", default=False,
        help=CLEAN_HELP_TEXT)

    parser.add_argument(
        "-a", "--alt-server", dest="alt_server",  metavar='ALT_SERVER_NAME',
        type=str, help="Name of an alternative server.", default=None)

    itunes_svr = PtPrefs().get_itunes_server()
    parser.add_argument(
        "-ls", "--local-stem", metavar='LOCAL_STEM',
        dest="local_stem", type=str, default=itunes_svr.local_path(),
        help="The section of the destination path names unique to the mount "
             "on the local host.")
    parser.add_argument(
        "-rs", "--remote-stem", metavar='REMOTE_STEM',
        dest="remote_stem", type=str, default=itunes_svr.remote_path(),
        help="The section of the destination path name unique to the remote "
             "host")

    parser.add_argument(
        "targets", metavar='FILE|DIR', type=str, nargs='+',
        help=TARGET_HELP_TEXT)

    args = parser.parse_args()

    return args


def tool_main(in_tool_module, args):
    """
    The main target processing function for all the tools.

    When a tool script calls this function it should pass the tool's module
    name as well as the namespace of parsed args. The function will then call
    back to the process_file() and process_dir() functions of the tool.

    This is done to minimise code duplication.

    Params:
        in_tool_module: The name of the tool module.

        args:           An object containing the parsed arguments from the
                        invoking command line.

    Returns:
        Nothing
    """
    if is_root():
        print >> sys.stderr, \
            get_script_name() + ": Error: This script must not run as root. "\
            "Exiting..."
        sys.exit(1)

    if args.dry_run:
        fake_file_ops = True  # noqa: F841

    if args.episode_num_list is not None:
        args.episode_num = args.episode_num_list[0]
    else:
        args.episode_num = None

    if args.episode_range_list is not None:
        args.episode_range = args.episode_range_list[0]
        if int(args.episode_range) < 2:
            args.episode_range = None
        args.episode_range_list = args.episode_range_list[1:]
    else:
        args.episode_range = None

    if args.extra_type_list is not None:
        args.extra_type = args.extra_type_list[0]
    else:
        args.extra_type = None

    if args.label_list is not None:
        args.label = args.label_list[0]
        args.label_list = args.label_list[1:]
    else:
        args.label = None

    tool = importlib.import_module('plextools.' + in_tool_module)

    for file_path in args.targets:
        if os.path.exists(file_path):
            if os.path.isfile(file_path):
                tool.process_file(file_path, args)
                if args.episode_num_list is not None:
                    if len(args.episode_num_list) == 1:
                        args.episode_num = str(int(args.episode_num) + 1)
                    else:
                        args.episode_num = args.episode_num_list[1]
                        args.episode_num_list = args.episode_num_list[1:]
                if args.episode_range_list is not None:
                    if len(args.episode_range_list) >= 1:
                        args.episode_range = args.episode_range_list[0]
                        if int(args.episode_range) < 2:
                            args.episode_range = None
                        args.episode_range_list = args.episode_range_list[1:]
                    else:
                        args.episode_range = None
                        args.episode_range_list = None
                if args.extra_type_list is not None:
                    if len(args.extra_type_list) == 1:
                        args.extra_type = args.extra_type_list[0]
                    else:
                        args.extra_type = args.extra_type_list[1]
                        args.extra_type_list = args.extra_type_list[1:]
                if args.label_list is not None:
                    if len(args.label_list) >= 1:
                        args.label = args.label_list[0]
                        args.label_list = args.label_list[1:]
                    else:
                        args.label = None
                        args.label_list = None
            elif os.path.isdir(file_path):
                tool.process_dir(file_path, args)
        else:
            pt_print_err("The file '" + file_path + "' "
                         "does not exist. Perhaps it is a broken symbolic "
                         "link or alias")


def print_tool_desc(in_tool_name):
    """
    Common function to print tool information on initial call.

    Params:
        in_tool_name: The name of the calling tool.

    Returns:
        Nothing
"""
    pt_print('Plex Tools - ' + colors.BOLD + get_script_name() +
             colors.END + ' version ' + get_plextools_version_string())
    if in_tool_name is not None:
        pt_print(colors.FG_BLUE + in_tool_name + colors.END, inset=1)


def is_path_on_network_mount(in_path, verbose=False):
    """
    Test if the passed path ends up on a network mount

    Params:
        in_path: The file/folder path to test.
        verbose: If True emits progess info to sys.stdout, default=FALSE

    Returns:
        If fails:       None
        If succeeds:    An IsMountPathReply object containing the information
                        obtained.
    """
    MOUNT_REGEX = r"(//(.*)@(.*)/(.*))( on )(.*)( \(.*\))"
    try:
        raw_output = subprocess.Popen(["mount"],
                                      stdout=subprocess.PIPE).communicate()[0]
    except:
        return None
    true_path = os.path.realpath(in_path)
    lines = raw_output.split('\n')
    line_count = 0
    lines_len = len(lines)
    out_val = IsMountPathReply()
    while line_count < lines_len:
        list_line = lines[line_count]
        parsed_mount = re.search(MOUNT_REGEX, list_line)
        if parsed_mount is not None:
            mount_point = parsed_mount.groups()[5]
            if true_path[0:len(mount_point)] == mount_point:
                if verbose:
                    print 'The path \'{}\' is on a network mount.'\
                          .format(true_path)
                out_val = IsMountPathReply(True, parsed_mount.groups()[1],
                                           parsed_mount.groups()[2],
                                           parsed_mount.groups()[3],
                                           mount_point)
                break
        line_count += 1
    return out_val


def are_paths_on_same_server(in_path1, in_path2):
    """
    Tests if two file/folder paths end on the same network mount.

    Params:
        in_path1, in_path2: The paths to test.

    Returns:
        True or False, as appropriate
    """
    ret_val = False
    path1_info = is_path_on_network_mount(in_path1)
    path2_info = is_path_on_network_mount(in_path2)
    if path1_info.is_mount and path2_info.is_mount:
        if path1_info.server == path2_info.server:
            ret_val = True
    return ret_val


def are_paths_on_same_local_vol(in_path1, in_path2):
    """
    Tests if the paths are on a volume with the same local mount point.

    Params:
        in_path1, in_path2: The paths to test.

    Returns:
        True or False, as appropriate
    """
    ret_val = False
    drive1, parts1 = split_path(os.path.abspath(in_path1))
    drive2, parts2 = split_path(os.path.abspath(in_path2))
    if (drive1 != '' or drive2 != ''):
        if (drive1 == drive2):
            # Windows, same drive identifier
            ret_val = True
    else:
        if parts1[1] == parts2[1]:
            if parts1[1] == 'Volumes':
                if parts1[2] == parts1[2]:
                    ret_val = True
            else:
                ret_val = True
    return ret_val


def ensure_nomedia(in_file_path):
    """
    Prevent Kodi creating spurious entries in its video file lists.

    Kodi will search the movie dir sub-directories for other videos which
    causes some very odd looking things to appear in Kodi's file list if
    you have local extra files.

    This function creates a file called '.nomedia' in a directory, telling
    Kodi to ignore that directory's contents.

    Params:
        in_file_path - the path to a file. '.nomedia' will be created in the
                       same directory as this file.

    Returns:
        Nothing
    """
    d = os.path.dirname(os.path.abspath(in_file_path))
    no_media = d + "/.nomedia"
    cli_list = ['touch', no_media]
    exec_cli(cli_list, fake_file_ops)


def get_local_media_files(in_file_path):
    """
    Get a list of local media files associated with the indicated video file.

    Looks in the same directory as the passed-in file for local media files
    whose names begin with the entirety of the passed in filename (without
    the extension). Currently the function recognises subtitle, artwork and
    MP3 files.

    Subtitle files must end in one of the extensions 'srt', 'smi', 'ass' or
    'ssa' and may have that extension preceded by a language indicator, either
    the 2-letter ISO-639-1 code or the 3 letter ISO-639-2/B code. For example
    the file "Great Movie.mp4" could have the following associated sub-title
    files:

        "Great Movie.srt"
        "Great Movie.en.ssa"
        "Great Movie.eng.smi"

    Artwork files must have one of the extensions 'jpg', 'jpeg', 'png' or 'tbn'
    which may be preceded by a suffix indicating the artwork type, further
    optionally followed by a number if there is more than one of that type of
    artwork.

        Type                                Ends in
        ====                                =======
        Movie poster or TV episode artwork  .ext
        Poster (for TV or Movie)            -poster[-N].ext
        Background                          -fanart[-N].ext
        Banner                              -banner[-N].ext
        Season banner                       -sbanner[-N].ext
        Season poster                       -season[-N].ext
        Theme Music                         -theme.ext

    Params:
        in_file_path - the path to a video file.

    Returns:
        A list: Empty if no local media files found, otherwise populated with
        tuples identifying the found matching files. The tuples have the full
        file path as the first item and the type of the file as the second.
        The type is '' for episode artwork/movie poster files, one of 'poster',
        'fanart', 'banner', 'sbanner', 'season' for other artwork and 'subs'
        for subtitle files.

    """
    out_file_path = []
    file_dir = os.path.dirname(in_file_path)
    file_name = os.path.basename(in_file_path)
    file_stem = os.path.splitext(file_name)[0]

    full_file_list = os.listdir(file_dir)
    med_file_list = []
    for file_n in full_file_list:
        if fnmatch.fnmatch(file_n, file_stem + "*"):
            if file_n != file_name:
                # no point in adding original video file
                med_file_list.append(file_n)
    # Sort so progress info looks more systematic. Have to do this twice so
    # files with no suffix are ordered before those of the same kind with no
    # suffix. If not alternate local media are added in the reverse of the
    # order implied by their suffixes.
    med_file_list.sort(key=lambda f: os.path.splitext(f)[0])
    med_file_list.sort(key=lambda f: os.path.splitext(f)[1])

    for test_n in med_file_list:
        file_ext = os.path.splitext(test_n)[1]
        test_ext = file_ext[1:].lower()
        if test_ext in SUBTITLE_EXTENSIONS:
            # probably a subtitle file
            out_file_path.append((os.path.join(file_dir, test_n), 'subs'))
        elif test_ext == 'mp3':
            # probably a theme music file
            out_file_path.append((os.path.join(file_dir, test_n), 'theme'))
        elif test_ext in ARTWORK_EXTENSIONS:
            if test_n == file_stem + file_ext:
                # movie poster or episode artwork
                out_file_path.append((os.path.join(file_dir, test_n), ''))
            else:
                # search for structural artwork
                for test_sfx in LOCAL_MEDIA_SUFFIXES:
                    if fnmatch.fnmatch(test_n, file_stem + "-" + test_sfx +
                                       "*"):
                        out_file_path.append((os.path.join(file_dir, test_n),
                                             test_sfx))
                        break
    return out_file_path


def get_out_sub_ext(in_sub_file_path):
    """
    Check a subtitle file and get the extension to use in a Plex library.

    Params:
        in_sub_file_path: The file to examine.

    Returns:
        Nothing
    """
    out_code = ''
    f_parts = os.path.splitext(os.path.basename(in_sub_file_path))
    test_code = f_parts[0][-3:].lower()
    if test_code[0] == '.':
        # May be an ISO-639-1 language code, see if is in list.
        if test_code[-2:] in ISO_639_1_CODES:
            out_code = test_code
    else:
        # Definitely not ISO-639-1, test for ISO-639-2/B code
        test_code = f_parts[0][-4:].lower()
        if test_code[0] == '.':
            # May be an ISO-639-2/B language code, see if is in list.
            if test_code[-3:] in ISO_639_2B_CODES:
                out_code = test_code
    if out_code != "":
        out_code = out_code + f_parts[1].lower()
    else:
        out_code = '.' + PtPrefs().get_iso_lang_code() + f_parts[1].lower()
    return out_code


def _prime_idx_dict():
    """
    Intialises a dictionary to keep track of media file indices.

    When _get_local_media_dest_path() is called it is passed a dictionary that
    tracks how many of each local media file type have been found so far for
    the video file. This functions generates and initialises that dictionary.

    Returns:
        A dictionary with the required keys, all set to value 0.
    """
    out_dict = {
                POSTER_SFX: 0,
                FANART_SFX: 0,
                BANNER_SFX: 0,
                SBANNER_SFX: 0,
                SEASON_SFX: 0,
                THEME_SFX: 0
                }
    return out_dict


def _get_local_media_dest_path(in_source_art_path, in_dest_video_path,
                               idx_dict):
    """
    Generate the name and path for an artwork file associated with a video.

    Examines a source file name first to determine what the artwork type is.
    The file must be named following this convention:

        Type                                Ends in
        ====                                =======
        Movie poster or TV episode artwork  .ext
        Poster artwork (for TV or Movie)    -poster[-N].ext
        Background artwork                  -fanart[-N].ext
        Banner artwork                      -banner[-N].ext
        Season banner artwork               -sbanner[-N].ext
        Season poster                       -season[-N].ext
        Theme music                         -theme.ext      (not working yet)

    and then determines the correct name and path for the destination file.

    Params
        in_source_art_path: A tuple consisting of:
                                (the path to the art file to examine,
                                 the type of structural artwork)

        in_dest_video_path: The path of the video file when added to the Plex
                            library.

        idx_dict:           A dictionary used to keep track of how many of an
                            artwork type have already been returned for this
                            video file. Passed in by calling function and
                            mutated by this function when new artwork is given
                            a destination name. Avoids collisions.

    Returns:
            a string with the full pathname for the destination file.
        or:
            None - if cannot build valid destination path
    """
    out_path = None
    video_type = detect_file_name_type(in_dest_video_path, False)
    src_file_name = os.path.basename(in_source_art_path[0])
    src_file_stem, src_file_ext = os.path.splitext(src_file_name)
    src_test_ext = src_file_ext[1:].lower()
    src_file_type = in_source_art_path[1]
    dest_path_root, dest_path_fname = os.path.split(in_dest_video_path)
    dest_path_stem = os.path.splitext(in_dest_video_path)[0]
    dest_ext = '.' + src_test_ext

    if video_type == TYPE_PLEX_TV['id']:
        # TV series have some local media that movies do not
        dest_path_season = dest_path_root
        dest_path_root = os.path.dirname(dest_path_root)
        for case in switch(src_file_type):
            if case('theme'):
                # Theme music, easy.
                out_path = os.path.join(dest_path_root, 'theme' +
                                        dest_ext)
                break
            if case('season'):
                # Season poster
                dest_parsed_name = re.search(PLEX_TV_REGEX, dest_path_fname)
                dest_season = dest_parsed_name.group(4)
                if dest_season == '00':
                    out_path = os.path.join(dest_path_season,
                                            'season-specials-poster' +
                                            dest_ext)
                else:
                    name_tail = dest_season
                    if idx_dict[SEASON_SFX] > 0:
                        idx = string.lowercase[idx_dict[SEASON_SFX] - 1]
                        name_tail = name_tail + idx
                    idx_dict[SEASON_SFX] += 1
                    out_path = os.path.join(dest_path_season, 'Season' +
                                            name_tail + dest_ext)
                break
            if case('sbanner'):
                # Season banner
                dest_parsed_name = re.search(PLEX_TV_REGEX, dest_path_fname)
                dest_season = dest_parsed_name.group(4)
                if dest_season == '00':
                    out_path = os.path.join(dest_path_season,
                                            'season-specials-banner' +
                                            dest_ext)
                else:
                    name_tail = dest_season + '-banner'
                    if idx_dict[SBANNER_SFX] > 0:
                        idx = string.lowercase[idx_dict[SBANNER_SFX] - 1]
                        name_tail = name_tail + idx
                    idx_dict[SBANNER_SFX] += 1
                    out_path = os.path.join(dest_path_season, 'Season' +
                                            name_tail + dest_ext)
                break

    # Common local media files
    for case in switch(src_file_type):
        if case(''):
            # Episode artwork or movie poster.
            if video_type == TYPE_PLEX_TV['id']:
                # Episode artwork
                out_path = dest_path_stem + dest_ext
            else:
                # Movie poster
                idx = ''
                if idx_dict[POSTER_SFX] > 0:
                    idx = '-' + str(idx_dict[POSTER_SFX])
                idx_dict[POSTER_SFX] += 1
                out_path = os.path.join(dest_path_root,
                                        'poster' + idx + dest_ext)
            break
        if case('poster'):
            # Show or movie poster
            idx = ''
            if idx_dict[POSTER_SFX] > 0:
                idx = '-' + str(idx_dict[POSTER_SFX])
            idx_dict[POSTER_SFX] += 1
            out_path = os.path.join(dest_path_root,
                                    'poster' + idx + dest_ext)
            break
        if case('fanart'):
            # Background art for Plex
            idx = ''
            if idx_dict[FANART_SFX] > 0:
                idx = '-' + str(idx_dict[FANART_SFX])
            idx_dict[FANART_SFX] += 1
            out_path = os.path.join(dest_path_root,
                                    'fanart' + idx + dest_ext)
            break
        if case('banner'):
            # Show or movie banner
            idx = ''
            if idx_dict[BANNER_SFX] > 0:
                idx = '-' + str(idx_dict[BANNER_SFX])
            idx_dict[BANNER_SFX] += 1
            out_path = os.path.join(dest_path_root,
                                    'banner' + idx + dest_ext)
            break

    return out_path


def add_matching_local_media(in_src_video_path, in_dest_video_path, op,
                             op_args=None, host=None, user=None):
    """
    Add artwork, subtitle and other local media files.

    If artwork, subtitle files or other local media type exists for the source
    video file (in the same directory) then it is added to the plex library,
    appropriately renamed.

    If files with the new name already exist at the destination location they
    will be overwritten

    Params:
        in_src_video_path:  the path to a video file.
        in_dest_video_path: the path of the video file when added to the Plex
                            library.
        op:                 the operation which will be performed on the
                            source file to add it to the Plex library:
                            'cp' (copy), 'ln' (link) or 'mv' (move).
        op_args:            list of arguments required by the addition
                            operation.
        host:               if the command is to be executed on a remote host
                            pass host name here.
        user:               if the command is to be executed on a remote host
                            pass the username here.
    """
    pt_print("Checking for local media...", inset=1)
    src_local_media_file_paths = get_local_media_files(in_src_video_path)
    if len(src_local_media_file_paths) is not 0:
        # Preserve state between _get_local_media_dest_path() calls
        idx_dict = _prime_idx_dict()

        for src_local_media_file in src_local_media_file_paths:
            src_ext = os.path.splitext(src_local_media_file[0])[1].lower()
            bare_ext = src_ext[1:]
            split_dest = os.path.splitext(in_dest_video_path)
            if bare_ext.lower() in SUBTITLE_EXTENSIONS:
                out_ext = get_out_sub_ext(src_local_media_file[0])
                dest_local_media_file_path = split_dest[0] + out_ext
                pt_print("Copying subtitles as '" + colors.BOLD +
                         os.path.basename(dest_local_media_file_path) +
                         colors.END + "'...", inset=2)
            if bare_ext in ARTWORK_EXTENSIONS:
                dest_local_media_file_path = _get_local_media_dest_path(
                    src_local_media_file, in_dest_video_path, idx_dict)
                pt_print("Copying artwork as '" + colors.BOLD +
                         os.path.basename(dest_local_media_file_path) +
                         colors.END + "'...", inset=2)
            if bare_ext in MUSIC_EXTENSIONS:
                dest_local_media_file_path = _get_local_media_dest_path(
                    src_local_media_file, in_dest_video_path, idx_dict)
                pt_print("Copying artwork as '" + colors.BOLD +
                         os.path.basename(dest_local_media_file_path) +
                         colors.END + "'...", inset=2)
            cli_list = [op]
            if op_args is not None:
                for arg in op_args:
                    cli_list.append(arg)
            cli_list.append(src_local_media_file[0])
            cli_list.append(dest_local_media_file_path)
            exec_cli(cli_list, fake_file_ops, host, user)
        pt_print("Local media copied.", inset=2)
    else:
        pt_print("None found.", inset=2)


def add_file_to_plex(file_info, local_dest_full_path, args, in_op):
    """
    Places the file into the Plex Library at the given path.

    If it is a file type that contains mpeg-4 metadata then the function
    also deletes those atoms from the file that may confuse the Plex
    server by over-riding the video data implicit in the file name. This is
    necessary because sometimes that atomic data is wrong, or truncated from
    what it should be.

    This is inteneded to be a common function called by the 'copy'', 'move'
    and 'link' command modules rather than duplicating code what is pretty
    much the same routine in each module. I have not yet finished implementing
    it and currently no routine calls it.

    Params:
        file_info:              A FileInfo object representing he file to add

        local_dest_full_path:   The destination local file path

        args:                   Original command line args passed through

        in_op:                  Way to add the file. 'cp' if copying, 'mv' if
                                moving.
    Returns:
        Nothing
    """
    if in_op == 'cp':
        full_op_name = 'Copy'
        full_op_proc = 'Copying'
    elif in_op == 'mv':
        full_op_name = 'Move'
        full_op_proc = 'Moving'
    else:
        raise PTError("Unrecognised library addition operation '" +
                      colors.BOLD + in_op + colors.ALL_OFF + "'.")
    pt_print(full_op_proc + " file '" + colors.BOLD + file_info.file_name +
             colors.END + "' to Plex library", inset=1)
    if os.path.exists(local_dest_full_path) and not args.force:
        pt_print(colors.BOLD + "Error:" + colors.END + " '" + colors.BOLD +
                 local_dest_full_path + colors.END + "' already exists in "
                 "the Plex library.",
                 inset=2)
    else:
        if ensure_dir(local_dest_full_path, fake_file_ops):
            pt_print("Creating missing '" +
                     os.path.dirname(local_dest_full_path) +
                     "' directory", inset=1)
        new_file_name = os.path.basename(local_dest_full_path)
        # set up ivalues for exec_cli
        src_file_path = file_info.full_file_path
        dst_file_path = local_dest_full_path
        cli_host = None
        cli_user = None
        if are_paths_on_same_server(src_file_path, dst_file_path):
            # paths are on same file host so remote copy is more efficient
            src_svr = PtPrefs().get_server_by_local_filepath(src_file_path)
            dst_svr = PtPrefs().get_server_by_local_filepath(dst_file_path)
            if (src_svr is not None) and (dst_svr is not None):
                if (src_svr.remote_path() is not None) and \
                        (dst_svr.remote_path() is not None):
                    src_file_path = src_file_path.replace(
                        src_svr.local_path(), src_svr.remote_path())
                    dst_file_path = dst_file_path.replace(
                        dst_svr.local_path(), dst_svr.remote_path())
                    cli_host = src_svr.host()
                    cli_user = src_svr.user()
        cli_list = [in_op, src_file_path, dst_file_path]
        pt_print(full_op_proc + " as '" + colors.BOLD + new_file_name +
                 colors.END + "'...", inset=1)
        exec_cli(cli_list, fake_file_ops, cli_host, cli_user)
        pt_print(full_op_name + " complete.", inset=1)
        if in_op == 'mv':
            # This is hacky and I don't really like it
            exec_cli(['touch', local_dest_full_path], fake_file_ops)
        if args.clean_md:
            if check_for_nuisance_metadata(local_dest_full_path):
                remove_nuisance_metadata_from_file(local_dest_full_path)

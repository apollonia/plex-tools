# -*- coding: utf-8 -*-
"""Maintain and provide access to persistant state for plextools."""

# standard library imports
import os

# package specific imports
from prefs import Prefs, PrefsError
from util import switch, ISO_639_1_CODES, ISO_639_2B_CODES


# Atributes -------------------------------------------------------------------
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# same format as sys.version_info: "A tuple containing the five components of
# the version number: major, minor, micro, releaselevel, and serial. All
# values except releaselevel are integers; the release level is 'alpha',
# 'beta', 'candidate', or 'final'. The version_info value corresponding to the
# Python version 2.0 is (2, 0, 0, 'final', 0)."  Additionally we use a
# releaselevel of 'dev' for unreleased under-development code.
VERSION_INFO = (1, 0, 0, 'dev', 1)
__version__ = '1.0.0a1'


# Interface -------------------------------------------------------------------
__all__ = [
                # Classes
                'PtServerInfo', 'PtPrefs',
                # Path constants and defaults
                'MNT_ITUNES_STEM', 'SVR_ITUNES_STEM',
                'MNT_PLEX_MOVIE_LIBRARY_PATH', 'MNT_PLEX_TV_LIBRARY_PATH',
                'SVR_PLEX_MOVIE_LIBRARY_PATH'
            ]


# Constants -------------------------------------------------------------------
PT_PREFS_FILE_PATH = os.path.expanduser('~/.plextools.plist')
PT_PREFS_VERSION = 5

MNT_ITUNES_STEM = "/Volumes/Shared Media"
SVR_ITUNES_STEM = "/Volumes/Media Store/Users/sharedmedia"
ITUNES_SVR_HOST = "roadhouse.local"
ITUNES_SVR_USER = "bofh"
ITUNES_AP_PATH = "/usr/local/bin/AtomicParsley"
ITUNES_MI_PATH = "/usr/local/bin/mediainfo"
ITUNES_PE_PATH = "/usr/local/bin/mkvpropedit"

MNT_PLEX_MOVIE_LIBRARY_PATH = "/Volumes/Shared Media/Plex Media Libraries/"\
                              "Movies"
SVR_PLEX_MOVIE_LIBRARY_PATH = "/Volumes/Media Store/Users/sharedmedia/"\
                              "Plex Media Libraries/Movies"
PLEX_MOVIE_SVR_HOST = "roadhouse.local"
PLEX_MOVIE_SVR_USER = "bofh"
PLEX_MOVIE_AP_PATH = "/usr/local/bin/AtomicParsley"
PLEX_MOVIE_MI_PATH = "/usr/local/bin/mediainfo"
PLEX_MOVIE_PE_PATH = "/usr/local/bin/mkvpropedit"

MNT_PLEX_TV_LIBRARY_PATH = "/Volumes/Shared Media/Plex Media Libraries/"\
                           "TV Shows"
SVR_PLEX_TV_LIBRARY_PATH = "/Volumes/Media Store/Users/sharedmedia/"\
                           "Plex Media Libraries/TV Shows"
PLEX_TV_SVR_HOST = "roadhouse.local"
PLEX_TV_SVR_USER = "bofh"
PLEX_TV_AP_PATH = "/usr/local/bin/AtomicParsley"
PLEX_TV_MI_PATH = "/usr/local/bin/mediainfo"
PLEX_TV_PE_PATH = "/usr/local/bin/mkvpropedit"

ITUNES_NAME = 'Roadhouse iTunes'
MOVIES_NAME = 'Roadhouse Movies'
TVSHOWS_NAME = 'Roadhouse TV Shows'

ISO_LANG_CODE = 'en'

VERSION_PREF_NAME = 'version'
SERVER_ARRAY_PREF_NAME = 'server_array'
ITUNES_SVR_PREF_NAME = 'itunes_svr_name'
MOVIES_SVR_PREF_NAME = 'movies_svr_name'
TV_SVR_PREF_NAME = 'tvshows_svr_name'
SUB_LANG_CODE_PREF_NAME = 'sub_lang_code'

ITUNES_SVR_ID = 'itunes'
MOVIE_SVR_ID = 'movies'
TV_SVR_ID = 'tv'

SVR_INFO_TEXT = "Information about your server goes here."

NAME_KEY = 'name'
LOCAL_PATH_KEY = 'mount_path_stem'
REMOTE_PATH_KEY = 'server_path_stem'
HOST_KEY = 'server_host'
USER_KEY = 'server_user'
AP_PATH_KEY = 'atomicparsley_path'
MI_PATH_KEY = 'mediainfo_path'
PE_PATH_KEY = 'mkvpropedit_path'
SVR_INFO_KEY = 'server_info'

ITUNES_INFO = {NAME_KEY: ITUNES_NAME,
               SVR_INFO_KEY: SVR_INFO_TEXT,
               LOCAL_PATH_KEY: MNT_ITUNES_STEM,
               REMOTE_PATH_KEY: SVR_ITUNES_STEM,
               HOST_KEY: ITUNES_SVR_HOST,
               USER_KEY: ITUNES_SVR_USER,
               AP_PATH_KEY: ITUNES_AP_PATH,
               MI_PATH_KEY: ITUNES_MI_PATH,
               PE_PATH_KEY: ITUNES_PE_PATH}

PLEX_MOVIES_INFO = {NAME_KEY: MOVIES_NAME,
                    SVR_INFO_KEY: SVR_INFO_TEXT,
                    LOCAL_PATH_KEY: MNT_PLEX_MOVIE_LIBRARY_PATH,
                    REMOTE_PATH_KEY: SVR_PLEX_MOVIE_LIBRARY_PATH,
                    HOST_KEY: PLEX_MOVIE_SVR_HOST,
                    USER_KEY: PLEX_MOVIE_SVR_USER,
                    AP_PATH_KEY: PLEX_MOVIE_AP_PATH,
                    MI_PATH_KEY: PLEX_MOVIE_MI_PATH,
                    PE_PATH_KEY: PLEX_MOVIE_PE_PATH}

PLEX_TV_INFO = {NAME_KEY: TVSHOWS_NAME,
                SVR_INFO_KEY: SVR_INFO_TEXT,
                LOCAL_PATH_KEY: MNT_PLEX_TV_LIBRARY_PATH,
                REMOTE_PATH_KEY: SVR_PLEX_TV_LIBRARY_PATH,
                HOST_KEY: PLEX_TV_SVR_HOST,
                USER_KEY: PLEX_TV_SVR_USER,
                AP_PATH_KEY: PLEX_TV_AP_PATH,
                MI_PATH_KEY: PLEX_TV_MI_PATH,
                PE_PATH_KEY: PLEX_TV_PE_PATH}


# Implementation --------------------------------------------------------------


class PtServerInfo():
    def __init__(self, in_info_dict=None):
        if in_info_dict is None:
            err = PrefsError('Cannot create server object without info dict')
            raise err
        self.info_dict = in_info_dict

    def _storage_repr(self):
        return self.info_dict

    def name(self):
        return self.info_dict[NAME_KEY]

    def set_name(self, in_name):
        self.info_dict[NAME_KEY] = in_name

    def local_path(self):
        return self.info_dict[LOCAL_PATH_KEY]

    def set_local_path(self, in_path):
        self.info_dict[LOCAL_PATH_KEY] = os.path.abspath(in_path)

    def remote_path(self):
        if REMOTE_PATH_KEY not in self.info_dict:
            return None
        else:
            return self.info_dict[REMOTE_PATH_KEY]

    def set_remote_path(self, in_path):
        self.info_dict[REMOTE_PATH_KEY] = in_path

    def host(self):
        return self.info_dict[HOST_KEY]

    def set_host(self, in_host):
        self.info_dict[HOST_KEY] = in_host

    def user(self):
        return self.info_dict[USER_KEY]

    def set_user(self, in_name):
        self.info_dict[USER_KEY] = in_name

    def atomicparsley_path(self):
        return self.info_dict[AP_PATH_KEY]

    def set_atomicparsley_path(self, in_path):
        self.info_dict[AP_PATH_KEY] = in_path

    def mediainfo_path(self):
        return self.info_dict[MI_PATH_KEY]

    def set_mediainfo_path(self, in_path):
        self.info_dict[MI_PATH_KEY] = in_path

    def mkvpropedit_path(self):
        return self.info_dict[PE_PATH_KEY]

    def set_mkvpropedit_path(self, in_path):
        self.info_dict[PE_PATH_KEY] = in_path

    def info(self):
        return self.info_dict[SVR_INFO_KEY]

    def set_info(self, in_text):
        self.info_dict[SVR_INFO_KEY] = in_text


class PtPrefs(Prefs):
    """Provide access to and maintain persistant state for plextools."""
    def __init__(self, in_prefs_file=None):
        if in_prefs_file is None:
            in_prefs_file = PT_PREFS_FILE_PATH
        Prefs.__init__(self, in_prefs_file)
        # Initialise preferences if not loaded from file
        if not (VERSION_PREF_NAME in self.prefs):
            self.set_pref(VERSION_PREF_NAME, PT_PREFS_VERSION)
        if not (SERVER_ARRAY_PREF_NAME in self.prefs):
            self.set_pref(SERVER_ARRAY_PREF_NAME,
                          [ITUNES_INFO, PLEX_MOVIES_INFO, PLEX_TV_INFO])
        if not (ITUNES_SVR_PREF_NAME in self.prefs):
            self.set_pref(ITUNES_SVR_PREF_NAME, ITUNES_NAME)
        if not (MOVIES_SVR_PREF_NAME in self.prefs):
            self.set_pref(MOVIES_SVR_PREF_NAME, MOVIES_NAME)
        if not (TV_SVR_PREF_NAME in self.prefs):
            self.set_pref(TV_SVR_PREF_NAME, TVSHOWS_NAME)
        if not (SUB_LANG_CODE_PREF_NAME in self.prefs):
            self.set_pref(SUB_LANG_CODE_PREF_NAME, ISO_LANG_CODE)
        if self.get_pref(VERSION_PREF_NAME) < PT_PREFS_VERSION:
            self._update_prefs()

    def _update_prefs(self):
        if self.get_pref(VERSION_PREF_NAME) == 1:
            self._update_prefs_1_to_2()
            self.set_pref(VERSION_PREF_NAME, 2)
        if self.get_pref(VERSION_PREF_NAME) == 2:
            self._update_prefs_2_to_3()
            self.set_pref(VERSION_PREF_NAME, 3)
        if self.get_pref(VERSION_PREF_NAME) == 3:
            self._update_prefs_3_to_4()
            self.set_pref(VERSION_PREF_NAME, 4)
        if self.get_pref(VERSION_PREF_NAME) == 4:
            self._update_prefs_4_to_5()
            self.set_pref(VERSION_PREF_NAME, 5)

    def _update_prefs_1_to_2(self):
        # itunes
        svr_name = self.get_pref(ITUNES_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[AP_PATH_KEY] = ITUNES_AP_PATH
        tmp_svr[MI_PATH_KEY] = ITUNES_MI_PATH
        self._save_server_by_name(svr_name, tmp_svr)
        # movies
        svr_name = self.get_pref(MOVIES_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[AP_PATH_KEY] = PLEX_MOVIE_AP_PATH
        tmp_svr[MI_PATH_KEY] = PLEX_MOVIE_MI_PATH
        self._save_server_by_name(svr_name, tmp_svr)
        # tv shows
        svr_name = self.get_pref(TV_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[AP_PATH_KEY] = PLEX_TV_AP_PATH
        tmp_svr[MI_PATH_KEY] = PLEX_TV_MI_PATH
        self._save_server_by_name(svr_name, tmp_svr)

    def _update_prefs_2_to_3(self):
        # itunes
        svr_name = self.get_pref(ITUNES_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[PE_PATH_KEY] = ITUNES_PE_PATH
        self._save_server_by_name(svr_name, tmp_svr)
        # movies
        svr_name = self.get_pref(MOVIES_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[PE_PATH_KEY] = PLEX_MOVIE_PE_PATH
        self._save_server_by_name(svr_name, tmp_svr)
        # tv shows
        svr_name = self.get_pref(TV_SVR_PREF_NAME)
        tmp_svr = self._find_server_by_name(svr_name)
        tmp_svr[PE_PATH_KEY] = PLEX_TV_PE_PATH
        self._save_server_by_name(svr_name, tmp_svr)

    def _update_prefs_3_to_4(self):
        for svr in self.prefs[SERVER_ARRAY_PREF_NAME]:
            if SVR_INFO_KEY not in svr:
                svr[SVR_INFO_KEY] = SVR_INFO_TEXT
                self._save_server_by_name(svr[NAME_KEY], svr)

    def _update_prefs_4_to_5(self):
        pass

    def _find_server_by_property(self, in_prop_name, in_prop_text):
        """Returns the server as the raw dictionary."""
        out_svr = None
        for svr in self.prefs[SERVER_ARRAY_PREF_NAME]:
            if svr[in_prop_name] == in_prop_text:
                out_svr = svr
                break
        return out_svr

    def _save_server_by_property(self, in_prop_name, in_prop_text, in_svr):
        """Takes the server as the raw dictionary."""
        svr_idx = -1
        s_array = self.get_pref(SERVER_ARRAY_PREF_NAME)
        for svr in s_array:
            svr_idx += 1
            if svr[in_prop_name] == in_prop_text:
                s_array[svr_idx] = in_svr
                self.set_pref(SERVER_ARRAY_PREF_NAME, s_array)
                break

    def _find_server_by_name(self, in_name):
        return self._find_server_by_property(NAME_KEY, in_name)

    def _save_server_by_name(self, in_name, in_svr):
        # The explicit passing of the name is required in case the name has
        # been changed in the server info you are saving
        return self._save_server_by_property(NAME_KEY, in_name, in_svr)

    def _find_server_by_local_path(self, in_local_path):
        return self._find_server_by_property(LOCAL_PATH_KEY, in_local_path)

    def _property_for_server(self, in_prop_name, in_svr_name):
        svr = self._find_server_by_name(in_svr_name)
        return svr[in_prop_name]

    def _get_svr_name_by_id(self, in_svr_id):
        out_name = None
        for case in switch(in_svr_id):
            if case(ITUNES_SVR_ID):
                out_name = self.get_pref(ITUNES_SVR_PREF_NAME)
                break
            if case(MOVIE_SVR_ID):
                out_name = self.get_pref(MOVIES_SVR_PREF_NAME)
                break
            if case(TV_SVR_ID):
                out_name = self.get_pref(TV_SVR_PREF_NAME)
                break
        return out_name

    def _get_library_server(self, in_library_id, in_name=None):
        if in_name is None:
            svr_name = self._get_svr_name_by_id(in_library_id)
        else:
            svr_name = in_name
        return self._find_server_by_name(svr_name)

    def get_itunes_server(self, in_name=None):
        """Provides the server info object for the current iTunes library."""
        return PtServerInfo(self._get_library_server(ITUNES_SVR_ID, in_name))

    def get_movie_server(self, in_name=None):
        """Provides the server info object for the current movie library."""
        return PtServerInfo(self._get_library_server(MOVIE_SVR_ID, in_name))

    def get_tv_server(self, in_name=None):
        """Provides the server info object for the current TV show library."""
        return PtServerInfo(self._get_library_server(TV_SVR_ID, in_name))

    def get_server_by_local_filepath(self, in_local_path):
        """Finds server whose local path is the stem for a file path."""
        for svr in self.prefs[SERVER_ARRAY_PREF_NAME]:
            if svr[LOCAL_PATH_KEY] in in_local_path:
                return PtServerInfo(svr)
        # fall through if no match found
        return None

    def get_iso_lang_code(self):
        """Find the default ISO lang code for subtitles."""
        return self.get_pref(SUB_LANG_CODE_PREF_NAME)

    def set_iso_lang_code(self, in_code):
        """Set the default ISO lang code for subtitles."""
        lc_code = in_code.lower()
        if (lc_code in ISO_639_1_CODES) or (lc_code in ISO_639_2B_CODES):
            self.set_pref(SUB_LANG_CODE_PREF_NAME, lc_code)

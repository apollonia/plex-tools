"""
Utility routines.
"""

# standard library imports
import sys
import os
import argparse
import pwd
import subprocess
import pipes
import paramiko

# package specific imports
import __main__ as main


# Atributes -------------------------------------------------------------------
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2010 Alan Staniforth"
__license__ = "New-style BSD"
# same format as sys.version_info: "A tuple containing the five components of
# the version number: major, minor, micro, releaselevel, and serial. All
# values except releaselevel are integers; the release level is 'alpha',
# 'beta', 'candidate', or 'final'. The version_info value corresponding to the
# Python version 2.0 is (2, 0, 0, 'final', 0)."  Additionally we use a
# releaselevel of 'dev' for unreleased under-development code.
VERSION_INFO = (1, 0, 0, 'dev', 1)
__version__ = '1.0.0a1'


# Interface -------------------------------------------------------------------
__all__ = [  # Constants ###
            'ISO_639_1_CODES', 'ISO_639_2B_CODES',

            # Classes ###
            'PlainHelpFormatter', 'switch', 'CommandAction',

            # Functions ###
            'ensure_dir', 'exec_cli', 'extend_tuple', 'get_script_name',
            'get_cur_username', 'getversionstr', 'is_root',
            'run_applescript', 'remove_leading_zeroes', 'string_to_number',
            'same_partition'
            ]


# Constants -------------------------------------------------------------------

# Taken from <https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes>
ISO_639_1_CODES = ['aa', 'abk', 'ae', 'af', 'ak', 'am', 'an', 'ar', 'as', 'av',
                   'ay', 'az', 'ba', 'be', 'bg', 'bh', 'bi', 'bm', 'bn', 'bo',
                   'br', 'bs', 'ca', 'ce', 'ch', 'co', 'cr', 'cs', 'cu', 'cv',
                   'cy', 'da', 'de', 'dv', 'dz', 'ee', 'el', 'en', 'eo', 'es',
                   'et', 'eu', 'fa', 'ff', 'fi', 'fj', 'fo', 'fr', 'fy', 'ga',
                   'gd', 'gl', 'gn', 'gu', 'gv', 'ha', 'he', 'hi', 'ho', 'hr',
                   'ht', 'hu', 'hy', 'hz', 'ia', 'id', 'ie', 'ig', 'ii', 'ik',
                   'io', 'is', 'it', 'iu', 'ja', 'jv', 'ka', 'kg', 'ki', 'kj',
                   'kk', 'kl', 'km', 'kn', 'ko', 'kr', 'ks', 'ku', 'kv', 'kw',
                   'ky', 'la', 'lb', 'lg', 'li', 'ln', 'lo', 'lt', 'lu', 'lv',
                   'mg', 'mh', 'mi', 'mk', 'ml', 'mn', 'mr', 'ms', 'mt', 'my',
                   'na', 'nb', 'nd', 'ne', 'ng', 'nl', 'nn', 'no', 'nr', 'nv',
                   'ny', 'oc', 'oj', 'om', 'or', 'os', 'pa', 'pi', 'pl', 'ps',
                   'pt', 'qu', 'rm', 'rn', 'ro', 'ru', 'rw', 'sa', 'sc', 'sd',
                   'se', 'sg', 'si', 'sk', 'sl', 'sm', 'sn', 'so', 'sq', 'sr',
                   'ss', 'st', 'su', 'sv', 'sw', 'ta', 'te', 'tg', 'th', 'ti',
                   'tk', 'tl', 'tn', 'to', 'tr', 'ts', 'tt', 'tw', 'ty', 'ug',
                   'uk', 'ur', 'uz', 've', 'vi', 'vo', 'wa', 'wo', 'xh', 'yi',
                   'yo', 'za', 'zh', 'zu']

# Taken from <https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes>
ISO_639_2B_CODES = ['aar', 'abk', 'ace', 'ach', 'ada', 'ady', 'afa', 'afh',
                    'afr', 'ain', 'aka', 'akk', 'alb', 'ale', 'alg', 'alt',
                    'amh', 'ang', 'anp', 'apa', 'ara', 'arc', 'arg', 'arm',
                    'arn', 'arp', 'art', 'arw', 'asm', 'ast', 'ath', 'aus',
                    'ava', 'ave', 'awa', 'aym', 'aze', 'bad', 'bai', 'bak',
                    'bal', 'bam', 'ban', 'baq', 'bas', 'bat', 'bej', 'bel',
                    'bem', 'ben', 'ber', 'bho', 'bih', 'bik', 'bin', 'bis',
                    'bla', 'bnt', 'bos', 'bra', 'bre', 'btk', 'bua', 'bug',
                    'bul', 'bur', 'byn', 'cad', 'cai', 'car', 'cat', 'cau',
                    'ceb', 'cel', 'cha', 'chb', 'che', 'chg', 'chi', 'chk',
                    'chm', 'chn', 'cho', 'chp', 'chr', 'chu', 'chv', 'chy',
                    'cmc', 'cnr', 'cop', 'cor', 'cos', 'cpe', 'cpf', 'cpp',
                    'cre', 'crh', 'crp', 'csb', 'cus', 'cze', 'dak', 'dan',
                    'dar', 'day', 'del', 'den', 'dgr', 'din', 'div', 'doi',
                    'dra', 'dsb', 'dua', 'dum', 'dut', 'dyu', 'dzo', 'efi',
                    'egy', 'eka', 'elx', 'eng', 'enm', 'epo', 'est', 'ewe',
                    'ewo', 'fan', 'fao', 'fat', 'fij', 'fil', 'fin', 'fiu',
                    'fon', 'fre', 'frm', 'fro', 'frr', 'frs', 'fry', 'ful',
                    'fur', 'gaa', 'gay', 'gba', 'gem', 'geo', 'ger', 'gez',
                    'gil', 'gla', 'gle', 'glg', 'glv', 'gmh', 'goh', 'gon',
                    'gor', 'got', 'grb', 'grc', 'gre', 'grn', 'gsw', 'guj',
                    'gwi', 'hai', 'hat', 'hau', 'haw', 'heb', 'her', 'hil',
                    'him', 'hin', 'hit', 'hmn', 'hmo', 'hrv', 'hsb', 'hun',
                    'hup', 'iba', 'ibo', 'ice', 'ido', 'iii', 'ijo', 'iku',
                    'ile', 'ilo', 'ina', 'inc', 'ind', 'ine', 'inh', 'ipk',
                    'ira', 'iro', 'ita', 'jav', 'jbo', 'jpn', 'jpr', 'jrb',
                    'kaa', 'kab', 'kac', 'kal', 'kam', 'kan', 'kar', 'kas',
                    'kau', 'kaw', 'kaz', 'kbd', 'kha', 'khi', 'khm', 'kho',
                    'kik', 'kin', 'kir', 'kmb', 'kok', 'kom', 'kon', 'kor',
                    'kos', 'kpe', 'krc', 'krl', 'kro', 'kru', 'kua', 'kum',
                    'kur', 'kut', 'lad', 'lah', 'lam', 'lao', 'lat', 'lav',
                    'lez', 'lim', 'lin', 'lit', 'lol', 'loz', 'ltz', 'lua',
                    'lub', 'lug', 'lui', 'lun', 'luo', 'lus', 'mac', 'mad',
                    'mag', 'mah', 'mai', 'mak', 'mal', 'man', 'mao', 'map',
                    'mar', 'mas', 'may', 'mdf', 'mdr', 'men', 'mga', 'mic',
                    'min', 'mis', 'mkh', 'mlg', 'mlt', 'mnc', 'mni', 'mno',
                    'moh', 'mon', 'mos', 'mul', 'mun', 'mus', 'mwl', 'mwr',
                    'myn', 'myv', 'nah', 'nai', 'nap', 'nau', 'nav', 'nbl',
                    'nde', 'ndo', 'nds', 'nep', 'new', 'nia', 'nic', 'niu',
                    'nno', 'nob', 'nog', 'non', 'nor', 'nqo', 'nso', 'nub',
                    'nwc', 'nya', 'nym', 'nyn', 'nyo', 'nzi', 'oci', 'oji',
                    'ori', 'orm', 'osa', 'oss', 'ota', 'oto', 'paa', 'pag',
                    'pal', 'pam', 'pan', 'pap', 'pau', 'peo', 'per', 'phi',
                    'phn', 'pli', 'pol', 'pon', 'por', 'pra', 'pro', 'pus',
                    'que', 'raj', 'rap', 'rar', 'roa', 'roh', 'rom', 'rum',
                    'run', 'rup', 'rus', 'sad', 'sag', 'sah', 'sai', 'sal',
                    'sam', 'san', 'sas', 'sat', 'scn', 'sco', 'sel', 'sem',
                    'sga', 'sgn', 'shn', 'sid', 'sin', 'sio', 'sit', 'sla',
                    'slo', 'slv', 'sma', 'sme', 'smi', 'smj', 'smn', 'smo',
                    'sms', 'sna', 'snd', 'snk', 'sog', 'som', 'son', 'sot',
                    'spa', 'srd', 'srn', 'srp', 'srr', 'ssa', 'ssw', 'suk',
                    'sun', 'sus', 'sux', 'swa', 'swe', 'syc', 'syr', 'tah',
                    'tai', 'tam', 'tat', 'tel', 'tem', 'ter', 'tet', 'tgk',
                    'tgl', 'tha', 'tib', 'tig', 'tir', 'tiv', 'tkl', 'tlh',
                    'tli', 'tmh', 'tog', 'ton', 'tpi', 'tsi', 'tsn', 'tso',
                    'tuk', 'tum', 'tup', 'tur', 'tut', 'tvl', 'twi', 'tyv',
                    'udm', 'uga', 'uig', 'ukr', 'umb', 'und', 'urd', 'uzb',
                    'vai', 'ven', 'vie', 'vol', 'vot', 'wak', 'wal', 'war',
                    'was', 'wel', 'wen', 'wln', 'wol', 'xal', 'xho', 'yao',
                    'yap', 'yid', 'yor', 'ypk', 'zap', 'zbl', 'zen', 'zgh',
                    'zha', 'znd', 'zul', 'zun', 'zxx', 'zza']


# Classes ---------------------------------------------------------------------

class Borg:
    """
    Class to implement shared state between object instances.

    Derive classes whose instances you wish to share state from Borg:

    class MySingleton(Borg):

    and call Borg.__init__(self) very early in your class's __init__()

    See:
        <http://stackoverflow.com/a/1363867>
        <http://stackoverflow.com/a/1363857>
        <http://www.aleax.it/5ep.html>
    for (much) more information and use examples.

    """
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state


class PlainHelpFormatter(argparse.RawDescriptionHelpFormatter):
    """
    Class to improve formatting of optparse help output.

    Helper class for op, tell your OptionParser to use this to format
    the help text when first instantiate the object. Thus:

    parser = OptionParser(..., formatter=ptl.PlainHelpFormatter(), ...)
    """
    def format_description(self, description):
        if description:
            return description + "\n"
        else:
            return ""


class switch(object):
    """
    Class to implement switch statement in Python.

    From <http://code.activestate.com/recipes/410692/>

    Simple example:

        v = 'ten'
        for case in switch(v):
            if case('one'):
                print 1
                break
            if case('two'):
                print 2
                break
            if case('ten'):
                print 10
                break
            if case('eleven'):
                print 11
                break
            if case(): # default, could also just omit condition or 'if True'
                print "something else!"
                # No need to break here, it'll stop anyway
    """
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration

    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args:  # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False


class DataHolder:
    """
    Class to implement assignment in an if statement.

    Use thus:

        import re
        input = u'test bar 123'
        save_match = DataHolder(attr_name='match')
        if save_match(re.search('foo (\d+)', input)):
            print "Foo"
            print save_match.match.group(1)
        elif save_match(re.search('bar (\d+)', input)):
            print "Bar"
            print save_match.match.group(1)
        elif save_match(re.search('baz (\d+)', input)):
            print "Baz"
            print save_match.match.group(1)

    From Craig McQueen on Stack Overflow <http://stackoverflow.com/a/1806338>

    """
    def __init__(self, value=None, attr_name='value'):
        self._attr_name = attr_name
        self.set(value)

    def __call__(self, value):
        return self.set(value)

    def set(self, value):
        setattr(self, self._attr_name, value)
        return value

    def get(self):
        return getattr(self, self._attr_name)


class CommandAction(argparse.Action):
    """
    Place a positional argument anywhere in the command line.

    Based on a clever idea from Stack Overflow:

    <http://stackoverflow.com/a/5374229>

    """
    def __call__(self, parser, namespace, values, option_string=None):

        setattr(namespace, 'command', values[0])


# Functions -------------------------------------------------------------------

def getversionstr(in_version_info, strict=True):
    """
    Returns the library version number as a string.

    By default the function returns a distutils strict version string.
    In that case if the 'release level' is 'final' this is discarded
    and in the case of 'alpha', 'beta', 'candidate' and 'dev' it is
    converted into the nearest of 'a' or 'b'.

    Params:
        in_version_info: A 3-5 item tuple following the sys.version_info
                         pattern
        strict: if False then a string that can be compared with
                distutils.version.LooseVersion() will be returned.
    """
    vstring = '.'.join([
        str(in_version_info[0]),
        str(in_version_info[1]),
        str(in_version_info[2])
        ])
    if in_version_info[3] != "":
        if strict:
            if in_version_info[3] in ['alpha', 'dev']:
                vstring = "".join([vstring, 'a', str(in_version_info[4])])
            if in_version_info[3] in ['beta', 'candidate']:
                vstring = "".join([vstring, 'b', str(in_version_info[4])])
        else:
            vstring = "".join([vstring, in_version_info[3],
                              str(in_version_info[4])])
    return vstring


def remove_leading_zeroes(in_str):
    """
    Remove leading zeroes from the string representation of a number.

    Does not work for complex numbers.

    Params:
        in_str - the number to convert as a string

    Returns:
        the number as a  string without leading zeroes
    """
    return str(string_to_number(in_str))


def string_to_number(in_str, in_imaginary=None):
    """
    Convert the string representation of a number to a number.

    Returns it as the appropriate class for the passed in number - int, float
    complex.

    Thanks to Javier <http://stackoverflow.com/a/379966> for this method.

    Params:
        in_str - the number to convert as a string

    Returns:
        the number as a number object
    """
    try:
        return int(in_str)
    except ValueError:
        try:
            return float(in_str)
        except:
            if in_imaginary is None:
                return complex(in_str)
            else:
                return complex(in_str, in_imaginary)


def merge_dicts(*dict_args):
    """
    Merge two or more dicts.

    Copy to final dictionary is shallow, precedence goes to key:value pairs in
    later dicts.

    Thanks to Aaron Hall <http://stackoverflow.com/a/26853961> for this
    method that is portable across Python 2 and 3. Aaron also gives the
    way to do this if you can guarantee Python 3.5: z = (**x, **y).

    Params:
        *dict_args -
        debug - if True output debugging info

    Returns:
        The merged dictionaries with later key:value pair having precedence
            over earlier
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def extend_tuple(in_tuple, deep, value_only, *new):
    """
    Add item(s) to a tuple.

    Params:
        in_tuple:   The tuple to be extended

        deep:       If the item(s) in new is an iterable item (list, tuple
                    dict) then add the contents as individual items of the
                    new tuple. The method does not regard strings as iterable.

                    This does not cause the function to recurse into contained
                    container objects, ie if the list [1, [2, 3, 4], 5] is
                    passed, the items added will be 1, [2, 3, 4] and 5, not
                    1, 2, 3, 4, 5.

        value_only: If deep is true and the object being iterated is a dict
                    then by default the items will be added as tuples of each
                    key/value pair. If this is True then the bare value only
                    will be added.

        new:        The item (or items) to add.

    Returns:
        A tuple with the new item(s)

    """
    new_tuple_list = []
    for i in in_tuple:
        new_tuple_list.append(i)
    for arg in new:
        if deep:
            if hasattr(arg, '__iter__'):
                # special case for dicts
                if type(arg) is dict:
                    for i in arg:
                        if value_only:
                            new_tuple_list.append(arg[i])
                        else:
                            new_tuple_list.append((i, arg[i]))
                else:
                    for i in arg:
                        new_tuple_list.append(i)
            else:
                new_tuple_list.append(arg)
        else:
            new_tuple_list.append(arg)
    return tuple(new_tuple_list)


def ensure_dir(in_file_path, fake_it=False):
    """
    Ensures the directory holding the file passed in exists.

    Will create it and and any required enclosing directory if necessary.

    Params:
        f: the file path whose directory is to be created if not present
        fake_it: if true don't create dir, just print a log line

    Returns:
        If nothing is done: False
        If a directory is created: True
    """
    d = os.path.dirname(os.path.abspath(in_file_path))
    dir_created = False
    if not os.path.exists(d):
        dir_created = True
        # print "Creating missing '" + d + "' directory"
        if not fake_it:
            # only actually create dirs if not testing
            try:
                os.makedirs(d, 0755)
            except:
                dir_created = False
    return dir_created


def split_path(in_path, debug=False):
    """
    Platform independant method to split a file path into its components.

    The function does no path expansion or conversion. If you pass a partial
    path the returned array will only conjtain the parts of the path in that
    partial path and if (on windows) there is no drive specifier no drive
    letter will be returned.

    Params:
        in_path - the path to parse as a string
        debug - if True output debugging info

    Returns a tuple whose parts are:
        [0] - the drive letter if (a) on Windows and (b) if present in the
              path, otherwise ''
        [1] - an array, each element a path component
    """
    drive, path = os.path.splitdrive(in_path)
    parts = []
    while True:
        newpath, tail = os.path.split(path)
        if debug:
            print repr(path), (newpath, tail)
        if newpath == path:
            assert not tail
            if path:
                parts.append(path)
            break
        parts.append(tail)
        path = newpath
    parts.reverse()
    return (drive, parts)


def get_script_name():
    """Get the stripped name of the script file that called the function."""
    s_name = os.path.basename(main.__file__)
    return s_name


def same_partition(f1, f2):
    """
    Test to see if f1 and f2 are on the same partition.

    From StackOverflow <http://stackoverflow.com/q/249775>

    Params:
        f1:   file or directory path
        f2:   file or directory path
    """
    out_truth = False
    try:
        out_truth = os.stat(f1).st_dev == os.stat(f2).st_dev
    except OSError:
        out_truth = False
    except:
        print "Unexpected error:", sys.exc_info()[0]
    return out_truth


def which(program):
    """
    Cross platform implementation of unix 'which' tool.

    Params:
        program:   string with the name of the executable to search for

    Returns: the path to the executable if it exists
             otherwise None

    Found on Stack Overflow <http://stackoverflow.com/q/377017>
    """

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None


def is_root():
    """Check if running as root."""
    cur_user_name = get_cur_username()
    return cur_user_name == 'root'


def get_cur_username():
    """Get the current user name."""
    return(pwd.getpwuid(os.getuid())[0])


def cmd_line_from_list(in_list):
    """
    Build a command line from a list of strings.

    Params:
        in_list:    A list of strings, each representing one item of a POSIX
        command line.

    Returns:
        The list items combined into a single string correctly quoted so it
        could be pasted into a terminal or executed via os.system() or
        paramiko.
   """
    out_string = ' '.join(pipes.quote(p) for p in in_list)
    return out_string


def exec_cli(command_list, fake_it=False, host=None, user=None, echo=False):
    """
    Construct and execute a command line, either locally or on a remote host.

    Params:
        command_list:   list of strings to be passed to subprocess.check_call()

        fake_it:        if true don't create dir, just print a log line

        host:           the remote host on which to execute the command

        user:           the user on the remote host

        echo:           print the command output

    If command is to executed on a remote host paramiko will be used and the
    assumption made that an SSH key is loaded in ssh_agent that will allow
    the paramiko ssh client to log in without a password.

    Returns:
        A tuple: (raw_output, raw_err, ret_code)
            raw_output: output in response to the command - or None
            raw_err:    std error output in response to the command - or None
            ret_code:   command return code - or None
    """
    raw_output = raw_err = ret_code = None
    if not fake_it:
        try:
            if host is None:
                my_proc = subprocess.Popen(command_list,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
                (raw_output, raw_err) = my_proc.communicate()
                ret_code = my_proc.returncode
            else:
                # paramiko.util.log_to_file('ssh.log') # sets up logging
                BUF_SIZE = -1
                cmd = cmd_line_from_list(command_list)
                cmd = cmd.encode('utf-8')
                client = paramiko.SSHClient()
                client.load_system_host_keys()
                client.connect(host, username=user)
                chan = client.get_transport().open_session()
                chan.exec_command(cmd)
                raw_output = ''.join(chan.makefile('rb', BUF_SIZE))
                raw_err = ''.join(chan.makefile_stderr('rb', BUF_SIZE))
                ret_code = chan.recv_exit_status()
                client.close()
        except subprocess.CalledProcessError, e:
            if raw_err is None:
                raw_err = ""
            print "Command '" + " ".join(command_list) + \
                  "' failed, error: " + str(e.returncode) + ': ' + raw_err
        except (paramiko.SSHException, paramiko.PasswordRequiredException,
                paramiko.BadAuthenticationType, paramiko.ChannelException,
                paramiko.BadHostKeyException, paramiko.AuthenticationException,
                paramiko.ProxyCommandFailure) as e:
            if raw_err is None:
                raw_err = ""
            print "Paramiko command '" + " ".join(command_list) + \
                  "' failed, error: " + e.message + ': ' + raw_err
            if client is not None:
                client.close()
        except:
            if raw_err is None:
                raw_err = ""
            print "Unexpected error:", sys.exc_info()[0], ': ', raw_err
    else:
        print " ".join(command_list)
    if echo:
        if (raw_output is not None) and (raw_output is not ""):
            print raw_output
        if (raw_err is not None) and (raw_err is not ""):
            print raw_err
    return (raw_output, raw_err, ret_code)


def run_applescript(script, fake_it=False):
    """
    Runs an Applescript.

    Params:
        script: a list, each item of which is a line of the Applescript

        fake_it: if true don't create dir, just print a log line
    """
    osascript_command = ['osascript']  # Initialise the osascript command line
    for script_line in script:
        # Add each line of the script to the osascript command line
        osascript_command.append('-e')
        osascript_command.append(script_line)
    if not fake_it:
        try:
            subprocess.check_call(osascript_command)
        except subprocess.CalledProcessError, e:
            print "Command '" + " ".join(osascript_command) + \
                  "' failed, error: " + str(e.returncode)
            pass
        except:
            print "Unexpected error:", sys.exc_info()[0]
            pass
    else:
        print " ".join(osascript_command)

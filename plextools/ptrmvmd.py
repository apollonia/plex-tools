#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Remove nuisance metadata from a video file.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
from plextools import colors
from plextools import ptl
from ptl import pt_print, pt_print_err
from util import switch, ensure_dir, exec_cli, get_script_name, is_root, \
                 DataHolder


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.getversionstr(ptl.VERSION_INFO)

def process_dir(dir_path, args):
    """Tests if dir_path is a series or season dir and calls correct handler"""
    full_dir_path = os.path.abspath(dir_path)
    os.path.walk(dir_path, process_dir_files, args)

def process_dir_files(args, dir_path, dir_list):
    """For each file in dir_list with extracted md, call restore function."""
    found_files = []
    for f in dir_list:
        cur_file = os.path.join(dir_path, f)
        if ptl.check_for_nuisance_metadata(cur_file):
            found_files.append(cur_file)
    if len(found_files) != 0:
        for ff in found_files:
            process_file(ff, args)

def process_file(in_file_path, args):
    """Remove the metadata from the indicated file."""
    cur_file = os.path.abspath(in_file_path)
    pt_print("".join(["Removing metadata from: '", colors.BOLD, 
             os.path.basename(cur_file), colors.END, "'"]), inset=1)
    if ((not ptl.saved_metadata_exists(in_file_path)) or \
       (not ptl.extracted_cover_art_exists(in_file_path))) and \
       (not args.force):
        pt_print(colors.FG_RED +  "Error:" + colors.END + " no saved metadata "
                 "file exists for the file '" + colors.BOLD + 
                 in_file_path + colors.END + "'.", inset=1)
    else:
        ptl.remove_nuisance_metadata_from_file(in_file_path)

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptcp() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools restore metadata module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptrstmd(args)
    
    # exit cleanly
    sys.exit(0)

def ptrmvmd(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    
    ptl.print_tool_desc('Remove metadata tool')
    
    ptl.tool_main('ptrmvmd', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

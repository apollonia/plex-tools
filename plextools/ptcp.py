#! /usr/bin/env python
"""
Script to copy video files into the Plex TV Shows library.

Assumes the library is on the on Roadhouse server. Intended to be used 
from a client with the 'Media Store' volume mounted.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
import colors
import ptl
import ptprefs
from ptprefs import PtPrefs
from ptl import pt_print, pt_print_err, ensure_nomedia
from util import switch, ensure_dir, exec_cli, get_script_name, \
                 is_root


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()


def process_file(file_path, args):
    """Loads the FileInfo structure and passes it to the relevant function"""
    try:
        pt_print("".join(["Analysing '", colors.BOLD, 
                 os.path.basename(file_path), colors.END, "'"]), inset=1)
        file_info = ptl.FileInfo(file_path, args)
        if file_info.validate():
            if file_info.name_type in ptl.TV_TYPES:
                copy_tv_file_to_plex(file_info, args)
            elif file_info.name_type in ptl.MOVIE_TYPES:
                copy_movie_file_to_plex(file_info, args)
        else:
            pt_print_err("Insufficient information to create a Plex name for "
                         "file '" + colors.BOLD + os.path.basename(file_path) +
                         colors.END + "'.")
    except ptl.PTError, e:
        pt_print_err(e.value)

def copy_tv_file_to_plex(file_info, args):
    """Copies the given TV series file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    tv_svr = PtPrefs().get_tv_server(args.alt_server)
    local_dest_full_path = os.path.join(tv_svr.local_path(), 
                                        file_info.show_name, 
                                        "Season " + file_info.season_num, 
                                        new_file_name)
    copy_file_to_plex(file_info, local_dest_full_path, args)

def copy_movie_file_to_plex(file_info, args):
    """Copies the given movie file into the Plex Library"""
    new_file_name = file_info.plex_file_name()
    movie_dir_name = file_info.movie_dir_name()
    movie_svr = PtPrefs().get_movie_server(args.alt_server)
    local_dest_full_path = os.path.join(movie_svr.local_path(), 
                                        movie_dir_name, new_file_name)
    copy_file_to_plex(file_info,local_dest_full_path, args)

def copy_file_to_plex(file_info, local_dest_full_path, args):
    """
    Copies the file into the Plex Library at the given path
    
    If it is a file type that contains mpeg-4 metadata then the function 
    also deletes those atoms from the file that may confuse the Plex 
    server by over-riding the video data implicit in the file name. This is 
    necessary because sometimes that atomic data is wrong, or truncated from 
    what it should be.
    
    """
    pt_print("Copying file '" + colors.BOLD + file_info.file_name + 
             colors.END + "' to Plex library", inset=1)
    if os.path.exists(local_dest_full_path) and not args.force:
        pt_print(colors.BOLD +  "Error:" + colors.END + " '" + colors.BOLD + 
                 local_dest_full_path + colors.END + "' already exists in "
                 "the Plex library.", 
                 inset=2)
    else:
        if ensure_dir(local_dest_full_path, ptl.fake_file_ops):
            pt_print("Creating missing '" + 
                     os.path.dirname(local_dest_full_path) + 
                     "' directory", inset=1)
        if (file_info.name_type == ptl.TYPE_PLEX_EXTRA['id']):
            # an extra, ensure '.nomedia file is present
            ensure_nomedia(local_dest_full_path)
        new_file_name = os.path.basename(local_dest_full_path)
        # set up ivalues for exec_cli
        src_file_path = file_info.full_file_path
        dst_file_path = local_dest_full_path
        cli_host = None
        cli_user = None
        if ptl.are_paths_on_same_server(src_file_path, dst_file_path):
            src_svr = PtPrefs().get_server_by_local_filepath(src_file_path)
            dst_svr = PtPrefs().get_server_by_local_filepath(dst_file_path)
            if (src_svr is not None) and (dst_svr is not None):
                if (src_svr.remote_path() is not None) and \
                        (dst_svr.remote_path() is not None):
                    src_file_path = src_file_path.replace(
                        src_svr.local_path(), src_svr.remote_path())
                    dst_file_path = dst_file_path.replace(
                        dst_svr.local_path(), dst_svr.remote_path())
                    cli_host = src_svr.host()
                    cli_user = src_svr.user()
        cli_list = ["cp", src_file_path, dst_file_path]
        pt_print("Copying as '" + colors.BOLD + new_file_name + colors.END + 
                 "'...", inset=1)
        exec_cli(cli_list, ptl.fake_file_ops)
        pt_print("Copy complete.", inset=1)
        ptl.add_matching_local_media(src_file_path, dst_file_path, 'cp', None, 
                              cli_host, cli_user)
        if args.clean_md:
            if ptl.check_for_nuisance_metadata(local_dest_full_path):
                ptl.remove_nuisance_metadata_from_file(local_dest_full_path)

def process_dir(dir_path, args):
    print >> sys.stderr, \
    get_script_name() + ": Error: The path '" + dir_path + "' is a directory "\
    "not a file. I do not (yet) handle directories"

def show_file_name_type_help():
    help_text = """
    To generate the name the file will need to recognised by the Plex Media 
    Server """ + get_script_name() + """ needs information about the show or 
    movie it contains. The script can extract some of the information needed 
    from the original file name but to do that it needs an idea of the form 
    of the name. The script uses heuristics to help it determine the file 
    name form but occasionally the name form is not possible to determine and 
    in that case you need to pass an indication to the script as to the type 
    of file being processed.
    
    If the file is a TV Show pass -t 'tv', if a movie pass -t 'movie'. The 
    script will try to extract information from metadata within the file if 
    available and if it is not will prompt you for the required information. 
    You may also pass the required information as command line options 
    which overrides any information extracted frokm the filename or 
    metadata.
    
    If you are *really* sure of the source of a file you can pass a token 
    specifying the name form and source. In most cases this is useless as 
    if it is a well formed name the script would identify it heuristically 
    and extract info from it. If it is not well formed then the script's 
    code to extract information will probably fail so knowing the origin 
    is not much use.
    
    The exception to this rule is if it is an EyeTV exported file or one 
    from an iTunes library. In that case it will probably have enough 
    metadata within the file structure to not need information extracted from 
    the name. In that case you might pass one of the following tokens as the 
    parameter to the -t/--type option:
    
    'eyetvtv' - EyeTV exported TV show, name usually has the form:
        '<Series name> - <episode num>-<total episodes>, <Title>.<ext>'
        
    'eyetvmovie1' - EyeTV exported movie, name usually has the form:
        '<Movie Title> - <movie year>, Movie.<ext>'
        
    'eyetvmovie2' - EyeTV exported unrecognised movie, name usually has form:
        '<Movie Title> - <movie year>, Other.<ext>'
        
    'ittv' - iTunes library TV show, name usually has the form:
        '<nn> <Episode name>.<ext>'
        
    'itmovie' - iTunes library movie, name usually has the form:
        '[<nn> ]<Movie Title>.<ext>'
    
    You could also pass one of the following tokens, but it is rarely 
    useful because, as explained above, if the name is not recognised then 
    it is unlikely that information will be esuccessfully extracted from the 
    name and these file types rarely if ever contain useful metadata.
    
    'iplayer1' - get_iplayer type 1 - TV show, name usually has the form:
        '<Series Name>: Series <num> - <Episode info> <a1b2c3d4> default.<ext>
        
    'iplayer2' - get_iplayer type 2 - TV show, name usually has the form:
        '<Series Name> - <Season info>: <Episode info> <a1b2c3d4> default.<ext>
        
    'iplayer3' - get_iplayer type 3 - TV show, name usually has the form:
        '<Series name> - <Episode Title> <a1b2c3d4> default.<ext>'
        
    'iplayer4' - get_iplayer type 4 = movie, name usually has the form:
        '<Movie name> - <Movie name> <a1b2c3d4> default.<ext>'
        
    'mkvtv' - MakeMKV DVD or Blu-ray rip, name usually has the form:
        '<Series_name>_[-_]Season_<num>_[-_]Disc_<num>_t<nn>.<ext>'
    
    """
    print help_text

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptcp() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools copy module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptcp(args)
    
    # exit cleanly
    sys.exit(0)

def ptcp(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    
    ptl.print_tool_desc('Copy tool')
    
    ptl.tool_main('ptcp', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Restore metadata to a file that another Plex Tools script has 
extracted and saved locally.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
from plextools import colors
from plextools import ptl
from ptl import pt_print, pt_print_err
from util import switch, ensure_dir, exec_cli, get_script_name, is_root, \
                 DataHolder


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.getversionstr(ptl.VERSION_INFO)

def process_dir(dir_path, args):
    """Tests if dir_path is a series or season dir and calls correct handler"""
    full_dir_path = os.path.abspath(dir_path)
    fake_args = None
    os.path.walk(dir_path, process_dir_files, fake_args)

def process_dir_files(args, dir_path, dir_list):
    """For each file in dir_list with extracted md, call restore function."""
    found_files = []
    for f in dir_list:
        cur_file = os.path.join(dir_path, f)
        if ptl.saved_metadata_exists(cur_file):
            found_files.append(cur_file)
        elif ptl.extracted_cover_art_exists(cur_file) != None:
            found_files.append(cur_file)
    if len(found_files) != 0:
        for ff in found_files:
            process_file(ff, args)

def process_file(in_file_path, args):
    cur_file = os.path.abspath(in_file_path)
    pt_print("".join(["Restoring metadata to: '", colors.BOLD, 
             os.path.basename(cur_file), colors.END, "'"]), inset=1)
    for case in switch(ptl.get_video_container_format(cur_file)):
        if case(ptl.MP4_CONTAINER):
            restore_mp4_metadata(cur_file)
            break
        if case(ptl.MKV_CONTAINER):
            restore_mkv_metadata(cur_file)
            break

def restore_mp4_metadata(in_file_path):
    """
    Restores extracted metadata to an MPEG-4 container file.
    
    I use the ptl.NUISANCE_METADATA_ATOMS array to force the order in which 
    the atoms are re-written to the file. This is so media-info gives 
    consistent results. I have found that mediainfo will fill in the 
    <Movie_name> tag of its XML output with either the '©nam' atom contents 
    or 'tvsh' atom contents, choosing whichever comes second. As 'tvsh' 
    will only be present in a TV show file I put that second so when present 
    it is used and, in the event the file is a movie it will be absent and 
    '©nam' will be used.
    
    """
    restore_cl = ['AtomicParsley', in_file_path, '--overWrite']
    saved_md = ptl.get_saved_metadata(in_file_path)
    if saved_md is not None:
        for atom in ptl.NUISANCE_METADATA_ATOMS:
            if atom in saved_md:
                if (atom == ptl.EYETV_XML_LABEL) or \
                   (atom == ptl.EYETV_RATING_LABEL):
                    restore_cl.append('--rDNSatom')
                    restore_cl.append(saved_md[atom])
                    if (atom == ptl.EYETV_XML_LABEL):
                        restore_cl.append('name=iTunMOVI')
                    else:
                        restore_cl.append('name=iTunEXTC')
                    restore_cl.append('domain=com.apple.iTunes')
                else:
                    # Select the option flag to add
                    miss_flag = len(restore_cl)
                    if atom == ptl.NAME_ID['atom']:
                        restore_cl.append('--title')
                    elif atom == ptl.SERIES_NAME_ID['atom']:
                        restore_cl.append('--TVShowName')
                    elif atom == ptl.YEAR_ID['atom']:
                        restore_cl.append('--year')
                    elif atom == ptl.DESC_ID['atom']:
                        restore_cl.append('--description')
                    elif atom == ptl.LONG_DESC_ID['atom']:
                        restore_cl.append('--longdesc')
                    elif atom == ptl.ARTIST_ID['atom']:
                        restore_cl.append('--artist')
                    elif atom == ptl.GENRE_ID['atom']:
                        restore_cl.append('--genre')
                    elif atom == ptl.ALBUM_ARTIST_ID['atom']:
                        restore_cl.append('--albumArtist')
                    elif atom == ptl.COPYRIGHT_ID['atom']:
                        restore_cl.append('--copyright')
                    # That's the option flag added now append the data
                    if (atom != 'type'):
                        if miss_flag != len(restore_cl):
                            restore_cl.append(saved_md[atom])
                        else:
                            pt_print_err("".join(["No code to restore '",
                                         colors.BOLD, atom, colors.END,
                                         "' metadata. Contact maintainer!"]),
                                         inset=2)
    covers = ptl.extracted_cover_art_exists(in_file_path)
    if covers is not None:
        dir_path = os.path.dirname(in_file_path)
        for cover_name in covers:
            restore_cl.append('--artwork')
            restore_cl.append(os.path.join(dir_path, cover_name))
    if len(restore_cl) > 3:
        ptl.exec_cli(restore_cl, ptl.fake_file_ops)

def restore_mkv_metadata(in_file_path):
    """Restores extracted metadata to an Matroska container file."""
    restore_cl = ['mkvpropedit', in_file_path, '--edit', 'info', '--set']
    saved_md = ptl.get_saved_metadata(in_file_path)
    if saved_md is not None:
        saved_iter = iter(saved_md)
        md_id = DataHolder(attr_name='id')
        while md_id(next(saved_iter, None)) is not None:
            if (md_id.id == ptl.NAME_ID['atom']):
                restore_cl.append(''.join(['title=', saved_md[md_id.id]]))
    if len(restore_cl) > 5:
        ptl.exec_cli(restore_cl, ptl.fake_file_ops)

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptcp() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools restore metadata module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptrstmd(args)
    
    # exit cleanly
    sys.exit(0)

def ptrstmd(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    
    ptl.print_tool_desc('Restore metadata tool')
    
    ptl.tool_main('ptrstmd', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-
"""
Module to maintain and provide access to persistant state.
"""

# standard library imports
import os
import plistlib

# package specific imports
from util import Borg


# Atributes -------------------------------------------------------------------
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# same format as sys.version_info: "A tuple containing the five components of
# the version number: major, minor, micro, releaselevel, and serial. All
# values except releaselevel are integers; the release level is 'alpha',
# 'beta', 'candidate', or 'final'. The version_info value corresponding to the
# Python version 2.0 is (2, 0, 0, 'final', 0)."  Additionally we use a
# releaselevel of 'dev' for unreleased under-development code.
VERSION_INFO = (1, 0, 0, 'dev', 1)
__version__ = '1.0.0a1'


# Interface -------------------------------------------------------------------
__all__ = [
            # Classes ###
            'Prefs', 'PrefsError'
            ]


# Implementation --------------------------------------------------------------

class PrefsError(Exception):
    """
    Exception class for use by Prefs class routines

    Instantiate with an error string
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Prefs(Borg):
    """Access to and maintain persistant state for an application/tool."""
    def __init__(self, in_prefs_file=None):
        Borg.__init__(self)
        if (in_prefs_file is None) and (not ('file_path' in self.__dict__)):
            err = PrefsError('Must specify a prefs file path')
            raise err
        if (in_prefs_file is not None) and \
                (not ('file_path' in self.__dict__)):
            self.file_path = os.path.abspath(in_prefs_file)
        if (os.path.exists(self.file_path)) and \
           (not ('prefs' in self.__dict__)):
            self.prefs = plistlib.readPlist(self.file_path)
        else:
            if not ('prefs' in self.__dict__):
                self.prefs = {}
                self.file_path = os.path.abspath(in_prefs_file)
                plistlib.writePlist(self.prefs, self.file_path)

    def get_pref(self, in_pref_name):
        if in_pref_name in self.prefs:
            return self.prefs[in_pref_name]
        else:
            err = PrefsError("There is no preference called: '" +
                             in_pref_name + "'")
            raise err

    def set_pref(self, in_pref_name, in_pref_value):
        self.prefs[in_pref_name] = in_pref_value
        plistlib.writePlist(self.prefs, self.file_path)

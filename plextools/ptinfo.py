#! /usr/bin/env python
"""
Script to copy video files into the Plex TV Shows library.

Assumes the library is on the on Roadhouse server. Intended to be used 
from a client with the 'Media Store' volume mounted.

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re

# package specific imports
import colors
import ptl
from ptl import pt_print, pt_print_err
from util import switch, ensure_dir, exec_cli, get_script_name, \
                 is_root


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()

def process_dir(dir_path, args):
    """Tests if dir_path is a series or season dir and calls correct handler"""
    full_dir_path = os.path.abspath(dir_path)
    os.path.walk(dir_path, process_dir_files, args)

def process_dir_files(args, dir_path, dir_list):
    """For each file in dir_list with extracted md, call restore function."""
    found_files = []
    for f in dir_list:
        cur_file = os.path.join(dir_path, f)
        if ptl.check_for_nuisance_metadata(cur_file):
            found_files.append(cur_file)
    if len(found_files) != 0:
        for ff in found_files:
            process_file(ff, args)

def process_file(in_file_path, args):
    """Loads the FileInfo structure and passes it to the relevant function"""
    HEADER_COL_PADDING = 1
    FLAG_COL_WIDTH = 2
    LABEL_COL_WIDTH = 20
    
    file_name = os.path.basename(in_file_path)
    full_path = os.path.abspath(in_file_path)
    pt_print("".join(["Analysing '", colors.BOLD, file_name, 
             colors.END, "'"]), inset=1)
    container_type = ptl.get_video_container_format(full_path)
    pt_print("".join(["Video container format: '", colors.BOLD, 
             container_type, colors.END, "'"]), inset=1)
    name_type = ptl.detect_file_name_type(full_path, False)
    type_str = 'Unidentified' if (name_type is  None) else \
        ptl._name_type_description(name_type)
    pt_print("".join(["File name format: '", colors.BOLD, 
             type_str, colors.END, "'"]), inset=1)
    
    md = ptl.get_show_metadata(full_path)
    pt_print("".join([colors.UNDERSCORE, "Embedded metadata:", colors.END]), 
             inset=1)
    md_found = False
    for md_item in md:
        print_inset = 2
        if (md[md_item] is not None) and (md_item != ptl.TYPE_ATOM):
            md_found = True
            if md_item == u'iTunMOVI':
                md_id = u'com.apple.iTunes;iTunMOVI'
            elif md_item == u'iTunEXTC':
                md_id = u'com.apple.iTunes;iTunEXTC'
            else:
                md_id = md_item
            md_flags_str = build_md_flags_str(md_id)
            md_label_str = get_md_label_str(md_id)
            md_label_str = md_label_str.ljust(LABEL_COL_WIDTH)
            md_ld_len = (FLAG_COL_WIDTH + HEADER_COL_PADDING + 
                         len(md_label_str) + HEADER_COL_PADDING + 
                         (ptl.PT_PRINT_INSET_WIDTH * print_inset) + 
                         ptl.PT_PRINT_MARKER_ARROW_WIDTH)
            md_text_str = colors.BOLD + \
                          get_md_text_str(md[md_item], md_ld_len) + \
                          colors.END
            print_str = ' '.join([md_flags_str, md_label_str, md_text_str])
            pt_print(print_str, inset=print_inset)
    if md_found is False:
        pt_print('None found...', inset=print_inset)
    pt_print("".join([colors.UNDERSCORE, "Metadata from filename:", \
             colors.END]), inset=1)
    if name_type is None:
        pt_print('Nothing - ' + colors.BOLD + 
                 'file name type not recognised' + colors.END + '.', inset=1)
    else:
        name_info = ptl.NameInfo(full_path)
        if name_info.name is not None:
            print_str = build_name_info_str('TM', 'Name', LABEL_COL_WIDTH,
                                            name_info.name)
            pt_print(print_str, inset=2)
        if name_info.season_num is not None:
            print_str = build_name_info_str('T-', 'Season number', 
                                            LABEL_COL_WIDTH, 
                                            name_info.season_num)
            pt_print(print_str, inset=2)
        if name_info.episode_num is not None:
            print_str = build_name_info_str('T-', 'Episode number', 
                                            LABEL_COL_WIDTH,
                                            name_info.episode_num)
            pt_print(print_str, inset=2)
        if name_info.episode_range is not None:
            print_str = build_name_info_str('T-', 'Episode range', 
                                            LABEL_COL_WIDTH,
                                            name_info.episode_num + ' - '
                                            + name_info.episode_range)
            pt_print(print_str, inset=2)
        if name_info.label is not None:
            print_str = build_name_info_str('T-', 'Label', LABEL_COL_WIDTH,
                                            name_info.label)
            pt_print(print_str, inset=2)
        if name_info.movie_year is not None:
            print_str = build_name_info_str('TM', 'Release year', 
                                            LABEL_COL_WIDTH,
                                            name_info.movie_year)
            pt_print(print_str, inset=2)

def build_name_info_str(in_flags, in_label, MAX_LABEL_WIDTH, in_text):
    nm_flags_str = in_flags
    nm_label_str = in_label.ljust(MAX_LABEL_WIDTH)
    nm_text_str = colors.BOLD + in_text + colors.END
    return ' '.join([nm_flags_str, nm_label_str, nm_text_str])

def build_md_flags_str(in_atom):
    """
    Create string holding metadata functionality flags
    
    2 chars wide. 
        If flag is clear char is '-'
        If is nuisance metadata first char is 'N'
        If is useful metadata second char is 'U'
    
    """
    out_str = ''
    if in_atom in ptl.NUISANCE_METADATA_ATOMS:
        out_str = ''.join([colors.FG_RED, 'N', colors.END])
    else:
        out_str = '-'
    if in_atom in ptl.USEFUL_METADATA_ATOMS:
        out_str = ''.join([out_str, colors.FG_GREEN, 'U', colors.END])
    else:
        out_str = out_str + '-'
    return out_str

def get_md_label_str(in_id):
    """Get the gescription string for a metadata item."""
    return_desc = None
    for cur_id in ptl.META_IDS:
        if cur_id['atom'] == in_id:
            return_desc = cur_id['desc']
            break
    return return_desc

def get_md_text_str(in_text, in_hdr_len):
    """
    Build a string that will fit in after the header in an 80 char line.
    
    The return string will be cropped, ending with a period ellipsis, if the
    text passed in is longer than the line length available.
    
    Params:
        in_text - the text to be modified if required
        in_hdr_len - the length of line used by header text.
    
    """
    lf_pos = in_text.find('\n')
    out_str = (in_text.rsplit('\n')[0]) if (lf_pos != -1) \
        else in_text
    if (len(out_str) > (79 - in_hdr_len)) or (lf_pos != -1):
        str_len = 79 - (in_hdr_len + 4)
        out_str = out_str[0:str_len] + '...'
    return out_str

def main():
    """
    Function called by entry point when run as a script.
    
    Handles the argument parsing by calling common routine in ptl and calls 
    through to ptcp() the actual functional entry point for external scripts 
    to call.
    
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Plex Tools copy module."
    
    # Parse the arguments.
    args = ptl.common_parse_args(description)
    
    # Call the working entry point.
    ptinfo(args)
    
    # exit cleanly
    sys.exit(0)

def ptinfo(args):
    """
    The working entry point called by plextool with a namespace of already
    parsed arguments.
    
    Invokes ptl.tool_main which cals back to the process_file() and 
    process_dir() functions in this file.
    
    """
    ptl.print_tool_desc('Info tool')
    
    ptl.tool_main('ptinfo', args)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script generating transcode-video commands to convert videos to MKV

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re
import curses
import xml.etree.ElementTree as ET
import plistlib


# package specific imports
from plextools import colors
from plextools import ptl
from plextools.ptl import PTError, pt_print, pt_print_err, mediainfo_xml_option
from plextools.util import switch, ensure_dir, exec_cli, get_script_name, \
                           is_root, cmd_line_from_list, which
from plextools import ptcp, ptmv, ptln, ptsavmd, ptrstmd, ptrmvmd, ptinfo


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()


################################## Constants #################################
CROP_REGEX = r'From `.*`:[ ]+([0-9]+:[0-9]+:[0-9]+:[0-9]+)'
"""Reasonably reliable regex to pull a suggested crop from detect-crop.sh 
progress output when called with '--values-only'."""

NEW_CROP_REGEX = r'transcode-video --crop ([0-9]+:[0-9]+:[0-9]+:[0-9]+)'
"""Regex to pull a suggested crop from detect-crop verbose progress output 
when called without '--values-only'."""

QUIET_HELP_TEXT = \
    """
    Don't output status and progress information. This allows the output to be
    piped to sh so you no longer need to copy and paste it to create a script.
    """

OTHER_TOOLS_HELP_TEXT = \
    """
    Generate commands to use Don Melton's 'other video transcoding' tools - the
    ruby based tools which use hardware acceleration
    """

AUDIO_TRACK_INFO_HELP_TEXT = \
    """
    Print out information - order, width, language - of the file(s) audio
    tracks then exit.
    """

NO_CROP_HELP_TEXT = \
    """
    By default the generated command lines crop the video to remove areas
    that are unused in the original - eg the areas above and below a letter-
    boxed movie. If you use this option the command line will omit the crop
    instructions.
    """

MAIN_AUDIO_HELP_TEXT = \
    """
    Indicate which track to use as the main audio track in the transcoded
    video. One number is consumed for each video file processed and if all are
    used up before the last file is processed then the default value of 1 is
    used. Note this means that if you are processing multiple files you will
    either need to put all those with a non-default main track as the first
    files processed or specify the 1 for any track that should use the default
    but is processed before a file with a non-default main-audio track.
    """

USE_TOOL_HELP_TEXT = \
    """
    Indicate which video transcoding tool to use. Accepted values are:
        'other' for 'other-transcode'
        'new'   for 'transcode-video'
        'bash'  for 'transcode-video.sh'
    Overides the default tool whatever that is. If the tool is not present an
    exception will be thrown.
    """

ELEM_LABEL_BIT_RATE = "Bit_rate"
ELEM_LABEL_CHANNELS = "Channel_s_"
ELEM_LIST = \
    (("Format", "Format"), (ELEM_LABEL_BIT_RATE, "Bit rate"),
    (ELEM_LABEL_CHANNELS, "Channels"), ("Bit_depth", "Bit depth"),
    ("Compression_mode", "Compression"), ("Title", "Title"),
    ("Language", "Language"), ("Default", "Default"))
ELEM_LABEL = 0
ELEM_TEXT = 1

TOOLS_OTHER = 'other'
TOOLS_NEW = 'new'
TOOLS_BASH = 'bash'


# Globals ####################################################################
out_cl_list = []


# Functions ##################################################################
def my_parse_args(description):
    # parse options and args
    parser = ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter, 
        description=description)
    
    parser.add_argument('-v', '--version', action='version', 
                        version="%(prog)s ("+__version__+")")
    
    parser.add_argument(
        "-q", "--quiet", dest="quiet",  action="store_true", default=False, 
        help=QUIET_HELP_TEXT)
    
    parser.add_argument(
        "-i", "--audio-track-info", dest="audio_track_info",
        action="store_true", default=False, help=AUDIO_TRACK_INFO_HELP_TEXT)
    
    parser.add_argument(
        "-n", "--no-crop", dest="no_crop",
        action="store_true", default=False, help=NO_CROP_HELP_TEXT)
    
    parser.add_argument(
        "-u", "--use-tool", dest="use_tool", metavar='USE_TOOL', type=str,
        nargs='?', help=USE_TOOL_HELP_TEXT, default=None)
    
    parser.add_argument(
        '-m', '--main-audio-track', dest="main_audio_track_list",
        metavar='MAIN_AUDIO_TRACK', type=str, nargs='*',
        help=MAIN_AUDIO_HELP_TEXT, default=None)
    
    parser.add_argument(
        "targets", metavar='FILE|DIR', type=str, nargs='+',
        help="One or more video files (or directories containing video "\
             "files) for tool to scan")
    
    args = parser.parse_args()
    
    return args

def is_eyetv_pkg(in_dir_path):
    """Tests a directory path to see if it is an EyeTV package"""
    eyetv_pkg = False
    if in_dir_path[-6:] == '.eyetv':
        eyetv_pkg = True
    return eyetv_pkg

def add_cl_to_out_list(in_cl, comment=False):
    """Add a command line to final output. Comment out if 'comment' is True"""
    if comment:
        out_cl_list.append("".join(["# ", in_cl]))
    else:
        out_cl_list.append(in_cl)

def my_pt_print(msg, inset=0, no_nl=False, no_output=False):
    """Filters calls to pt_print() to suppress output.
    
    Params:
        msg, inset, no_nl: passed unchanged to pt_print()
        no_output: if True the call to pt_print(0 is not made
    
    """
    if not no_output:
        pt_print(msg, inset, no_nl)

def process_dir(dir_path, args):
    """Walk directory tree calling (indirectly) process_file() on each file"""
    full_dir_path = os.path.abspath(dir_path)
    (par_dir_path, dir_name) = os.path.split(full_dir_path)
    os.path.walk(dir_path, process_dir_files, args)

def file_name_from_eyetv_plist(in_plist_file):
    media_info = plistlib.readPlist(in_plist_file)
    epg_info = media_info['epg info']
    out_file_name = None
    # identify show type
    if epg_info['SEASONID'] > 0:
        file_name_list = [epg_info['TITLE']]
        file_name_list[0] = file_name_list[0].encode("utf-8")
        file_name_list.append(ptl.get_plex_ident(str(epg_info['SEASONID']),
                                             epg_info['EPISODENUM']))
        label_str = "Episode " + epg_info['EPISODENUM']
        label_list = epg_info['ABSTRACT'].split(', ')
        if len(label_list) > 1:
            if (label_list[1] != "Series") and (label_list[1] is not None) and\
               (label_list[1] != ""):
                label_str = u', '.join(map(unicode, label_list[1:]))
        file_name_list.append(label_str)
        out_file_name = ' - '.join(file_name_list)
    elif 'Movie' in epg_info['CONTENT']:
        out_file_name = epg_info['TITLE'] + ' (' + epg_info['YEAR'] + ')'
    return out_file_name

def process_dir_files(in_args, in_dir_path, in_files):
    is_eyetv = is_eyetv_pkg(in_dir_path)
    for video_file in in_files:
        if is_eyetv:
            if (video_file[-4:] != '.mpg') and (video_file[-3:] != '.ts'):
                # not a video file, skip
                continue
        file_path = os.path.join(in_dir_path, video_file)
        process_file(file_path, is_eyetv, in_args)

def get_eyetv_mkv_name(in_eyetv_pkg):
    out_file_name_stem = None
    for plist_file in os.listdir(in_eyetv_pkg):
        if plist_file[-7:] == '.eyetvp':
            # try to build plex file name
            plist_path = os.path.join(in_eyetv_pkg, plist_file)
            out_file_name_stem = file_name_from_eyetv_plist(plist_path)
    if out_file_name_stem is None:
        pkg_name = os.path.splitext(os.path.basename(in_eyetv_pkg))[0]
        out_file_name_stem = pkg_name
    out_mkv_file_name = out_file_name_stem + '.mkv'
    return out_mkv_file_name

def get_default_tools():
    """What is the default transcoding tool?
    
    Checks for the presence of each of the video transcoding scripts toolsets,
    starting with 'other-transcode', then 'transcode-video' and finally
    'transcode-video.sh' (roughly best-to-worst order) and returns the first
    one it finds.
    
    Params:
        None
    
    Returns:
        If a tool is found one of the constants:
            TOOLS_OTHER, TOOLS_NEW, TOOLS_BASH
        If no tool is found:
            None
    """
    out_tool = None
    if have_other_tools():
        out_tool = TOOLS_OTHER
    elif have_new_tools():
        out_tool = TOOLS_NEW
    elif have_bash_tools():
        out_tool = TOOLS_BASH
    return (out_tool)

def have_bash_tools():
    """Are original bash based tools present?"""
    which_result = which('transcode-video.sh')
    return (which_result != None)

def have_new_tools():
    """Are newer, ruby based, tools present?"""
    which_result = which('transcode-video')
    return (which_result != None)

def have_other_tools():
    """Are the newer, hardware acceleration based, 'other' tools present?"""
    which_result = which('other-transcode')
    return (which_result != None)

def get_crop(in_file_path, in_args):
    out_crop = None
    if in_args.use_tool == TOOLS_OTHER:
        out_crop = {'handbrake': None, 'mplayer': None,
                    'ffmpeg': "--crop auto "}
    elif in_args.use_tool == TOOLS_NEW:
        out_crop = new_tools_crop(in_file_path, in_args)
    else:
        out_crop = old_tools_crop(in_file_path, in_args)
    return out_crop

def old_tools_crop(in_file_path, in_args):
    out_crop_dict = {'handbrake': None, 'mplayer': None, 'ffmpeg': None}
    try:
        command_list = ["detect-crop.sh", "--values-only", in_file_path]
        result = exec_cli(command_list)
        if (result[2] != 0) and (result[0] == ''):
            # test to see if not simple error, just differing values
            progress_lines = result[1].split("\n")
            prog_len = len(progress_lines)
            stderr_re = re.search(CROP_REGEX, progress_lines[prog_len - 3])
            if stderr_re != None:
                # found a suggested crop, should be HandBrakeCLI value
                my_pt_print("HandBrakeCLI and mplayer disagree on crop",
                            inset=2, no_output=in_args.quiet)
                handbrake_crop = stderr_re.group(1)
                # handbrake version of command line
                out_crop_dict['handbrake'] = "".join(["--crop ",
                                                      handbrake_crop, " "])
                # get mplayer crop
                stderr_re = re.search(CROP_REGEX, progress_lines[prog_len - 2])
                if stderr_re != None:
                    mplayer_crop = stderr_re.group(1)
                    # mplayer version of command line
                    out_crop_dict['mplayer'] = "".join(["--crop ",
                                                        mplayer_crop, " "])
            else:
                # unequivocal error
                pt_print_err(str(result[2]))
                pt_print(result[1])
                out_crop_dict = None
        else:
            if result[0] is not None:
                result_lines = result[0].split("\n")
                my_pt_print("HandBrakeCLI & mplayer agree on crop", inset=2,
                            no_output=in_args.quiet)
                out_crop = result_lines[0]
                out_crop_dict['handbrake'] = "".join(["--crop ",
                                                      out_crop, " "])
            else:
                pt_print_err("'detect-crop.sh' returns no crop but also no "
                             "error.", inset=2)
                out_crop_dict = None
    except ptl.PTError, e:
        pt_print_err(e.value)
    
    return out_crop_dict

def new_tools_crop(in_file_path, in_args):
    out_crop_dict = {'handbrake': None, 'mplayer': None, 'ffmpeg': None}
    try:
        command_list = ["detect-crop", in_file_path]
        result = exec_cli(command_list)
        if result[2] == 0:
            # got a crop
            progress_lines = result[0].split("\n")
            prog_len = len(progress_lines)
            agree_str = "disagree"
            if prog_len == 7:
                agree_str = "agree"
            my_pt_print("HandBrakeCLI and ffmpeg " + agree_str + " on crop",
                        inset=2, no_output=in_args.quiet)
            out_dict_idx = "handbrake"
            for response_line in progress_lines:
                if response_line[:1] == 't':
                    crop_re = re.search(NEW_CROP_REGEX, response_line)
                    if crop_re is not None:
                        crop_str = crop_re.group(1)
                        out_crop_dict[out_dict_idx] = "".join(["--crop ",
                                                             crop_str, " "])
                        out_dict_idx = "ffmpeg"
            if out_crop_dict['handbrake'] is None:
                raise PTError("Failed to find any crop value in detect-crop"
                              " output")
    except ptl.PTError, e:
        pt_print_err(e.value)
        out_crop_dict = None
    
    return out_crop_dict

def get_mediainfo_track_iter(in_file_path, in_is_eyetv):
    """Returns an object to iterate over the tracks in a mediainfo XML file"""
    try:
        xml_output = ptl.safe_mediainfo_call(in_file_path, 
                                             mediainfo_xml_option())[0]
    except:
        pt_print_err("".join(["Mediainfo was unable to parse: '", colors.BOLD, 
                     os.path.basename(in_file_path), 
                     colors.END, "'"]), inset=2)
        return None
    # get track info
    root = ET.fromstring(xml_output)
    file_element = root.find('File')
    if file_element is None:
        raise PTError("No file '" + colors.BOLD + in_file_path + 
                      colors.ALL_OFF +  "' found by mediainfo.")
    out_iter = file_element.iter('track')
    if out_iter is None:
        raise PTError("No track info found by mediainfo in '" + 
                      colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
    return out_iter

def print_audio_track_info(in_file_path, in_is_eyetv, in_args):
    """Function to display information about audio tracks"""
    # change display filename if this is an EyeTV video
    out_filename = os.path.basename(in_file_path)
    if in_is_eyetv:
        out_filename = os.path.dirname(in_file_path)
    my_pt_print("Audio track info for  " + out_filename + ":",
                inset=1, no_output=in_args.quiet)
    # get track iterator
    track_iter = get_mediainfo_track_iter(in_file_path, in_is_eyetv)
    audio_track_count = 0
    while True:
        track = next(track_iter, None)
        if track is not None:
            if track.attrib[ptl.TYPE_ATOM] == 'Audio':
                audio_track_count += 1
                my_pt_print("Audio track found...", inset=2, 
                            no_output=in_args.quiet)
                # mostly, but not always tracks have a 'typeorder' attribute
                # which I use for track number, otherwise a count
                if ptl.ORDER_ATOM in track.attrib:
                    audio_track_count = int(track.attrib[ptl.ORDER_ATOM])
                my_pt_print("Track number: " + str(audio_track_count), inset=3,
                            no_output=in_args.quiet)
                for elem_id in ELEM_LIST:
                    elem = track.find(elem_id[ELEM_LABEL])
                    if elem is not None:
                        elem_text = elem.text
                        if elem_id[ELEM_LABEL] == ELEM_LABEL_CHANNELS:
                            # extract just the number of channels
                            elem_text = elem_text.split()[0]
                        elif elem_id[ELEM_LABEL] == ELEM_LABEL_BIT_RATE:
                            # pretty print bit rate if >= 1000
                            split_text = elem_text.split()
                            if len(split_text) == 3:
                                tail = ' '.join(split_text[1:])
                                elem_text = ','.join([split_text[0], tail])
                        my_pt_print(elem_id[ELEM_TEXT] + ": " + elem_text,
                            inset=4, no_output=in_args.quiet)
        else:
            break

def get_width_for_other_transcode(in_track):
    """Get audio width of track.
    
    Params:
        in_track - the audio track info extracted from mediainfo output by
                   ElementTree
    
    Returns:
        'surround' - if more then 2 channels found
        'stereo' - if only 2 channels found
        'original' if only one channel found (NB This means other-transcode
                   will disable transcoding for that track and copy it
                   instead. Shouldn't be a problem for mono sound!)
    """
    out_value = 'original'
    if in_track.attrib[ptl.TYPE_ATOM] == 'Audio':
        elem = in_track.find(ELEM_LABEL_CHANNELS)
        if elem is not None:
            elem_text = elem.text
            elem_text = elem_text.split()[0]
            if int(elem_text) > 2:
                out_value = 'surround'
            elif int(elem_text) == 2:
                out_value = 'stereo'
    else:
        raise ptl.PTError("Not an audio track so can't get audio width.")
    return out_value


def process_file(in_file_path, in_is_eyetv, in_args):
    """Chooses between building command line or printing audio track info"""
    if in_args.audio_track_info:
        print_audio_track_info(in_file_path, in_is_eyetv, in_args)
    else:
        build_file_cli(in_file_path, in_is_eyetv, in_args)
        if in_args.main_audio_track_list is not None:
            if len(in_args.main_audio_track_list) >= 1:
                in_args.main_audio_track = in_args.main_audio_track_list[0]
                in_args.main_audio_track_list = in_args.main_audio_track_list[1:]
            else:
                in_args.main_audio_track = None
                in_args.main_audio_track_list = None


def build_file_cli(in_file_path, in_is_eyetv, in_args):
    """Scans file and builds suggested transcode-video command line"""
    my_pt_print("".join(["Analysing '", colors.BOLD, 
                os.path.basename(in_file_path), 
                colors.END, "'"]), inset=1, no_output=in_args.quiet)
    
    if in_args.use_tool == TOOLS_OTHER:
        build_cli_for_other_tools(in_file_path, in_is_eyetv, in_args)
    else:
        build_cli_for_bash_or_new_tools(in_file_path, in_is_eyetv, in_args)

def build_cli_for_bash_or_new_tools(in_file_path, in_is_eyetv, in_args):
    """builds suggested transcode-video or transcode-video.sh command line"""
    # do we have new tools?
    new_tools = have_new_tools()

    # initialise transcode command line list
    if in_args.use_tool == TOOLS_NEW:
        tvcl_list = ["transcode-video --preset slow --add-subtitle all "
                     "--audio-width all=double "]
    else:
        tvcl_list = ["transcode-video.sh --slow --mkv "]

    # Customise cli for specific file
    if in_args.use_tool == TOOLS_BASH:
        try:
            xml_output = ptl.safe_mediainfo_call(in_file_path, 
                                                 mediainfo_xml_option())[0]
        except:
            pt_print_err("".join(["Mediainfo was unable to parse: '", colors.BOLD, 
                         os.path.basename(in_file_path), 
                         colors.END, "'"]), inset=2)
            return
        root = ET.fromstring(xml_output)
        file_element = root.find('File')
        if file_element is None:
            raise PTError("No file '" + colors.BOLD + in_file_path + 
                          colors.ALL_OFF +  "' found by mediainfo.")
        track_iter = file_element.iter('track')
        if track_iter is None:
            raise PTError("No track info found by mediainfo in '" + 
                          colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
        # count the audio and subtitle tracks
        num_audio_tracks = 0
        num_subtitle_tracks = 0
        while True:
            track = next(track_iter, None)
            if track is not None:
                if track.attrib[ptl.TYPE_ATOM] == 'Audio':
                    num_audio_tracks += 1
                elif track.attrib[ptl.TYPE_ATOM] == 'Text':
                    # found a subtitle track, increment the count
                    num_subtitle_tracks += 1
                    # check if is a non-trans-codeable subtitle, remove if is
                    # need to do it this slightly backwards way because the 
                    # "Format" element is not always present.
                    format_elem = track.find('Format')
                    if format_elem is not None:
                        format_elem_text = format_elem.text
                        if format_elem_text == "DVB Subtitle":
                            num_subtitle_tracks -= 1
            else:
                break
        # throw away silent audio tracks in EyeTV
        if in_is_eyetv:
            if num_audio_tracks > 1:
                num_audio_tracks -= 1
        # progress update
        my_pt_print("".join(["Audio tracks found: ", colors.BOLD, 
                    str(num_audio_tracks), colors.END]), inset=2,
                    no_output=in_args.quiet)
        my_pt_print("".join(["Subtitle tracks found: ", colors.BOLD, 
                    str(num_subtitle_tracks), colors.END]), inset=2,
                    no_output=in_args.quiet)
        # add audio track options
        if num_audio_tracks > 1:
            for track_num in range(2, (num_audio_tracks + 1)):
                option = "".join(["--add-audio double,", str(track_num)])
                if track_num == num_audio_tracks:
                    # last audio track, assume is commentary
                    option = option + ",\"Audio Commentary\""
                # add a terminating space for this option
                option = option + " "
                tvcl_list.append(option)
        # add subtitle track options
        if num_subtitle_tracks > 0:
            for track_num in range(1, (num_subtitle_tracks + 1)):
                option = "".join(["--add-subtitle ", str(track_num), " "])
                tvcl_list.append(option)
    # change output filename if this is an EyeTV video
    if in_is_eyetv:
        pkg_path = os.path.dirname(in_file_path)
        eyetv_out_filename = get_eyetv_mkv_name(pkg_path)
        option = "".join(["--output", " ",
            cmd_line_from_list([eyetv_out_filename]), " "])
        tvcl_list.append(option)
    # non-default main audio track?
    if in_args.main_audio_track_list is not None:
        main_audio_flag = ("--main-audio" if in_args.use_tool == TOOLS_NEW
                           else "--audio")
        option = "".join([main_audio_flag, " ", in_args.main_audio_track, " "])
        tvcl_list.append(option)
    if not in_args.no_crop:
        # work out crop
        crop_result = get_crop(in_file_path, in_args)
        if crop_result != None and \
            any(v is not None for v in crop_result.itervalues()):
            comment_this_line = False
            for index_val in ['handbrake', 'mplayer', 'ffmpeg']:
                if crop_result[index_val] is not None:
                    new_tvcl_list = list(tvcl_list)
                    new_tvcl_list.append(crop_result[index_val])
                    new_tvcl_list.append(cmd_line_from_list([in_file_path]))
                    tvcl = "".join(new_tvcl_list)
                    add_cl_to_out_list(tvcl, comment_this_line)
                    comment_this_line = True
        else:
            raise PTError("Error getting crop.")
    else:
        # if not cropping, still need to add the cli to the out list
        tvcl_list.append(cmd_line_from_list([in_file_path]))
        tvcl = "".join(tvcl_list)
        add_cl_to_out_list(tvcl)

def build_cli_for_other_tools(in_file_path, in_is_eyetv, in_args):
    """Builds suggested other-transcode command line"""
    # initialise command line
    tvcl_list = ["other-transcode --copy-track-names --convert-dvb-subs "
                 "--add-subtitle all --eac3-aac "]
    if not in_args.no_crop:
        tvcl_list.append("--crop auto ")
    # select non-default main audio track?
    if (in_args.main_audio_track_list is not None) and\
       (in_args.main_audio_track != '1'):
        option = "".join(["--main-audio", " ", in_args.main_audio_track, " "])
        tvcl_list.append(option)
    # add audio tracks with transcode modidied by width
    track_iter = get_mediainfo_track_iter(in_file_path, in_is_eyetv)
    audio_track_count = 0
    while True:
        track = next(track_iter, None)
        if track is not None:
            if track.attrib[ptl.TYPE_ATOM] == 'Audio':
                audio_track_count += 1
                my_pt_print("Audio track found...", inset=2, 
                            no_output=in_args.quiet)
                # mostly, but not always tracks have a 'typeorder' attribute
                # which I use for track number, otherwise a count
                if ptl.ORDER_ATOM in track.attrib:
                    audio_track_count = int(track.attrib[ptl.ORDER_ATOM])
                my_pt_print("Track number: " + str(audio_track_count), inset=3,
                            no_output=in_args.quiet)
                track_width = get_width_for_other_transcode(track)
                option = "".join(["--add-audio", " ", str(audio_track_count), 
                                 "=", track_width, " "])
                tvcl_list.append(option)
                if track_width == "original":
                    track_width = 'mono'
                my_pt_print("Track number " + str(audio_track_count) + " is "
                            + track_width + " audio.", inset=3, 
                            no_output=in_args.quiet)
        else:
            break
    # change output filename if this is an EyeTV video
    if in_is_eyetv:
        pkg_path = os.path.dirname(in_file_path)
        eyetv_out_filename = get_eyetv_mkv_name(pkg_path)
        option = "".join(["--output", " ",
            cmd_line_from_list([eyetv_out_filename]), " "])
        tvcl_list.append(option)
    # add input file name (quoting for CLI safety)
    tvcl_list.append(cmd_line_from_list([in_file_path]))
    # add generated CLI to output CLI list
    tvcl = "".join(tvcl_list)
    add_cl_to_out_list(tvcl)

def main():
    """Script generating transcode-video commands to convert video to MKV.
    
    This script uses mediainfo to examine MKV files, determine how many audio
    and subtitle tracks each contains; it then uses Don Melton's 
    'detect-crop.sh' script to find an optimum crop to use for the transcode.
    The script then uses the gathered information and builds suggested 
    transcode-video command lines.
    
    If 'detect-crop.sh' finds that HandBrakeCLI and mplayer suggest different 
    crop values it uses the HandBrakeCLI one but also lists a command line
    based on the mplayer value but comments it out.
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Script generating transcode-video commands  " \
                  "to convert videos to MKV"
    
    # parse options and args
    args = my_parse_args(description)
     
    if is_root():
        print >> sys.stderr, \
            get_script_name() + ": Error: This script must not run as root. "\
            "Exiting..."
        sys.exit(1)
    
    if not args.quiet:
        ptl.print_tool_desc('Analyses video files; suggests video transcoding'
                            ' command lines with crop, audio and subtitle'
                            ' options')
    
    if args.use_tool is not None:
        if args.use_tool == TOOLS_OTHER:
            if not have_other_tools():
                raise ptl.PTError("'other-transcode' is not available.")
        if args.use_tool == TOOLS_NEW:
            if not have_other_tools():
                raise ptl.PTError("'transcode-video' is not available.")
        if args.use_tool == TOOLS_BASH:
            if not have_other_tools():
                raise ptl.PTError("'transcode-video.sh' is not available.")
    else:
        args.use_tool = get_default_tools()
        if args.use_tool is None:
            raise ptl.PTError("No transcoding tool available.")

    if args.main_audio_track_list is not None:
        args.main_audio_track = args.main_audio_track_list[0]
        args.main_audio_track_list = args.main_audio_track_list[1:]
    else:
        args.main_audio_track = None

    for file_path in args.targets:
        if os.path.exists(file_path):
            if os.path.isfile(file_path):
                process_file(file_path, False, args)
            elif os.path.isdir(file_path):
                process_dir(file_path, args)
        else:
            pt_print_err("The file '" + file_path + "' "
                         "does not exist. Perhaps it is a broken symbolic "
                         "link or alias")
    # print out results
    if len(out_cl_list) > 0:
        my_pt_print(colors.FG_RED + "Suggested command lines:" + colors.END,
                    no_output=args.quiet)
        for cl in out_cl_list:
            cl = cl.encode("utf-8")
            print(cl)
    
    # exit cleanly
    sys.exit(0)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()

#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script generating transcode-video commands to convert videos to MKV

"""

# standard library imports
import subprocess
import os
import sys
import argparse
from argparse import ArgumentParser
import re
import curses
import xml.etree.ElementTree as ET
import plistlib


# package specific imports
from plextools import colors
from plextools import ptl
from plextools.ptl import pt_print, pt_print_err
from plextools.util import switch, ensure_dir, exec_cli, get_script_name, \
                           is_root, cmd_line_from_list, which
from plextools import ptcp, ptmv, ptln, ptsavmd, ptrstmd, ptrmvmd, ptinfo


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2014 Alan Staniforth"
__license__ = "New-style BSD"
# get version from mclib
__version__ = ptl.get_plextools_version_string()

################################## Constants #################################
CROP_REGEX = r'From `.*`:[ ]+([0-9]+:[0-9]+:[0-9]+:[0-9]+)'
"""Reasonably reliable regex to pull a suggested crop from detect-crop.sh 
progress output when called with '--values-only'."""

################################### Globals ##################################
out_cl_list = []

################################## Functions #################################
def my_parse_args(description):
    # parse options and args
    parser = ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter, 
        description=description)
    
    parser.add_argument('-v', '--version', action='version', 
                        version="%(prog)s ("+__version__+")")
    
    parser.add_argument(
        "targets", metavar='FILE|DIR', type=str, nargs='+',
        help="One or more video files (or directories containing video "\
             "files) for tool to scan")
    
    args = parser.parse_args()
    
    return args

def is_eyetv_pkg(in_dir_path):
    """Tests a directory path to see if it is an EyeTV package"""
    eyetv_pkg = False
    if in_dir_path[-6:] == '.eyetv':
        eyetv_pkg = True
    return eyetv_pkg

def add_cl_to_out_list(in_cl, comment=False):
    """Add a command line to final output. Comment out if 'comment' is True"""
    if comment:
        out_cl_list.append("".join(["# ", in_cl]))
    else:
        out_cl_list.append(in_cl)

def process_dir(dir_path, args):
    """Walk directory tree calling (indirectly) process_file() on each file"""
    full_dir_path = os.path.abspath(dir_path)
    (par_dir_path, dir_name) = os.path.split(full_dir_path)
    os.path.walk(dir_path, process_dir_files, args)

def file_name_from_eyetv_plist(in_plist_file):
    media_info = plistlib.readPlist(in_plist_file)
    epg_info = media_info['epg info']
    out_file_name = None
    # identify show type
    if epg_info['SEASONID'] > 0:
        file_name_list = [epg_info['TITLE']]
        file_name_list.append(ptl.get_plex_ident(str(epg_info['SEASONID']),
                                             epg_info['EPISODENUM']))
        label_str = epg_info['ABSTRACT'].split(', ')[1]
        file_name_list.append(label_str)
        out_file_name = ' - '.join(file_name_list)
    elif epg_info['CONTENT'].find('Movie') is not -1:
        out_file_name = epg_info['TITLE'] + ' (' + epg_info['YEAR'] + ')'
    return out_file_name

def process_dir_files(in_args, in_dir_path, in_files):
    is_eyetv = is_eyetv_pkg(in_dir_path)
    for video_file in in_files:
        if is_eyetv:
            if video_file[-4:] != '.mpg':
                # not a video file, skip
                continue
        file_path = os.path.join(in_dir_path, video_file)
        process_file(file_path, is_eyetv, in_args)
        if is_eyetv:
            for plist_file in in_files:
                if plist_file[-7:] == '.eyetvp':
                    # try to build plex file name
                    plist_path = os.path.join(in_dir_path, plist_file)
                    out_file_name_stem = file_name_from_eyetv_plist(plist_path)
            if out_file_name_stem is None:
                pkg_name = os.path.splitext(os.path.basename(in_dir_path))[0]
                out_file_name_stem = pkg_name
            handbrake_file_stem = os.path.splitext(video_file)[0]
            handbrake_mkv_file = handbrake_file_stem + '.mkv'
            handbrake_log_file = handbrake_file_stem + '.mkv.log'
            new_mkv_file_name = out_file_name_stem + '.mkv'
            new_log_file_name = out_file_name_stem + '.mkv.log'
            rename_list = ['mv', handbrake_mkv_file, new_mkv_file_name]
            rename_cmd = cmd_line_from_list(rename_list)
            add_cl_to_out_list(rename_cmd)
            rename_list[1] = handbrake_log_file
            rename_list[2] = new_log_file_name
            rename_cmd = cmd_line_from_list(rename_list)
            add_cl_to_out_list(rename_cmd)

def process_file(in_file_path, in_is_eyetv, in_args):
    """Scans file and builds suggested transcode-video command line"""
    pt_print("".join(["Analysing '", colors.BOLD, 
             os.path.basename(in_file_path), 
             colors.END, "'"]), inset=1)
    # get the mediainfo XML output for the file
    try:
        xml_output = ptl.safe_mediainfo_call(in_file_path,
                                             mediainfo_xml_option())[0]
    except:
        pt_print_err("".join(["Mediainfo was unable to parse: '", colors.BOLD, 
                     os.path.basename(in_file_path), 
                     colors.END, "'"]), inset=2)
        return
    root = ET.fromstring(xml_output)
    file_element = root.find('File')
    if file_element is None:
        raise PTError("No file '" + colors.BOLD + in_file_path + 
                      colors.ALL_OFF +  "' found by mediainfo.")
    track_iter = file_element.iter('track')
    if track_iter is None:
        raise PTError("No track info found by mediainfo in '" + 
                      colors.BOLD + in_file_path + colors.ALL_OFF + "'.")
    # count the audio and subtitle tracks
    num_audio_tracks = 0
    num_subtitle_tracks = 0
    while True:
        track = next(track_iter, None)
        if track != None:
            if track.attrib[ptl.TYPE_ATOM] == 'Audio':
                num_audio_tracks += 1
            elif track.attrib[ptl.TYPE_ATOM] == 'Text':
                # found a subtitle track, increment the count
                num_subtitle_tracks += 1
                # check if is a non-trans-codeable subtitle, remove if is
                # need to do it this slightly backwards way because the 
                # "Format" element is not always present.
                format_elem = track.find("Format")
                if format_elem != None:
                    format_elem_text = format_elem.text
                    if format_elem_text == "DVB Subtitle":
                        num_subtitle_tracks -= 1
        else:
            break
    # throw away silent audio tracks in EyeTV
    if in_is_eyetv:
        if num_audio_tracks > 1:
            num_audio_tracks -= 1
    # progress update
    pt_print("".join(["Audio tracks found: ", colors.BOLD, 
             str(num_audio_tracks), colors.END]), inset=2)
    pt_print("".join(["Subtitle tracks found: ", colors.BOLD, 
             str(num_subtitle_tracks), colors.END]), inset=2)
    # initialise transcode video command line list
    tvcl_list = ["transcode-video.sh --slow --mkv "]
    # add audio track options
    if num_audio_tracks > 1:
        for track_num in range(2, (num_audio_tracks + 1)):
            option = "".join(["--add-audio double,", str(track_num)])
            if track_num == num_audio_tracks:
                # last audio track, assume is commentary
                option = option + ",\"Audio Commentary\""
            # add a terminating space for this option
            option = option + " "
            tvcl_list.append(option)
    # add subtitle track options
    if num_subtitle_tracks > 0:
        for track_num in range(1, (num_subtitle_tracks + 1)):
            option = "".join(["--add-subtitle ", str(track_num), " "])
            tvcl_list.append(option)
    # work out crop
    mplayer_crop = ''
    handbrake_crop = ''
    out_crop = ''
    try:
        command_list = ["detect-crop.sh", "--values-only", in_file_path]
        result = exec_cli(command_list)
        if (result[2] != 0) and (result[0] == ''):
            # test to see if not simple error, just differing values
            progress_lines = result[1].split("\n")
            prog_len = len(progress_lines)
            stderr_re = re.search(CROP_REGEX, progress_lines[prog_len - 3])
            if stderr_re != None:
                # found a suggested crop, should be HandBrakeCLI value
                pt_print("HandBrakeCLI and mplayer disagree on crop", inset=2)
                handbrake_crop = stderr_re.group(1)
                # handbrake version of command line
                handbrake_tvcl_list = list(tvcl_list)
                handbrake_tvcl_list.append(
                    "".join(["--crop ", handbrake_crop, " "]))
                # add the file path to the command list
                handbrake_tvcl_list.append(cmd_line_from_list([in_file_path]))
                tvcl = "".join(handbrake_tvcl_list)
                add_cl_to_out_list(tvcl)
                # get mplayer crop
                stderr_re = re.search(CROP_REGEX, progress_lines[prog_len - 2])
                if stderr_re != None:
                    mplayer_crop = stderr_re.group(1)
                    # mplayer version of command line
                    mplayer_tvcl_list = list(tvcl_list)
                    mplayer_tvcl_list.append(
                        "".join(["--crop ", mplayer_crop, " "]))
                    # add the file path to the command list
                    mplayer_tvcl_list.append(
                        cmd_line_from_list([in_file_path]))
                    tvcl = "".join(mplayer_tvcl_list)
                    add_cl_to_out_list(tvcl, True)
            else:
                # unequivocal error
                pt_print_err(str(result[2]))
                pt_print(result[1])
        else:
            if result[0] is not None:
                result_lines = result[0].split("\n")
                pt_print("HandBrakeCLI & mplayer agree on crop", inset=2)
                out_crop = result_lines[0]
                tvcl_list.append("".join(["--crop ", out_crop, " "]))
                # add the file path to the command list
                tvcl_list.append(cmd_line_from_list([in_file_path]))
                tvcl = "".join(tvcl_list)
                add_cl_to_out_list(tvcl)
            else:
                pt_print_err("'detect-crop.sh' returns no crop but also no "
                             "error.", inset=2)
    except ptl.PTError, e:
        pt_print_err(e.value)

def main():
    """Script generating transcode-video commands to convert video to MKV.
    
    This script uses mediainfo to examine MKV files, determine how many audio
    and subtitle tracks each contains; it then uses Don Melton's 
    'detect-crop.sh' script to find an optimum crop to use for the transcode.
    The script then uses the gathered information and builds suggested 
    transcode-video command lines.
    
    If 'detect-crop.sh' finds that HandBrakeCLI and mplayer suggest different 
    crop values it uses the HandBrakeCLI one but also lists a command line
    based on the mplayer value but comments it out.
    """
    description = """   %(prog)s version """ + \
                  ptl.get_plextools_version_string() + \
                  "\n\n    Script to generate 'transcode-video.sh' command " \
                  "lines for MKV files"
    
    # parse options and args
    args = my_parse_args(description)
     
    if is_root():
        print >> sys.stderr, \
            get_script_name() + ": Error: This script must not run as root. "\
            "Exiting..."
        sys.exit(1)
    
    ptl.print_tool_desc('Analyses MKV files; suggests transcode-video.sh'
                        ' command lines with crop, audio and subtitle options')
    
    for file_path in args.targets:
        if os.path.exists(file_path):
            if os.path.isfile(file_path):
                process_file(file_path, False, args)
            elif os.path.isdir(file_path):
                process_dir(file_path, args)
        else:
            pt_print_err("The file '" + file_path + "' "
                         "does not exist. Perhaps it is a broken symbolic "
                         "link or alias")
    # print out results
    if len(out_cl_list) > 0:
        pt_print(colors.FG_RED + "Suggested command lines:" + colors.END)
        for cl in out_cl_list:
            print(cl)
    
    # exit cleanly
    sys.exit(0)

### Called when run as a script, not imported as a module
if __name__ == "__main__":
    main()
